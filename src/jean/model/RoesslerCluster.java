package jean.model;

import jean.render.IStamp;

public class RoesslerCluster extends AttractorCluster {
	public RoesslerCluster(int cluster_index, Cluster up, int characteristic, String name, IStamp stamp) {
		super(cluster_index, up, characteristic, name, stamp);
	}

	public RoesslerCluster(int characteristic, String name, IStamp stamp) {
		super(characteristic, name, stamp);
	}
	
	protected void next_point(AttractorCell next, AttractorCell acell) {
		double a =  mng_LorenzA.eval(acell, this);
		double b =  mng_LorenzB.eval(acell, this);
		double c =  mng_LorenzC.eval(acell, this);
		
	    double dx = -(acell.ly + acell.lz);
	    double dy = acell.lx + a * acell.ly;
	    double dz = b + acell.lz * (acell.lx - c);

		next.lx += DELTA * dx;
		next.ly += DELTA * dy;
		next.lz += DELTA * dz;
	}
}
