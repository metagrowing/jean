package jean.model;

import java.util.List;

public class Grow {

	private Unaltered unaltered = new Unaltered();
	private Increase increase = new Increase();
	
	class Unaltered implements IGrowStrategy {
		@Override
		public void grow(List<Cell> cl, Cluster cluster) {
		}
	}

	class Increase implements IGrowStrategy {
		@Override
		public void grow(List<Cell> cl, Cluster cluster) {
			if(cluster.birthrate > 0.9999 || (cluster.birthrate > 0.0 && Math.random() < cluster.birthrate)) {
				cluster.cell_list.add(cluster.createCell(cluster.cell_list.size()));
//				for (Iterator<Cell> cell_iter = cluster.cell_list.iterator(); cell_iter.hasNext();) {
//					Cell cell = cell_iter.next();
//					System.out.println("cell index " + cell);
//				}
			}
		}
	}

	public IGrowStrategy strategy(List<Cell> cl, int cluster_size) {
		if(cl.size() >= cluster_size) return unaltered;
		return increase;
	}

}
