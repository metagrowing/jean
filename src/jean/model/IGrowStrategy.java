package jean.model;

import java.util.List;

public interface IGrowStrategy {
	void grow(List<Cell> cl, Cluster cluster);
}
