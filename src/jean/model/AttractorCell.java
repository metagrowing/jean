package jean.model;

public class AttractorCell extends Cell {
	protected double lx;
	protected double ly;
	protected double lz;
	
	public AttractorCell(int index, int id, Cluster cluster) {
		super(index, id, cluster);
	}

	@Override
	public void reset() {
		super.reset();
		lx = 0.05 * (Math.random() - 0.5);
		ly = 0.05 * (Math.random() - 0.5);
		lz = 0.05 * (Math.random() - 0.5);
	}

	@Override
	public void patchState(Cell other) throws CloneNotSupportedException {
		super.patchState(other);
		if (other instanceof AttractorCell) {
			AttractorCell a = (AttractorCell)other;
			this.lx = a.lx;
			this.ly = a.ly;
			this.lz = a.lz;
		}
	}

	@Override
	public String toString() {
		return "AttractorCell [lx=" + lx + ", ly=" + ly + ", lz=" + lz + "]";
	}

}
