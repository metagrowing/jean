package jean.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jean.OSCout.ScSynthSender;
import jean.application.Jean;
import jean.l2asm.FnManagement;
import jean.noise.JMath;
import jean.render.IStamp;
import jean.render.Oval;
import jean.render.Rect;
import jean.render.Triangle;
import jean.render.Words;
import jean.render.lwjgl.EvalutedUniforms;
import jean.render.lwjgl.ImageSequence;
import jean.render.lwjgl.ImageUnique;

public class Cluster {
	public static final int C_SILENT= 0;
	public static final int C_CALVE = 1;
	public static final int C_CURSOR = 2;

	public FnManagement mng_N = new FnManagement(0);
	public FnManagement mng_Shape = new FnManagement(0);
	public FnManagement mng_PosX = new FnManagement(0);
	public FnManagement mng_PosY = new FnManagement(0);
	public FnManagement mng_DimW = new FnManagement(1);
	public FnManagement mng_DimH = new FnManagement(1);
	public FnManagement mng_Rot = new FnManagement(0);
	public FnManagement mng_ColH = new FnManagement(0);
	public FnManagement mng_ColS = new FnManagement(0);
	public FnManagement mng_ColV = new FnManagement(0.5);
	public FnManagement mng_ColA = new FnManagement(0.5);
	public FnManagement mng_LayA = new FnManagement(0.1);
	public FnManagement mng_LayB = new FnManagement(0.5);
	public FnManagement mng_LayTx = new FnManagement(0);
	public FnManagement mng_LayTy = new FnManagement(0);
	public FnManagement mng_LaySx = new FnManagement(1.0);
	public FnManagement mng_LaySy = new FnManagement(1.0);
	public FnManagement mng_LayRot = new FnManagement(0);
	public FnManagement mng_Stream = new FnManagement(-1.0);
	public FnManagement mng_Birthrate = new FnManagement(1.0);
	public FnManagement mng_Decay = new FnManagement(0.0);
	public EvalutedUniforms blend_uniforms = new EvalutedUniforms();
	public List<Cell> cell_list = new ArrayList<Cell>();

	public int cluster_index;
	public final int characteristic;
	private String name;
	public Cluster up;
	boolean preserved;
	
	public IStamp stamp;
	public ImageUnique imageUnique;
	public ImageSequence imageSequence;
	
	public int blend_mode = 0;
	public float layer_alpha = 1.0f;
	public float layer_tx = 0.0f;
	public float layer_ty = 0.0f;
	public float layer_sx = 1.0f;
	public float layer_sy = 1.0f;
	public double layer_rot = 0.0;

	public double birthrate = 1.0;

	public ScSynthSender fresh_synth;
	public ScSynthSender hit_synth;
	public ScSynthSender exit_synth;
	public int stream;

	public static Cluster dummy = new Cluster();
	
	private Cluster() {
		this.characteristic = C_SILENT;
//		System.out.println("Cluster( preserved " + name + " " + preserved);
	}
	
	public Cluster(int cluster_index, Cluster up, int characteristic, String name, IStamp stamp) {
		this.cluster_index = cluster_index;
		this.up = up;
		this.characteristic = characteristic;
		this.name = name;
		this.stamp = stamp;
		set_defaults();
//		System.out.println("Cluster(int cluster_index, preserved " + name + " " + preserved);
	}

	public Cluster(int characteristic, String name, IStamp stamp) {
		this.characteristic = characteristic;
		this.name = name;
		this.stamp = stamp;
		set_defaults();
//		System.out.println("Cluster(int characteristic, preserved " + name + " " + preserved);
	}

	private void set_defaults() {
		blend_uniforms.clear();
		blend_uniforms.addUniform("xamount", new FnManagement(0.1));
		blend_uniforms.addUniform("yamount", new FnManagement(0.1));
		blend_uniforms.addUniform("xscale", new FnManagement(1));
		blend_uniforms.addUniform("yscale", new FnManagement(1));
		
		mng_N.remove_code();
		mng_Shape.remove_code();
		mng_Birthrate.remove_code();
		mng_Decay.remove_code();
		
		mng_LayA.remove_code();
		mng_LayB.remove_code();
		mng_LayTx.remove_code();
		mng_LayTy.remove_code();
		mng_LaySx.remove_code();
		mng_LaySy.remove_code();
		mng_LayRot.remove_code();
		
		mng_PosX.remove_code();
		mng_PosY.remove_code();
		mng_DimW.remove_code();
		mng_DimH.remove_code();
		mng_Rot.remove_code();
		
		mng_ColH.remove_code();
		mng_ColS.remove_code();
		mng_ColV.remove_code();
		mng_ColA.remove_code();
		
		stamp = new Oval();

		imageUnique = null;
		imageSequence = null;
		
		fresh_synth = null;
		hit_synth   = null;
		exit_synth  = null;
		mng_Stream.remove_code();
	}

	protected boolean validate_msg(List<Object> args) {
		int len = args.size();
		return len >= 2 && (len % 2) == 0;
	}

	protected void parsePair(Object arg, Object name, String key) {
		if (name.equals(key)) {
			if (arg instanceof String) {
				if (key.equals("img")) {
					imageUnique = new ImageUnique();
					imageUnique.load((String) arg);
				}
				else if (key.equals("imgs")) {
					imageSequence = new ImageSequence();
					imageSequence.load((String) arg);
				}
				else if (key.equals("fresh")) {
					fresh_synth = new ScSynthSender((String) arg);
				}
				else if (key.equals("hit")) {
					hit_synth = new ScSynthSender((String) arg);
				}
				else if (key.equals("exit")) {
					exit_synth = new ScSynthSender((String) arg);
				}
			}
//			if (arg instanceof Float) {
//				if (key.equals("shape")) {
//					switch((int)(Math.round((float)arg))) {
//					case 0:
//						if(!(stamp instanceof Oval))
//							stamp = new Oval();
//						break;
//					case 1:
//						if(!(stamp instanceof Rect))
//							stamp = new Rect();
//						break;
//					case 2:
//						if(!(stamp instanceof Triangle))
//							stamp = new Triangle();
//						break;					
//					case 3:
//						if(!(stamp instanceof Words))
//							stamp = new Words();
//						break;					
//					}
//				}
//			}
		}
	}

	public void reload(List<Object> args) {
		if (!validate_msg(args))
			return;

		if(preserved) {
//			System.out.println(name + " resize C " + cell_list.size() + " " + 0 + " " + preserved);
			clear();
			preserved = false;
//			System.out.println(name + " resize D " + cell_list.size() + " " + 0  + " "+ preserved);
		}
		set_defaults();

		final int stop = args.size() - 1;
		for (int ax = 2; ax < stop; ax += 2) {
			Object name = args.get(ax);
			Object arg = args.get(ax + 1);
			FnManagement.parsePair(arg, name, "n", mng_N);
			FnManagement.parsePair(arg, name, "shape", mng_Shape);
			FnManagement.parsePair(arg, name, "br", mng_Birthrate);
			FnManagement.parsePair(arg, name, "dec", mng_Decay);
			FnManagement.parsePair(arg, name, "x", mng_PosX);
			FnManagement.parsePair(arg, name, "y", mng_PosY);
			FnManagement.parsePair(arg, name, "w", mng_DimW);
			FnManagement.parsePair(arg, name, "h", mng_DimH);
			FnManagement.parsePair(arg, name, "rot", mng_Rot);
			FnManagement.parsePair(arg, name, "H", mng_ColH);
			FnManagement.parsePair(arg, name, "S", mng_ColS);
			FnManagement.parsePair(arg, name, "V", mng_ColV);
			FnManagement.parsePair(arg, name, "A", mng_ColA);
			FnManagement.parsePair(arg, name, "la", mng_LayA);
			FnManagement.parsePair(arg, name, "lb", mng_LayB);
			FnManagement.parsePair(arg, name, "ltx", mng_LayTx);
			FnManagement.parsePair(arg, name, "lty", mng_LayTy);
			FnManagement.parsePair(arg, name, "lsx", mng_LaySx);
			FnManagement.parsePair(arg, name, "lsy", mng_LaySy);
			FnManagement.parsePair(arg, name, "lrot", mng_LayRot);
//			parsePair(arg, name, "shape");
			parsePair(arg, name, "fresh");
			parsePair(arg, name, "hit");
			parsePair(arg, name, "exit");
			FnManagement.parsePair(arg, name, "stream", mng_Stream);
			parsePair(arg, name, "img");
			parsePair(arg, name, "imgs");
		}
		blend_uniforms.reload(args);

		// NICE calculate per frame, instead only once during reload
		switch((int) mng_Shape.eval(Cell.dummy, this)) {
		case 0:
			if(!(stamp instanceof Oval))
				stamp = new Oval();
			break;
		case 1:
			if(!(stamp instanceof Rect))
				stamp = new Rect();
			break;
		case 2:
			if(!(stamp instanceof Triangle))
				stamp = new Triangle();
			break;					
		case 3:
			if(!(stamp instanceof Words))
				stamp = new Words();
			break;					
		}
	}

	protected static int id_counter = 1;

	protected Cell createCell(int index) {
//		System.out.println("createCell preserved " + name + " " + preserved);
		return new Cell(index, id_counter++, this);
	}

	public void clear() {
		Jean.habiat.decay_factory.strategy(cell_list, 0).decay(cell_list, 0);
	}

	public void resize() {
		int next_size = (int) mng_N.eval(Cell.dummy, this);

//		System.out.println(name + " resize A " + cell_list.size() + " " + next_size + " " + preserved);
		birthrate = mng_Birthrate.eval(Cell.dummy, this);
		Jean.habiat.decay_factory.strategy(cell_list, next_size).decay(cell_list, next_size);
		Jean.habiat.grow_factory.strategy(cell_list, next_size).grow(cell_list, this);
//		System.out.println(name + " resize B " + cell_list.size() + " " + next_size);
	}

	public boolean match(final String pattern) {
		return this.name.equals(pattern);
	}

	public void update_pre(Cell cell) {
	}

	public void update(Cell cell) {
		try {
			double old_x = cell.pos_x;
			double old_y = cell.pos_y;
			Cell next = (Cell) cell.clone();
			next.pos_x = mng_PosX.eval(cell, this);
			next.pos_y = mng_PosY.eval(cell, this);
			next.delta_x = next.pos_x - old_x;
			next.delta_y = next.pos_y - old_y;
			eval_wh(next, cell);
			eval_hsva(next, cell);

			cell.patchState(next);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

	public void update_post(Cell cell) {
		if(cell.is_fresh) {
			cell.is_fresh = false;
			if(characteristic == C_CURSOR && fresh_synth != null)
				fresh_synth.send_synth_new(this, this, cell, cell);
		}
	}

	protected void eval_wh(Cell next, Cell cell) {
		next.dim_w = mng_DimW.eval(cell, this);
		next.dim_h = mng_DimH.eval(cell, this);
		double rot = mng_Rot.eval(cell, this);
		next.sin_rot = (float) Math.sin(rot);
		next.cos_rot = (float) Math.cos(rot);
	}

	protected void eval_hsva(Cell next, Cell cell) {
		next.color.h = (float) mng_ColH.eval(cell, this);
		next.color.s = (float) mng_ColS.eval(cell, this);
		next.color.v = (float) mng_ColV.eval(cell, this);
		next.color.a = (float) mng_ColA.eval(cell, this);
		next.color.update_rgb();
	}

	public void prepare_layer() {
		blend_mode = (int) mng_LayB.eval(Cell.dummy, this);
		layer_alpha = JMath.clamp_0_1((float)mng_LayA.eval(Cell.dummy, this));
		if(blend_mode < 0)
			blend_mode = 0;
		layer_tx = (float)mng_LayTx.eval(Cell.dummy, this);
		layer_ty = (float)mng_LayTy.eval(Cell.dummy, this);
		layer_sx = (float)mng_LaySx.eval(Cell.dummy, this);
		layer_sy = (float)mng_LaySy.eval(Cell.dummy, this);
		layer_rot = mng_LayRot.eval(Cell.dummy, this);

		if(imageSequence != null) {
			imageSequence.init_peer();
			imageSequence.draw_background(layer_alpha, layer_tx, layer_ty);
		}
		if(imageUnique != null) {
			imageUnique.init_peer();
			imageUnique.draw_background(layer_alpha, layer_tx, layer_ty);
		}
		stream = (int) mng_Stream.eval(Cell.dummy, this);
	}

	public void draw(Cell cell) {
		if(stamp != null && cell != null && cell.isVisible()) {
			stamp.update(cell);
//			Jean.binding.drawMesh(stamp.getPositions());
		}
	}

	public double x_up(int id) {
		if (up != null) {
			final int size = up.cell_list.size();
			if (size > 0)
				return up.cell_list.get(id % size).pos_x;
		}
		return 0.0;
	}

	public double x_upup(int id) {
		if (up != null) {
			return up.x_up(id);
		}
		return 0.0;
	}

	public double x_upupup(int id) {
		if (up != null) {
			return up.x_upup(id);
		}
		return 0.0;
	}

	public double y_up(int id) {
		if (up != null) {
			final int size = up.cell_list.size();
			if (size > 0)
				return up.cell_list.get(id % size).pos_y;
		}
		return 0.0;
	}

	public double y_upup(int id) {
		if (up != null) {
			return up.y_up(id);
		}
		return 0.0;
	}

	public double y_upupup(int id) {
		if (up != null) {
			return up.y_upup(id);
		}
		return 0.0;
	}

	public void reset() {
		for (Iterator<Cell> cell_iter = cell_list.iterator(); cell_iter.hasNext();) {
			Cell cell = cell_iter.next();
			cell.reset();
		}
		
	}

	@Override
	public String toString() {
		return "Cluster [cluster_id=" + cluster_index + ", name=" + name + ", characteristic=" + characteristic
				+ ", cell_list.size=" + cell_list.size() + ", stamp=" + stamp + ", hit_synth=" + hit_synth + ", exit_synth=" + exit_synth + ", up=" + up + "]";
	}

	public String getName() {
		return name;
	}

}
