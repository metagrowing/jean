package jean.model;

import static org.junit.Assert.assertEquals;
//import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import jean.application.Jean;
import jean.model.Decay.AllEoL;
import jean.model.Decay.EoL;
import jean.model.Decay.Limit;

class DecayTest {

	@Test
	void testStrategy1() {
		Decay d = new Decay();
		assertEquals(AllEoL.class, d.strategy(null, 0).getClass());
		
		List<Cell> cl = new ArrayList<Cell>();
		cl.add(Cell.dummy);
		assertEquals(AllEoL.class, d.strategy(cl, 0).getClass());
		assertEquals(EoL.class, d.strategy(cl, 1).getClass());
		assertEquals(EoL.class, d.strategy(cl, 2).getClass());
		cl.add(Cell.dummy);
		assertEquals(Limit.class, d.strategy(cl, 1).getClass());
	}

	@Test
	void testStrategy2() {
		Jean.habiat = new Habiat();
		Jean.habiat.culster_list_lock.lock();
		try {
			Decay d = new Decay();
			List<Cell> cl = new ArrayList<Cell>();
				cl.add(Cell.dummy);
				int n_max = 0;
				d.strategy(cl, n_max).decay(cl, n_max);
				assertEquals(n_max, cl.size());
		} finally {
			Jean.habiat.culster_list_lock.unlock();
		}
	}

	@Test
	void testStrategy3() {
		Jean.habiat = new Habiat();
		Jean.habiat.culster_list_lock.lock();
		try {
			Decay d = new Decay();
			List<Cell> cl = new ArrayList<Cell>();
			Cell dummy0 = new Cell(0, 0, Cluster.dummy);
			cl.add(dummy0);
			Cell dummy1 = new Cell(1, 1, Cluster.dummy);
			cl.add(dummy1);
			dummy1.decay = -1;
			int n_max = 2;
			d.strategy(cl, n_max).decay(cl, n_max);
			assertEquals(1, cl.size());
		} finally {
			Jean.habiat.culster_list_lock.unlock();
		}

	}

	@Test
	void testStrategy4() {
		Jean.habiat = new Habiat();
		Jean.habiat.culster_list_lock.lock();
		try {
			Decay d = new Decay();
			List<Cell> cl = new ArrayList<Cell>();
			Cell dummy0 = new Cell(0, 0, Cluster.dummy);
			cl.add(dummy0);
			dummy0.decay = 0.1;
			
			Cell dummy1 = new Cell(1, 1, Cluster.dummy);
			cl.add(dummy1);
			dummy1.decay = -1;
			
			Cell dummy3 = new Cell(3, 3, Cluster.dummy);
			cl.add(dummy3);
			dummy3.decay = 0.3;
			
			Cell dummy2 = new Cell(2, 2, Cluster.dummy);
			cl.add(dummy2);
			dummy2.decay = 0.2;
			
			int n_max = 1;
			d.strategy(cl, n_max).decay(cl, n_max);
			assertEquals(n_max, cl.size());
			assertEquals(0.3, cl.get(0).decay, 0.001);
			assertEquals(3, cl.get(0).id);
		} finally {
			Jean.habiat.culster_list_lock.unlock();
		}

	}

	@Test
	void testStrategy5() {
		Jean.habiat = new Habiat();
		Jean.habiat.culster_list_lock.lock();
		try {
			Decay d = new Decay();
			List<Cell> cl = new ArrayList<Cell>();
			Cell dummy0 = new Cell(0, 0, Cluster.dummy);
			cl.add(dummy0);
			dummy0.decay = 0.1;
			
			Cell dummy1 = new Cell(1, 1, Cluster.dummy);
			cl.add(dummy1);
			dummy1.decay = 0.05;
			
			Cell dummy3 = new Cell(3, 3, Cluster.dummy);
			cl.add(dummy3);
			dummy3.decay = 0.3;
			
			Cell dummy2 = new Cell(2, 2, Cluster.dummy);
			cl.add(dummy2);
			dummy2.decay = 0.2;
			
			int n_max = 2;
			d.strategy(cl, n_max).decay(cl, n_max);
			assertEquals(n_max, cl.size());
			assertEquals(0.3, cl.get(0).decay, 0.001);
			assertEquals(3, cl.get(0).id);
			assertEquals(0.2, cl.get(1).decay, 0.001);
			assertEquals(2, cl.get(1).id);
		} finally {
			Jean.habiat.culster_list_lock.unlock();
		}

	}

}
