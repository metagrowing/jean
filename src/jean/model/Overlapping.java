package jean.model;

public class Overlapping {
// https://stackoverflow.com/questions/1585459/whats-the-most-efficient-way-to-detect-triangle-triangle-intersections
// http://thirdpartyninjas.com/blog/2008/10/07/line-segment-intersection/
// https://blackpawn.com/texts/pointinpoly/default.html

//	def line_intersect2(v1,v2,v3,v4):
//    '''
//    judge if line (v1,v2) intersects with line(v3,v4)
//    '''
//    d = (v4[1]-v3[1])*(v2[0]-v1[0])-(v4[0]-v3[0])*(v2[1]-v1[1])
//    u = (v4[0]-v3[0])*(v1[1]-v3[1])-(v4[1]-v3[1])*(v1[0]-v3[0])
//    v = (v2[0]-v1[0])*(v1[1]-v3[1])-(v2[1]-v1[1])*(v1[0]-v3[0])
//    if d<0:
//        u,v,d= -u,-v,-d
//    return (0<=u<=d) and (0<=v<=d)

//  judge if line (v1,v2) intersects with line(v3,v4)
	public static boolean line_intersect(float[] v1, float[] v2, float[] v3, float[] v4) {
		float v1_x = v1[0];
		float v1_y = v1[1];
		float v2_x = v2[0];
		float v2_y = v2[1];
		float v3_x = v3[0];
		float v3_y = v3[1];
		float v4_x = v4[0];
		float v4_y = v4[1];
		float d = (v4_y - v3_y) * (v2_x - v1_x) - (v4_x - v3_x) * (v2_y - v1_y);
		float u = (v4_x - v3_x) * (v1_y - v3_y) - (v4_y - v3_y) * (v1_x - v3_x);
		float v = (v2_x - v1_x) * (v1_y - v3_y) - (v2_y - v1_y) * (v1_x - v3_x);
		if (d < 0) {
			u = -u;
			v = -v;
			d = -d;
		}
		return (0 <= u) && (u <= d) && (0 <= v) && (v <= d);
	}

//	def point_in_triangle2(A = [];B,C,P):
//    v0 = [C[0]-A[0], C[1]-A[1]]
//    v1 = [B[0]-A[0], B[1]-A[1]]
//    v2 = [P[0]-A[0], P[1]-A[1]]
//    cross = lambda u,v: u[0]*v[1]-u[1]*v[0]
//    u = cross(v2,v0)
//    v = cross(v1,v2)
//    d = cross(v1,v0)
//    if d<0:
//        u,v,d = -u,-v,-d
//    return u>=0 and v>=0 and (u+v) <= d

	public static float cross(float u_x, float u_y, float v_x, float v_y) {
		return u_x * v_y - u_y * v_x;
	}

	public static boolean point_in_triangle(float[] A, float[] B, float[] C, float[] P) {
		float A_x = A[0];
		float A_y = A[1];
		float B_x = B[0];
		float B_y = B[1];
		float C_x = C[0];
		float C_y = C[1];
		float P_x = P[0];
		float P_y = P[1];
		final float v0_x = C_x - A_x;
		final float v0_y = C_y - A_y;
		final float v1_x = B_x - A_x;
		final float v1_y = B_y - A_y;
		final float v2_x = P_x - A_x;
		final float v2_y = P_y - A_y;
		float u = cross(v2_x, v2_y, v0_x, v0_y);
		float v = cross(v1_x, v1_y, v2_x, v2_y);
		float d = cross(v1_x, v1_y, v0_x, v0_y);
		if (d < 0) {
			u = -u;
			v = -v;
			d = -d;
		}
		return u >= 0 && v >= 0 && (u + v) <= d;
	}

//	def tri_intersect2(t1, t2):
//	    '''
//	    judge if two triangles in a plane intersect 
//	    '''
//	    if line_intersect2(t1[0],t1[1],t2[0],t2[1]): return True
//	    if line_intersect2(t1[0],t1[1],t2[0],t2[2]): return True
//	    if line_intersect2(t1[0],t1[1],t2[1],t2[2]): return True
//	    if line_intersect2(t1[0],t1[2],t2[0],t2[1]): return True
//	    if line_intersect2(t1[0],t1[2],t2[0],t2[2]): return True
//	    if line_intersect2(t1[0],t1[2],t2[1],t2[2]): return True
//	    if line_intersect2(t1[1],t1[2],t2[0],t2[1]): return True
//	    if line_intersect2(t1[1],t1[2],t2[0],t2[2]): return True
//	    if line_intersect2(t1[1],t1[2],t2[1],t2[2]): return True
//	    inTri = True 
//	    inTri = inTri and point_in_triangle2(t1[0],t1[1],t1[2], t2[0])
//	    inTri = inTri and point_in_triangle2(t1[0],t1[1],t1[2], t2[1])
//	    inTri = inTri and point_in_triangle2(t1[0],t1[1],t1[2], t2[2])
//	    if inTri == True: return True
//	    inTri = True
//	    inTri = inTri and point_in_triangle2(t2[0],t2[1],t2[2], t1[0])
//	    inTri = inTri and point_in_triangle2(t2[0],t2[1],t2[2], t1[1])
//	    inTri = inTri and point_in_triangle2(t2[0],t2[1],t2[2], t1[2])
//	    if inTri == True: return True
//	    return False

//    judge if two triangles in a plane intersect 
	public static boolean tri_intersect(float[][] t1, float[][] t2) {
	    if(line_intersect(t1[0],t1[1],t2[0],t2[1])) return true;
	    if(line_intersect(t1[0],t1[1],t2[0],t2[2])) return true;
	    if(line_intersect(t1[0],t1[1],t2[1],t2[2])) return true;
	    if(line_intersect(t1[0],t1[2],t2[0],t2[1])) return true;
	    if(line_intersect(t1[0],t1[2],t2[0],t2[2])) return true;
	    if(line_intersect(t1[0],t1[2],t2[1],t2[2])) return true;
	    if(line_intersect(t1[1],t1[2],t2[0],t2[1])) return true;
	    if(line_intersect(t1[1],t1[2],t2[0],t2[2])) return true;
	    if(line_intersect(t1[1],t1[2],t2[1],t2[2])) return true;
	    boolean inTri = true;
	    inTri = inTri && point_in_triangle(t1[0],t1[1],t1[2], t2[0]);
	    inTri = inTri && point_in_triangle(t1[0],t1[1],t1[2], t2[1]);
	    inTri = inTri && point_in_triangle(t1[0],t1[1],t1[2], t2[2]);
	    if(inTri) return true;
	    inTri = true;
	    inTri = inTri && point_in_triangle(t2[0],t2[1],t2[2], t1[0]);
	    inTri = inTri && point_in_triangle(t2[0],t2[1],t2[2], t1[1]);
	    inTri = inTri && point_in_triangle(t2[0],t2[1],t2[2], t1[2]);
	    return inTri;
	}
}
