package jean.model;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import jean.application.Jean;

public class Decay {

	private Limit limit = new Limit();
	private AllEoL allEol = new AllEoL();
	private EoL eol = new EoL();
	
	class Limit implements IDecayStrategy {
		@Override
		public void decay(List<Cell> cl, int n_max) {
			try {
				int next_size = Math.max(n_max, 0);
				eol.decay(cl, next_size);

				if (cl.size() > next_size) {
					cl.sort(new Comparator<Cell>() {
						public int compare(Cell o1, Cell o2) {
							return Double.compare(o2.decay, o1.decay);
						}
					});
					while (cl.size() > next_size) {
						int tail = cl.size() - 1;
						Cell lost = cl.get(0);
						cl.remove(tail);
						Jean.habiat.forget(lost);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class EoL implements IDecayStrategy {
		@Override
		public void decay(List<Cell> cl, int n_max) {
			try {
				for (Iterator<Cell> cell_iter = cl.iterator(); cell_iter.hasNext();) {
					Cell c = cell_iter.next();
					if(c.decay < 0.0) {
						Jean.habiat.forget(c);
						cell_iter.remove();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	class AllEoL implements IDecayStrategy {
		@Override
		public void decay(List<Cell> cl, int n_max) {
			try {
				for (Iterator<Cell> cell_iter = cl.iterator(); cell_iter.hasNext();) {
					Jean.habiat.forget(cell_iter.next());
				}
				cl.clear();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public IDecayStrategy strategy(List<Cell> cl, int n_max) {
		if(n_max <= 0) return allEol;
		if(cl.size() > n_max) return limit;
		return eol;
	}

}
