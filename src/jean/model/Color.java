package jean.model;

import jean.noise.JMath;

public class Color implements Cloneable {
	public float h;
	public float s;
	public float v;
	public float a;
	public float r;
	public float g;
	public float b;

	public Color(float h, float s, float v, float a) {
		super();
		this.h = h;
		this.s = s;
		this.v = v;
		this.a = a;
		update_rgb();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Color c = (Color) super.clone();
		return c;
	}

	public void update_rgb()
	{
		float v01 = JMath.clamp_0_1(v);
	    if(s <= 0.0) {
	        r = g = b = v01;
	    }
	    else {
		    float sector = 6.0f * JMath.fractional_0_1(h);
		    int i = (int)Math.floor(sector);
		    float fractional = sector - i;
			float s01 = JMath.clamp_0_1(s);
		    float p = v01 * (1.0f - s01);
		    float q = v01 * (1.0f - (s01 * fractional));
		    float t = v01 * (1.0f - (s01 * (1.0f - fractional)));

		    switch(i) {
		    case 0:
		        r = v01;
		        g = t;
		        b = p;
		        break;
		    case 1:
		        r = q;
		        g = v01;
		        b = p;
		        break;
		    case 2:
		        r = p;
		        g = v01;
		        b = t;
		        break;

		    case 3:
		        r = p;
		        g = q;
		        b = v01;
		        break;
		    case 4:
		        r = t;
		        g = p;
		        b = v01;
		        break;
		    default:
		        r = v01;
		        g = p;
		        b = q;
		        break;
		    }
	        r = JMath.clamp_0_1(r);
	        g = JMath.clamp_0_1(g);
	        b = JMath.clamp_0_1(b);
	    }

	}

	@Override
	public String toString() {
		return "Color [h=" + h + ", s=" + s + ", v=" + v + ", r=" + r + ", g=" + g + ", b=" + b + ", a=" + a + "]";
	}
}
