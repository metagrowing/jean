package jean.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OverlappingTest {

	@Test
	void testTri_intersect0() {
		float[][] tri1 = new float[3][2];
		tri1[0][0] = 0.0f; tri1[0][1] = 0.0f;
		tri1[1][0] = 0.0f; tri1[1][1] = 0.0f;
		tri1[2][0] = 0.0f; tri1[2][1] = 0.0f;
		float[][] tri2 = new float[3][2];
		tri2[0][0] = 0.0f; tri2[0][1] = 0.0f;
		tri2[1][0] = 0.0f; tri2[1][1] = 0.0f;
		tri2[2][0] = 0.0f; tri2[2][1] = 0.0f;
		
		assertEquals(true, Overlapping.tri_intersect(tri1, tri2));
	}

	@Test
	void testTri_intersect1() {
		float[][] tri1 = new float[][] {
			{0.0f, 1.0f},
			{-2.0f, 0.0f},
			{0.0f, -1.0f},
		};
		float[][] tri2 = new float[][] {
			{ 0.0f,  1.0f},
			{-2.0f,  0.0f},
			{ 0.0f, -1.0f},
		};		
		assertEquals(true, Overlapping.tri_intersect(tri1, tri2));
	}

	@Test
	void testTri_intersect2x() {
		float[][] tri1 = new float[][] {
			{ 0.0f,  1.0f},
			{-2.0f,  0.0f},
			{ 0.0f, -1.0f},
		};
		float dx = -0.5f;
		float[][] tri2 = new float[][] {
			{ 0.0f+dx,  1.0f},
			{-2.0f+dx,  0.0f},
			{ 0.0f+dx, -1.0f},
		};		
		assertEquals(true, Overlapping.tri_intersect(tri1, tri2));
	}

	@Test
	void testTri_intersect2y() {
		float dy = -0.5f;
		float[][] tri1 = new float[][] {
			{ 0.0f,  1.0f+dy},
			{-2.0f,  0.0f+dy},
			{ 0.0f, -1.0f+dy},
		};
		float[][] tri2 = new float[][] {
			{ 0.0f,  1.0f},
			{-2.0f,  0.0f},
			{ 0.0f, -1.0f},
		};		
		assertEquals(true, Overlapping.tri_intersect(tri1, tri2));
	}

	@Test
	void testTri_intersect3() {
		float[][] tri1 = new float[][] {
			{ 0.0f,  1.0f},
			{-2.0f,  0.0f},
			{ 0.0f, -1.0f},
		};
		float dx = 2.01f;
		float[][] tri2 = new float[][] {
			{ 0.0f+dx,  1.0f},
			{-2.0f+dx,  0.0f},
			{ 0.0f+dx, -1.0f},
		};		
		assertEquals(false, Overlapping.tri_intersect(tri1, tri2));
	}

	@Test
	void testTri_intersect4() {
		float[][] tri1 = new float[][] {
			{ 0.0f,  1.0f},
			{-2.0f,  0.0f},
			{ 0.0f, -1.0f},
		};
		float dx = 1.99f;
		float[][] tri2 = new float[][] {
			{ 0.0f+dx,  1.0f},
			{-2.0f+dx,  0.0f},
			{ 0.0f+dx, -1.0f},
		};		
		assertEquals(true, Overlapping.tri_intersect(tri1, tri2));
	}

	@Test
	void testTri_intersect5() {
		float[][] tri1 = new float[][] {
			{ 0.0f,  1.0f},
			{-2.0f,  0.0f},
			{ 0.0f, -1.0f},
		};
		float sx = 0.99f;
		float sy = 0.99f;
		float[][] tri2 = new float[][] {
			{ 0.0f*sx,  1.0f*sy},
			{-2.0f*sx,  0.0f*sy},
			{ 0.0f*sx, -1.0f*sy},
		};		
		assertEquals(true, Overlapping.tri_intersect(tri1, tri2));
	}

	@Test
	void testTri_intersect6() {
		float[][] tri1 = new float[][] {
			{ 1.0f,  1.0f},
			{-1.0f,  1.0f},
			{ 2.0f,  0.0f},
		};
		float[][] tri2 = new float[][] {
			{ 1.5f, -9.0f},
			{ 1.0f,  9.0f},
			{ 0.0f, -1.0f},
		};		
		assertEquals(true, Overlapping.tri_intersect(tri1, tri2));
	}

}
