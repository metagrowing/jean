package jean.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Cell implements Cloneable {
	public int id;
	public int index;
	public Cluster cluster;
	public int cluster_index;
	public boolean over;
	public boolean outside;
	public double pos_x;
	public double pos_y;
	public double delta_x;
	public double delta_y;
	public double dim_w;
	public double dim_h;
	public double sin_rot;
	public double cos_rot;
	public Color color;
	public double decay = 1.0;
	
	private float[][] tri1 = new float[3][2];
	private float[][] tri2 = new float[3][2];
	
	public static final int UNKNOWN = 0;
	public static final int ENTERED = 1;
	public static final int LEFT    = 2;
	
    private Map<Integer, Integer> watch; // flat copy when cloned
	public int frame;
	boolean is_fresh;

	public static Cell dummy = new Cell();
	
	private Cell() {
	}

	public Cell(int index, int id, Cluster cluster) {
		super();
		this.index = index;
		this.id = id;
		this.cluster = cluster;
		this.cluster_index = cluster.cluster_index;
		this.dim_w = 5.0;
		this.dim_h = 5.0;
		this.sin_rot = 0.0;
		this.cos_rot = 1.0;
		this.color = new Color(0.3f, 1.0f, 1.0f, 1.0f); 
		this.watch = cluster.characteristic == Cluster.C_CALVE ? new HashMap<Integer, Integer>() : null;
		this.frame = 0;
		this.is_fresh = true;
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		Cell c = (Cell) super.clone();
		c.color = (Color) this.color.clone();
		return c;
	}

	public void patchState(Cell other) throws CloneNotSupportedException {
		this.pos_x = other.pos_x;
		this.pos_y = other.pos_y;
		this.delta_x = other.delta_x;
		this.delta_y = other.delta_y;
		this.dim_w = other.dim_w;
		this.dim_h = other.dim_h;
//		this.rot = other.rot;
		this.sin_rot = other.sin_rot;
		this.cos_rot = other.cos_rot;
		this.color.h = other.color.h;
		this.color.s = other.color.s;
		this.color.v = other.color.v;
		this.color.a = other.color.a;
		this.color.update_rgb();
		this.decay = other.decay;
	}

	private static float transform_x(Cell c, float x0, float y0) {
	    float x = x0 * (float)c.dim_w;
	    float y = y0 * (float)c.dim_h;
	    x = (float) (x * c.cos_rot - y * c.sin_rot);
	    return x + (float)c.pos_x;
	}

	private static float transform_y(Cell c, float x0, float y0) {
	    float x = x0 * (float)c.dim_w;
	    float y = y0 * (float)c.dim_h;
	    y = (float) (x * c.sin_rot + y * c.cos_rot);
	    return y + (float)c.pos_y;
	}

	public void updateTriOverlaping(Cell other) {
		//TODO use AABB
//		long t0 = System.nanoTime();
		over = false;
		float[] this_pos = cluster.stamp.collisionShape();
		float[] other_pos = other.cluster.stamp.collisionShape();
		int i = 0;
		int ri = 0;
		while(!over && i < this_pos.length / 9) {
			float x1 = this_pos[ri++];
			float y1 = this_pos[ri++];
			tri1[0][0] = transform_x(this, x1, y1);
			tri1[0][1] = transform_y(this, x1, y1);
			ri++;
			x1 = this_pos[ri++];
			y1 = this_pos[ri++];
			tri1[1][0] = transform_x(this, x1, y1);
			tri1[1][1] = transform_y(this, x1, y1);
			ri++;
			x1 = this_pos[ri++];
			y1 = this_pos[ri++];
			tri1[2][0] = transform_x(this, x1, y1);
			tri1[2][1] = transform_y(this, x1, y1);
			ri++;
			int j = 0;
			int rj = 0;
			while(!over && j < other_pos.length / 9) {
				float x2 = other_pos[rj++];
				float y2 = other_pos[rj++];
				tri2[0][0] = transform_x(other, x2, y2);
				tri2[0][1] = transform_y(other, x2, y2);
				rj++;
				x2 = other_pos[rj++];
				y2 = other_pos[rj++];
				tri2[1][0] = transform_x(other, x2, y2);
				tri2[1][1] = transform_y(other, x2, y2);
				rj++;
				x2 = other_pos[rj++];
				y2 = other_pos[rj++];
				tri2[2][0] = transform_x(other, x2, y2);
				tri2[2][1] = transform_y(other, x2, y2);
				rj++;
				
				over = Overlapping.tri_intersect(tri1, tri2);
				++j;
			}
			++i;
		}
		
		outside = !over;
//		long t1 = System.nanoTime();
//		System.out.println("elapsed " + (t1 - t0));
	}

	public void updateOverlaping(Cell other) {
		//TODO use AABB
//		final double half = 0.5;
//		final double half_save  = 0.55;
		final double half = 1.0;
		final double half_save  = 1.05;
		final double r1_hit_left = pos_x-half*dim_w;
		final double r1_hit_right = pos_x+half*dim_w;
		final double r1_hit_top = pos_y+half*dim_h;
		final double r1_hit_bottom = pos_y-half*dim_h;
		
		final double r2_hit_left = other.pos_x-half*dim_w;
		final double r2_hit_right = other.pos_x+half*other.dim_w;
		final double r2_hit_top = other.pos_y+half*other.dim_h;
		final double r2_hit_bottom = other.pos_y-half*other.dim_h;
		
		over = true;
		if (r1_hit_left > r2_hit_right || r1_hit_right < r2_hit_left) {
			over = false;
		}

		if (r1_hit_top < r2_hit_bottom || r1_hit_bottom > r2_hit_top) {
			over = false;
		}

		final double r1_out_left = pos_x-half_save*dim_w;
		final double r1_out_right = pos_x+half_save*dim_w;
		final double r1_out_top = pos_y+half_save*dim_h;
		final double r1_out_bottom = pos_y-half_save*dim_h;
		
		final double r2_out_left = other.pos_x-half_save*dim_w;
		final double r2_out_right = other.pos_x+half_save*other.dim_w;
		final double r2_out_top = other.pos_y+half_save*other.dim_h;
		final double r2_out_bottom = other.pos_y-half_save*other.dim_h;
		
		outside = false;
		if (r1_out_left > r2_out_right || r1_out_right < r2_out_left) {
			outside = true;
		}
		if (r1_out_top < r2_out_bottom || r1_out_bottom > r2_out_top) {
			outside = true;
		}
	}

	public void updateState(Cluster calve_cluster, Cluster cursor_cluster, Cell cursor_cell) {
		if(cluster.characteristic != Cluster.C_CALVE)
			return;
		
		int state = watch.containsKey(cursor_cell.id) ? watch.get(cursor_cell.id) : UNKNOWN;

		switch(state) {
		case UNKNOWN:
			state = over ? ENTERED : LEFT;
			break;
		case ENTERED:
			if(outside) {
				state = LEFT;
//				System.out.println("left    " + this);
				if(calve_cluster.exit_synth != null)
					calve_cluster.exit_synth.send_synth_new(calve_cluster, cursor_cluster, this, cursor_cell);
			}
			break;
		case LEFT:
			if(over) {
				state = ENTERED;
//				System.out.println("entered " + this);
				if(calve_cluster.hit_synth != null)
					calve_cluster.hit_synth.send_synth_new(calve_cluster, cursor_cluster, this, cursor_cell);
			}
			break;
		}

		watch.put(cursor_cell.id, state);
	}

	public void forget_watch(Cell forget_cell) {
		if(watch != null)
			watch.remove(forget_cell.id);
	}

	public double has_entered() {
		if (watch != null) {
			Collection<Integer> v = watch.values();
			return v.contains(ENTERED) ? 1.0 : 0.0;
		}
		return 0.0;
	}
	
	public boolean isVisible() {
		return dim_w > 0.0 && dim_h > 0.0;
	}

	public void reset() {
	}

	@Override
	public String toString() {
		return "Cell [id=" + id + ", index=" + index + ", cluster_index=" + cluster_index + ", over="
				+ over + ", outside=" + outside + ", pos_x=" + pos_x + ", pos_y=" + pos_y  + ", delta_x=" + delta_x + ", delta_y=" + delta_y + ", dim_w=" + dim_w
				+ ", dim_h=" + dim_h + ", color=" + color + "]";
	}

}
