package jean.model;

import java.util.List;

public interface IDecayStrategy {
	void decay(List<Cell> cl, int n_max);
}
