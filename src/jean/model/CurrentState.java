package jean.model;

import java.util.Iterator;

import jean.OSCout.ScStreamSender;
import jean.application.Jean;
import jean.l2asm.FnManagement;

public class CurrentState {
	public static boolean need_full_clear = true;
	public static int frame = 0;
	public static double u0, u1, u2, u3, u4, u5, u6, u7;
	public static double t0, t1, t2, t3, t4, t5, t6, t7;
	public static FnManagement mng_t0 = new FnManagement(0);
	public static FnManagement mng_t1 = new FnManagement(0);
	public static FnManagement mng_t2 = new FnManagement(0);
	public static FnManagement mng_t3 = new FnManagement(0);
	public static FnManagement mng_t4 = new FnManagement(0);
	public static FnManagement mng_t5 = new FnManagement(0);
	public static FnManagement mng_t6 = new FnManagement(0);
	public static FnManagement mng_t7 = new FnManagement(0);
	
	public static ScStreamSender oscStreamSender;

	public CurrentState() {
		super();
		oscStreamSender = new ScStreamSender();
	}

	public void collide() {
		Jean.habiat.culster_list_lock.lock();
	    try {
			calulate_collide();
	    } finally {
	    	Jean.habiat.culster_list_lock.unlock();
	    }
	}

	private void calulate_collide() {
		try {
			for (int ica = 0; ica < Jean.habiat.getNumClusters(Cluster.C_CALVE); ++ica) {
				Cluster calve_cluster = Jean.habiat.getCluster(Cluster.C_CALVE, ica);
				if (calve_cluster != null) {
					for (int icu = 0; icu < Jean.habiat.getNumClusters(Cluster.C_CURSOR); ++icu) {
						Cluster cursor_cluster = Jean.habiat.getCluster(Cluster.C_CURSOR, icu);
						if (cursor_cluster != null) {
							for (Iterator<Cell> calves_iter = calve_cluster.cell_list.iterator(); calves_iter.hasNext();) {
								Cell calve_cell = calves_iter.next();
								for (Iterator<Cell> cursor_iter = cursor_cluster.cell_list.iterator(); cursor_iter.hasNext();) {
									Cell cursor_cell = cursor_iter.next();
									calve_cell.updateTriOverlaping(cursor_cell);
									calve_cell.updateState(calve_cluster, cursor_cluster, cursor_cell);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void update() {
		Jean.habiat.culster_list_lock.lock();
		try {
			t0 = mng_t0.eval(Cell.dummy, Cluster.dummy);
			t1 = mng_t1.eval(Cell.dummy, Cluster.dummy);
			t2 = mng_t2.eval(Cell.dummy, Cluster.dummy);
			t3 = mng_t3.eval(Cell.dummy, Cluster.dummy);
			t4 = mng_t4.eval(Cell.dummy, Cluster.dummy);
			t5 = mng_t5.eval(Cell.dummy, Cluster.dummy);
			t6 = mng_t6.eval(Cell.dummy, Cluster.dummy);
			t7 = mng_t7.eval(Cell.dummy, Cluster.dummy);

			for (Iterator<Cluster> cluster_iter = Jean.habiat.culster_pipeline.iterator(); cluster_iter.hasNext();) {
				Cluster cluster = cluster_iter.next();

				cluster.resize();
				for (Iterator<Cell> cell_iter = cluster.cell_list.iterator(); cell_iter.hasNext();) {
					cluster.update_pre(cell_iter.next());
				}
				for (Iterator<Cell> cell_iter = cluster.cell_list.iterator(); cell_iter.hasNext();) {
					cluster.update(cell_iter.next());
				}
				for (Iterator<Cell> cell_iter = cluster.cell_list.iterator(); cell_iter.hasNext();) {
					Cell cell = cell_iter.next();
					cluster.update_post(cell);
					if (cluster.stream > 0)
						oscStreamSender.send(cell);
				}
				for (Iterator<Cell> cell_iter = cluster.cell_list.iterator(); cell_iter.hasNext();) {
					Cell cell = cell_iter.next();
					cell.decay -= cluster.mng_Decay.eval(cell, cluster);
					++cell.frame;
				}
			}
		} finally {
			Jean.habiat.culster_list_lock.unlock();
		}
	}

}
