package jean.model;

import jean.render.IStamp;

public class LorenzCluster extends AttractorCluster {
	public LorenzCluster(int cluster_index, Cluster up, int characteristic, String name, IStamp stamp) {
		super(cluster_index, up, characteristic, name, stamp);
	}
	
	public LorenzCluster(int characteristic, String name, IStamp stamp) {
		super(characteristic, name, stamp);
	}
	
	protected void next_point(AttractorCell next, AttractorCell acell) {
		double a =  mng_LorenzA.eval(acell, this);
		double b =  mng_LorenzB.eval(acell, this);
		double c =  mng_LorenzC.eval(acell, this);
		double dx = a * (acell.ly - acell.lx);
		double dy = acell.lx * (b - acell.lz) - acell.ly;
		double dz = acell.lx * acell.ly - c * acell.lz;
		next.lx += DELTA * dx;
		next.ly += DELTA * dy;
		next.lz += DELTA * dz;
	}
}
