package jean.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import jean.render.Oval;
import jean.render.Rect;

public class Habiat {
	public static final int SCREEN_HEIGHT = 1024;
	public static final int SCREEN_WIDTH = 1024;
	
	public List<Cluster> culster_pipeline = new ArrayList<Cluster>();
	public Decay decay_factory = new Decay();
	public Grow grow_factory = new Grow();

	public ReentrantLock culster_list_lock = new ReentrantLock();
	private int iter;

	public Habiat() {
	}

	public int getNumClusters(int characteristic) throws Exception {
		if(!culster_list_lock.isHeldByCurrentThread())
			throw new Exception("using culster list without locking");
		int count =0;
		for (Iterator<Cluster> iterator = culster_pipeline.iterator(); iterator.hasNext();) {
			Cluster cluster = iterator.next();
			if(cluster.characteristic == characteristic)
				++count;
		}
		return count;
	}

	public Cluster getCluster(int characteristic, int cluster_num) throws Exception {
		if(!culster_list_lock.isHeldByCurrentThread())
			throw new Exception("using culster list without locking");
		int count = 0;
		for (Iterator<Cluster> iterator = culster_pipeline.iterator(); iterator.hasNext();) {
			Cluster cluster = iterator.next();
			if(cluster.characteristic == characteristic) {
				if(cluster_num == count)
					return cluster;
				++count;
			}
		}
		return null;
	}

	private boolean matchType(final String full, final String pattern) {
//		System.out.println(full + " "  + pattern + " " + full.startsWith(pattern));
		if(full.length() < pattern.length())
			return false;
		for(int px=0; px<pattern.length(); ++px) {
			if(full.charAt(px) != pattern.charAt(px))
				return false;
		}
		return true;
	}
	
	public void build_pipeline(final List<Object> args) throws Exception {
		if(!culster_list_lock.isHeldByCurrentThread())
			throw new Exception("using culster list without locking");

		preserve_or_forget(args);
		List<Cluster> new_pipeline = new ArrayList<Cluster>();
		rebuild_pipeline(args, new_pipeline);
		reconnect_pipeline(new_pipeline);
//		culster_pipeline.clear();
		culster_pipeline = new_pipeline;
	}

	private void rebuild_pipeline(final List<Object> args, List<Cluster> new_pipeline) throws Exception {
		final int len = args.size();
		for(int ax=2; ax<len; ++ax) {
			Object arg = args.get(ax);
			if(arg instanceof String) {
				final String name = (String)arg;
				Cluster reuse = find_in_pipeline(name);
				if(reuse != null) {
					new_pipeline.add(reuse);
				}
				else {
					add_new_cluster(new_pipeline, arg, name);
				}
			}
			else {
				System.err.println("did not understand '" + arg + "' in (pipe expression");
			}
		}
	}

	private void reconnect_pipeline(List<Cluster> new_pipeline) {
		int depth = 0;
		Cluster upstream = null;
		for (Iterator<Cluster> cluster_iter = new_pipeline.iterator(); cluster_iter.hasNext();) {
			Cluster c = cluster_iter.next();
			c.cluster_index = depth;
			c.up = upstream;
			upstream = c;
			depth++;
		}
	}

	private void add_new_cluster(List<Cluster> new_pipeline, Object arg, final String name) {
		if(matchType(name, ":silent")) {
			new_pipeline.add(new Cluster(Cluster.C_SILENT, name, null));
		}
		else if(matchType((String)arg, ":cursor")) {
			new_pipeline.add(new Cluster(Cluster.C_CURSOR, name, new Oval()));
		}
		else if(matchType((String)arg, ":calve")) {
			new_pipeline.add(new Cluster(Cluster.C_CALVE, name, new Rect()));
		}
		else if(matchType((String)arg, ":lorenz")) {
			new_pipeline.add(new LorenzCluster(Cluster.C_CURSOR, name, new Oval()));
		}
		else if(matchType((String)arg, ":roessler")) {
			new_pipeline.add(new RoesslerCluster(Cluster.C_CURSOR, name, new Oval()));
		}
		else if(matchType((String)arg, ":synchro")) {
			new_pipeline.add(new SynchroCluster(Cluster.C_CURSOR, name, new Oval()));
		}
		else {
			System.err.println("did not understand '" + arg + "' in (pipe expression");
		}
	}

	private void preserve_or_forget(final List<Object> args) throws Exception {
		for (Iterator<Cluster> cluster_iter = culster_pipeline.iterator(); cluster_iter.hasNext();) {
			cluster_iter.next().preserved = false;
		}
		
		final int len = args.size();
		for(int ax=2; ax<len; ++ax) {
			Object arg = args.get(ax);
			if(arg instanceof String) {
				final String name = (String)arg;
				Cluster c = find_in_pipeline(name);
				if(c != null) {
					c.preserved = true;
//					System.out.println("preserve_or_forget 1 preserved " + c.getName() + " " + c.preserved);
				}
			}
		}

		for (Iterator<Cluster> cluster_iter = culster_pipeline.iterator(); cluster_iter.hasNext();) {
			Cluster c = cluster_iter.next();
//			System.out.println("preserve_or_forget 2 preserved " + c.getName() + " " + c.preserved);
			if(!c.preserved) {
				for (Iterator<Cell> cell_iter = c.cell_list.iterator(); cell_iter.hasNext();) {
					Cell cell = cell_iter.next();
					forget(cell);
//					System.out.println("forget  " + forget_cell);
				}
				c.clear();
			}
		}
	}

	private Cluster find_in_pipeline(String name) throws Exception {
		for (Iterator<Cluster> cluster_iter = culster_pipeline.iterator(); cluster_iter.hasNext();) {
			Cluster c = cluster_iter.next();
			if(c.getName().equals(name))
				return c;
		}
		return null;
	}
	
	public void draw() {
		culster_list_lock.lock();
		try {
			for (Iterator<Cluster> cluster_iter = culster_pipeline.iterator(); cluster_iter.hasNext();) {
				Cluster cluster = cluster_iter.next();
				for (Iterator<Cell> cell_iter = cluster.cell_list.iterator(); cell_iter.hasNext();) {
					Cell cell = cell_iter.next();
					cluster.draw(cell);
				}
			}
		} finally {
			culster_list_lock.unlock();
		}
	}

	public void forget(Cell forget_cell) throws Exception {
		if (!culster_list_lock.isHeldByCurrentThread())
			throw new Exception("using culster list without locking");
		for (Iterator<Cluster> cluster_iter = culster_pipeline.iterator(); cluster_iter.hasNext();) {
			Cluster cluster = cluster_iter.next();
			for (Iterator<Cell> cell_iter = cluster.cell_list.iterator(); cell_iter.hasNext();) {
				Cell cell = cell_iter.next();
				cell.forget_watch(forget_cell);
//				System.out.println("forget  " + forget_cell);
			}
		}
	}

	public void rewindCellList() {
		iter = 0;
	}

	public boolean hasNextCell(int cluster_index) {
		return cluster_index >= 0
				&& cluster_index < culster_pipeline.size()
				&& culster_pipeline.get(cluster_index).cell_list.size() > 0
				&& iter < culster_pipeline.get(cluster_index).cell_list.size();
	}
	
	public Cell nextCell(int cluster_index) {
		return culster_pipeline.get(cluster_index).cell_list.get(iter++);
	}

	public boolean hasCluster(int cluster_index) {
		return cluster_index >= 0
				&& cluster_index < culster_pipeline.size();
	}

	public Cluster getClusterAt(int cluster_index) {
		return culster_pipeline.get(cluster_index);
	}

	public void reset() throws Exception {
		if (!culster_list_lock.isHeldByCurrentThread())
			throw new Exception("using culster list without locking");
		for (Iterator<Cluster> cluster_iter = culster_pipeline.iterator(); cluster_iter.hasNext();) {
			Cluster cluster = cluster_iter.next();
			cluster.reset();
		}
	}

}
