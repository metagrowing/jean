package jean.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import jean.noise.JMath;

class ColorTest {

	private static final float EPSILON = 0.0001f;

	private static void assertRGBA(float r, float g, float b, float a, float h, float s, float v, float a0) {
		Color c = new Color(h, s, v, a0);
		assertEquals(r, c.r, EPSILON);
		assertEquals(g, c.g, EPSILON);
		assertEquals(b, c.b, EPSILON);
		assertEquals(a, c.a, EPSILON);		
	}
	
	@Test
	void testColor() {
		assertRGBA(0, 0, 0, 1, 0, 0, 0, 1);
		assertRGBA(0, 0, 0, 0, 0, 0, 0, 0);
		assertRGBA(1, 1, 1, 1, 0, 0, 1, 1);
		assertRGBA(0, 0, 0, 1, 1, 1, 0, 1);
		
//	  	Red 	(0°,100%,100%) 	#FF0000 	(255,0,0)
		assertRGBA(1, 0, 0, 1, 0, 1, 1, 1);
//	  	Lime 	(120°,100%,100%) 	#00FF00 	(0,255,0)
		assertRGBA(0, 1, 0, 1, 120.0f/360.0f, 1, 1, 1);
//	  	Blue 	(240°,100%,100%) 	#0000FF 	(0,0,255)
//	  	Yellow 	(60°,100%,100%) 	#FFFF00 	(255,255,0)
//	  	Cyan 	(180°,100%,100%) 	#00FFFF 	(0,255,255)
		assertRGBA(0, 1, 1, 1, 180.0f/360.0f, 1, 1, 1);
//	  	Magenta 	(300°,100%,100%) 	#FF00FF 	(255,0,255)
//	  	Silver 	(0°,0%,75%) 	#BFBFBF 	(191,191,191)
		assertRGBA(192f/256f, 192f/256f, 192f/256f, 1, 0, 0, 0.75f, 1);
//	  	Gray 	(0°,0%,50%) 	#808080 	(128,128,128)
//	  	Maroon 	(0°,100%,50%) 	#800000 	(128,0,0)
		assertRGBA(0.5f, 0, 0, 1, 0, 1, 0.5f, 1);
//	  	Olive 	(60°,100%,50%) 	#808000 	(128,128,0)
//	  	Green 	(120°,100%,50%) 	#008000 	(0,128,0)
//	  	Purple 	(300°,100%,50%) 	#800080 	(128,0,128)
//	  	Teal 	(180°,100%,50%) 	#008080 	(0,128,128)
//	  	Navy 	(240°,100%,50%) 	#000080 	(0,0,128)
	}

	@Test
	void testFractional_0_1() {
		assertEquals(0.0f, JMath.fractional_0_1(0.0f), EPSILON);
		assertEquals(0.0f, JMath.fractional_0_1(1.0f), EPSILON);

		assertEquals(0.1f, JMath.fractional_0_1(0.1f), EPSILON);
		assertEquals(0.9f, JMath.fractional_0_1(0.9f), EPSILON);
		assertEquals(0.01f, JMath.fractional_0_1(1.01f), EPSILON);
		
		assertEquals(0.9f, JMath.fractional_0_1(-0.1f), EPSILON);
		assertEquals(0.1f, JMath.fractional_0_1(-0.9f), EPSILON);
		assertEquals(0.99f, JMath.fractional_0_1(-1.01f), EPSILON);
	}

	@Test
	void testClamp_0_1() {
		assertEquals(0.0f, JMath.clamp_0_1(0.0f), EPSILON);
		assertEquals(0.5f, JMath.clamp_0_1(0.5f), EPSILON);
		assertEquals(1.0f, JMath.clamp_0_1(1.0f), EPSILON);
		assertEquals(1.0f, JMath.clamp_0_1(1.0001f), EPSILON);
		assertEquals(0.0f, JMath.clamp_0_1(-0.001f), EPSILON);
		assertEquals(0.0f, JMath.clamp_0_1(-1.001f), EPSILON);
	}

}
