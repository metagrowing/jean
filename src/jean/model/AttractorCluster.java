package jean.model;

import java.util.List;

import jean.l2asm.FnManagement;
import jean.render.IStamp;

public abstract class AttractorCluster extends Cluster {
	public FnManagement mng_LorenzA = new FnManagement(10);
	public FnManagement mng_LorenzB = new FnManagement(28);
	public FnManagement mng_LorenzC = new FnManagement(8.0 / 3.0);
	public FnManagement mng_ScaleX = new FnManagement(0.05);
	public FnManagement mng_ScaleY = new FnManagement(0.03);
	
	protected static final double DELTA = 0.004;

	public AttractorCluster(int cluster_index, Cluster up, int characteristic, String name, IStamp stamp) {
		super(cluster_index, up, characteristic, name, stamp);
	}

	public AttractorCluster(int characteristic, String name, IStamp stamp) {
		super(characteristic, name, stamp);
	}
	
	@Override
	protected Cell createCell(int index) {
		AttractorCell cell = new AttractorCell(index, id_counter++, this);
		cell.reset();
		return cell;
	}

	@Override
	public void reload(List<Object> args) {
		if (!validate_msg(args))
			return;

		super.reload(args);
		
		final int stop = args.size() - 1;
		for (int ax = 2; ax < stop; ax += 2) {
			Object name = args.get(ax);
			Object arg = args.get(ax + 1);
			FnManagement.parsePair(arg, name, "pa", mng_LorenzA);
			FnManagement.parsePair(arg, name, "pb", mng_LorenzB);
			FnManagement.parsePair(arg, name, "pc", mng_LorenzC);
			FnManagement.parsePair(arg, name, "sx", mng_ScaleX);
			FnManagement.parsePair(arg, name, "sy", mng_ScaleY);
		}
	}

	@Override
	public void update(Cell cell) {
		AttractorCell acell = (AttractorCell)cell;

		AttractorCell next;
		try {
			double old_x = cell.pos_x;
			double old_y = cell.pos_y;
			next = (AttractorCell) acell.clone();
			eval_wh(next, acell);
			eval_hsva(next, acell);

			next_point(next, acell);

			double sx =  mng_ScaleX.eval(acell, this);
			double sy =  mng_ScaleY.eval(acell, this);
			next.pos_x = sx * acell.lx;
			next.pos_y = sy * acell.ly;
			next.delta_x = next.pos_x - old_x;
			next.delta_y = next.pos_y - old_y;
			acell.patchState(next);
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
	}

	abstract protected void next_point(AttractorCell next, AttractorCell acell);
}
