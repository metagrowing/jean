package jean.model;

import java.util.Iterator;
import java.util.List;

import jean.l2asm.FnManagement;
import jean.render.IStamp;

public class SynchroCluster extends Cluster {
	public FnManagement mng_Influx = new FnManagement(0.0064);
	public FnManagement mng_Drain  = new FnManagement(0.0033);
	public FnManagement mng_Pulse  = new FnManagement(6 * 0.0025);
	private double drain;
	private double influx;
	private double pulse;
	
	public SynchroCluster(int cluster_index, Cluster up, int characteristic, String name, IStamp stamp) {
		super(cluster_index, up, characteristic, name, stamp);
	}
	
	public SynchroCluster(int characteristic, String name, IStamp stamp) {
		super(characteristic, name, stamp);
	}
	
	@Override
	protected Cell createCell(int index) {
		SynchroCell cell = new SynchroCell(index, id_counter++, this);
		cell.reset();
		return cell;
	}

	@Override
	public void reload(List<Object> args) {
		if (!validate_msg(args))
			return;

		super.reload(args);
		
		final int stop = args.size() - 1;
		for (int ax = 2; ax < stop; ax += 2) {
			Object name = args.get(ax);
			Object arg = args.get(ax + 1);
			FnManagement.parsePair(arg, name, "influx", mng_Influx);
			FnManagement.parsePair(arg, name, "drain", mng_Drain);
			FnManagement.parsePair(arg, name, "pulse", mng_Pulse);
		}
	}

	@Override
	public void update_pre(Cell cell) {
		drain  = mng_Drain.eval(cell, this);
		influx = mng_Influx.eval(cell, this);
		pulse  = mng_Pulse.eval(cell, this);
		if (cell instanceof SynchroCell) {
			SynchroCell scell = (SynchroCell) cell;
			scell.level1 = 0;
			scell.pulse_step = 0;
		}
	}

	@Override
	public void update(Cell cell) {
		super.update(cell);
		if (cell instanceof SynchroCell) {
			SynchroCell scell = (SynchroCell) cell;

			try {
				SynchroCell next = (SynchroCell) scell.clone();

				double drain_step = next.level0 * drain;
				next.level1 = next.level0 + influx - drain_step;
				if (next.level1 > 1) {
					next.level1 = 0;
					next.pulse_step = 0;
					for (Iterator<Cell> cell_iter = cell_list.iterator(); cell_iter.hasNext();) {
						SynchroCell other_cell = (SynchroCell) cell_iter.next();
						other_cell.pulse_step += pulse;
					}
					if(cell.cluster.hit_synth != null)
						cell.cluster.hit_synth.send_synth_new(this, this, next, next);
				}
				scell.patchState(next);
				scell.level0 = next.level0;
				scell.level1 = next.level1;
				scell.pulse_step = next.pulse_step;
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update_post(Cell cell) {
		if (cell instanceof SynchroCell) {
			SynchroCell scell = (SynchroCell) cell;
			scell.level0 = scell.level1 + scell.pulse_step;
			scell.dim_w *= scell.level0;
			scell.dim_h *= scell.level0;
		}
		super.update_post(cell);
	}

	@Override
	public String toString() {
		return "SynchroCluster [drain=" + drain + ", influx=" + influx + ", pulse=" + pulse + "]";
	}

}
