package jean.model;

public class SynchroCell extends Cell {
	protected double level0;
	protected double level1;
	protected double pulse_step;

	public SynchroCell(int index, int id, Cluster cluster) {
		super(index, id, cluster);
	}

	@Override
	public void reset() {
		level0 = Math.random();
		super.reset();
	}

	@Override
	public String toString() {
		return "SynchroCell [level0=" + level0 + ", level1=" + level1 + ", pulse_step=" + pulse_step + "]";
	}

}
