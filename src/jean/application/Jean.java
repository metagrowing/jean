package jean.application;

import jean.model.CurrentState;
import jean.model.Habiat;
import jean.render.lwjgl.Binding;

public class Jean implements IApplication {

    public static Habiat habiat;
    public static Binding binding;
	public static boolean send_osc = false;
	private CurrentState currentState;

	public static void main(String[] args) {
		try {
			System.out.println("Starting Jean 0.0");
			System.out.println("java.version " + System.getProperty("java.version"));
			
			// TODO enable NanoKontrol2Listener optional
//			try (jean.MIDIin.NanoKontrol2Listener midiListener = new jean.MIDIin.NanoKontrol2Listener()) {
//				midiListener.setup();
//			}

			jean.OSCin.OscListener oscListener = new jean.OSCin.OscListener();
			oscListener.setup();

			new Jean().run();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	private void reset() {
		try {
			Jean.habiat.culster_list_lock.lock();
		    try {
				CurrentState.need_full_clear = true;
				CurrentState.frame = 0;
				habiat.reset();
		    } finally {
		    	Jean.habiat.culster_list_lock.unlock();
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void run() {
		habiat = new Habiat();
		currentState = new CurrentState();
		binding = new Binding(this);
		send_osc = true;
		binding.run();
	}

	@Override
	public void onUpdate() {
		currentState.update();
		currentState.collide();
		habiat.draw();
		CurrentState.frame += 1;
	}

	@Override
	public void onKey(int key, int mods) {
		System.out.println("key " + key + " mods " + mods);
		switch ((int) key) {
		case 73: // i key
			binding.save_framebuffer();
			break;
		case 82: // r key
			if ((mods & 1) == 1) {
				reset();
			}
			binding.setRecording(true);
			break;
		case 83: // s key
			binding.setRecording(false);
			break;
		case 89: // z key
			reset();
			break;
		}
	}

	@Override
	public void onExit() {
		send_osc = false;
		System.out.println("Jean is closing");
	}

}
