package jean.application;

public interface IApplication {

	void onUpdate();

	void onKey(int key, int mods);

	void onExit();

}