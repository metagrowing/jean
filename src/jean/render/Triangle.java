package jean.render;


import jean.model.Cell;
import jean.render.lwjgl.Shape;

public class Triangle extends Shape implements IStamp {

	public Triangle() {
		super();
		init();
	}

	@Override
	public void init() {
		final float a = 1.0f;
        positions = new float[] {
                 0.0f, a, 0.0f,
                -a,   -a, 0.0f,
                 a,   -a, 0.0f
        };
        super.is_out_of_sync = true;
	}

	@Override
	public void update(Cell cell) {
	}

	@Override
	public String toString() {
		return "Oval []";
	}

}
