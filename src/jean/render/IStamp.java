package jean.render;

import jean.model.Cell;

public interface IStamp {
	
//	void draw(final Cell cell);
	void init();
	void update(Cell cell);
//	boolean isVisible(Cell cell);
	int numTriangls();
	float[] collisionShape();
	void clear();
}
