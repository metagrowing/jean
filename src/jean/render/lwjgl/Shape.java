package jean.render.lwjgl;

import java.nio.FloatBuffer;

import org.lwjgl.system.MemoryStack;

import static org.lwjgl.opengl.GL30.*;


public abstract class Shape {
	protected float[] positions;
    public int vaoId;
    int vboId;
    protected boolean is_out_of_sync = false;
    boolean is_allocated = false;
	public boolean triangle_fan_draw_mode = false;

//	public float[] getPositions() {
//		return positions;
//	}

	public int numTriangls() {
		return positions.length / 3;
	}

	public float[] collisionShape() {
		return positions;
	}

	public void init_peer() {
		if(is_out_of_sync) {
			clear();

			vaoId = glGenVertexArrays();
			glBindVertexArray(vaoId);

			try (MemoryStack stack = MemoryStack.stackPush()) {
				// Positions VBO
				vboId = glGenBuffers();
				FloatBuffer positionsBuffer = stack.callocFloat(positions.length);
				positionsBuffer.put(0, positions);
				glBindBuffer(GL_ARRAY_BUFFER, vboId);
				glBufferData(GL_ARRAY_BUFFER, positionsBuffer, GL_STATIC_DRAW);
				glEnableVertexAttribArray(0);
				glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

				glBindBuffer(GL_ARRAY_BUFFER, 0);
				glBindVertexArray(0);
				is_allocated = true;
				is_out_of_sync = false;
			}
		}
	}

	public void clear() {
		if(is_allocated) {
			glDeleteBuffers(vboId);
		    glDeleteVertexArrays(vaoId);
		    is_allocated = false;
		}
	}
}