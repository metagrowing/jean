package jean.render.lwjgl;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_REPEAT;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNPACK_ALIGNMENT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glPixelStorei;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.stb.STBImage.stbi_failure_reason;
import static org.lwjgl.stb.STBImage.stbi_image_free;
import static org.lwjgl.stb.STBImage.stbi_load;
import static org.lwjgl.stb.STBImage.stbi_set_flip_vertically_on_load_thread;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.system.MemoryStack;

import jean.model.Cell;
import jean.render.IStamp;

public class ImageUnique  extends Shape  implements IStamp {
	private static final String MEDIA = "media";
	String image_path = "";
    ByteBuffer image_buffer;
    private int textureId;
	private int width;
	private int height;
	private ShaderProgram imageShader;
    
	public ImageUnique() {
		super();
		init();
	}

	public void init() {
		final float a = 1.0f;
		positions = new float[] {
                 a,  a, 0.0f,
                -a,  a, 0.0f,
                -a, -a, 0.0f,
                
                -a, -a, 0.0f,
                 a, -a, 0.0f,
                 a,  a, 0.0f,
        };
        super.is_out_of_sync = true;
	}
	
	public void load(String fn) {
		clear();
		image_path = MEDIA + "/" + fn;

		File f = new File(image_path);
		if (f.isFile() && f.canRead()) {
			try (MemoryStack stack = MemoryStack.stackPush()) {
				IntBuffer w = stack.mallocInt(1);
				IntBuffer h = stack.mallocInt(1);
				IntBuffer channels = stack.mallocInt(1);
				stbi_set_flip_vertically_on_load_thread(1);
				image_buffer = stbi_load(image_path, w, h, channels, 4);
				width = w.get();
				height = h.get();
				is_out_of_sync = image_buffer != null;
				if(image_buffer == null) {
					System.err.println("Can't understand this file as an image  '" + image_path + "'");
					System.err.println(stbi_failure_reason());
				}
				else {
					System.out.println("Reading image file  '" + image_path + "'");
				}
			}
		}
		else {
			System.err.println("Can't read file '" + image_path + "'");
		}
	}

    private void image2texture() {
        textureId = glGenTextures();

        glBindTexture(GL_TEXTURE_2D, textureId);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image_buffer);

		if(image_buffer != null) {
			stbi_image_free(image_buffer);
			image_buffer = null;
		}
    }
    
	public void init_peer() {
		if(is_out_of_sync) {
			image2texture();
			if(imageShader == null)
				imageShader = new ShaderProgram("copy", "image");
			super.init_peer();
	    }
    }

	//TODO image transform should be independent from the layer transformation
	public void draw_background(float alpha, float ltx, float lty) {
		if(width > 0 && height > 0 && imageShader != null) {
		    // NICE use texture coordinates s t
			// NICE support rotation, scaling, translation
	        glUseProgram(imageShader.programId);
			imageShader.setUniform1i("texture0", 0);
			imageShader.setUniform1f("alpha", alpha);
			imageShader.setUniform1f("ltx", ltx);
			imageShader.setUniform1f("lty", lty);
			glBindVertexArray(vaoId);
	        glActiveTexture(GL_TEXTURE0);
	        glBindTexture(GL_TEXTURE_2D, textureId);
			glDrawArrays(GL_TRIANGLES, 0, positions.length / 3);
			glBindVertexArray(0);
			imageShader.unbind();
		}
	}

	public void clear() {
		if(image_buffer != null) {
//			stbi_image_free(image_buffer);
			image_buffer = null;
			image_path = "";
		}
		super.clear();
	}

	@Override
	public void update(Cell cell) {
	}

	@Override
	public String toString() {
		return "ImageUnique [image=" + image_path + "]";
	}

}
