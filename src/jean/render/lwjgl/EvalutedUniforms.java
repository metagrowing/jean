package jean.render.lwjgl;

import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUniform1f;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jean.l2asm.FnManagement;
import jean.model.Cell;
import jean.model.Cluster;

public class EvalutedUniforms {

    private Map<String, FnManagement> uniforms = new HashMap<String, FnManagement>();

    public void addUniform(String uniform_name, FnManagement mng_Fn) {
    	uniforms.put(uniform_name, mng_Fn);
    }
    
    public void bind(int programId, Cell cell, Cluster cluster) {
        uniforms.forEach((uniform_name, mng_Fn) -> {
        	double value = mng_Fn.eval(cell, cluster);
        	if(Binding.DEBUG_BINDING) System.out.println("uniform_name=" + uniform_name + ", value=" + value);
    		int location = glGetUniformLocation(programId, uniform_name);
    		glUniform1f(location, (float)value);
        });
    }

	private void parsePair(Object arg, Object name, String key, FnManagement m) {
		if (name.equals(key)) {
			if (arg instanceof Float) {
				System.out.println("f " + name + " " + arg);
				m.setValue((Float)arg);
			} else if (arg instanceof String) {
				System.out.println("s " + name + " " + arg);
				m.reload_code(arg, key);
			}
		}
	}

	public void reload(List<Object> args) {
        uniforms.forEach((uniform_name, mng_Fn) -> {
        	mng_Fn.remove_code();
        });
        
		final int stop = args.size() - 1;
		for (int ax = 2; ax < stop; ax += 2) {
			Object name = args.get(ax);
			Object arg = args.get(ax + 1);
	        uniforms.forEach((uniform_name, mng_Fn) -> {
				parsePair(arg, name, uniform_name, mng_Fn);
	        });
		}
    }

	public void clear() {
        uniforms.forEach((uniform_name, mng_Fn) -> {
        	mng_Fn.remove_code();
        });
        uniforms.clear();
	}

}
