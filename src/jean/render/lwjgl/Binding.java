package jean.render.lwjgl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.imageio.ImageIO;

import org.lwjgl.Version;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;
import org.lwjgl.system.MemoryStack;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;
import static org.lwjgl.opengl.GL20.glGetUniformLocation;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL30.*;

import jean.application.IApplication;
import jean.application.Jean;
import jean.model.Cell;
import jean.model.Cluster;
import jean.model.CurrentState;
import jean.model.Habiat;

public class Binding {
	IApplication app;
	
    public Binding(IApplication app) {
		super();
		this.app = app;
	}

    private long glfw_window;
	private int[] rgba32f_bufferId = new int[3];
	private int[] rgba32f_textureId = new int[3];
	private float CLEAR_ALPHA = 0.0f; // TODO alpha 0 or 1?
	private static final int A_BUFFER= 0;
	private static final int B_BUFFER= 1;
	private static final int L_BUFFER= 2;
	public final static boolean DEBUG_BINDING = false;
	private char toName(int bi) {
		switch(bi) {
		case 0: return 'A';
		case 1: return 'B';
		case 2: return 'L';
		}
		return '?';
	}
	
	private float[] positions;
	private int vaoId;
	private int vboId;

	private ShaderProgram layerShader;
	private ShaderProgram blendShaders[];
	private ShaderProgram copyShader;
	
	private boolean recording;
	
	private void init() {
		setRecording(false);
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		if (!glfwInit())
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		// Create the window
		glfw_window = glfwCreateWindow(Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT, "Jean", NULL, NULL);
		if (glfw_window == NULL)
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated
		// or released.
		glfwSetKeyCallback(glfw_window, (window, key, scancode, action, mods) -> {
			switch (key) {
			case GLFW_KEY_ESCAPE:
				if (action == GLFW_RELEASE)
					glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
				break;
			default:
				if (action == GLFW_PRESS)
					app.onKey(key, mods);
			}
		});

		glfwSetWindowIconifyCallback(glfw_window, (window, iconified) -> {
			Jean.send_osc = !iconified;
		});

		glfwSetWindowPos(glfw_window, 0, 0);
		glfwMakeContextCurrent(glfw_window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(glfw_window);

		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();
		create_quad();
		create_RGBA32F_buffer(A_BUFFER);
		create_RGBA32F_buffer(B_BUFFER);
		create_RGBA32F_buffer(L_BUFFER);
		
		layerShader = new ShaderProgram("transform", "direct_rgba");
		
		blendShaders = new ShaderProgram [] {
		    new ShaderProgram("transform", "blend_src"),     // 0
		    new ShaderProgram("transform", "blend_mix"),     // 1
		    new ShaderProgram("transform", "blend_max"),     // 2
		    new ShaderProgram("transform", "blend_min"),     // 3
		    new ShaderProgram("transform", "blend_mul"),     // 4
		    new ShaderProgram("transform", "blend_add"),     // 5
		    new ShaderProgram("transform", "blend_diff"),    // 6
		    new ShaderProgram("transform", "blend_screen"),  // 7
		    new ShaderProgram("transform", "blend_overlay"), // 8
		    new ShaderProgram("transform", "blend_inv"),     // 9
		    new ShaderProgram("transform", "blend_red"),     // 10
		    new ShaderProgram("transform", "blend_gray"),    // 11
		    new ShaderProgram("transform", "blend_inv_dest"),// 12
		    new ShaderProgram("transform", "geom_rotate"),   // 13
		    new ShaderProgram("transform", "geom_pixelate"), // 14
		    new ShaderProgram("transform", "geom_repeat"),   // 15
		    new ShaderProgram("transform", "geom_noise"),    // 16
		    new ShaderProgram("transform", "geom_blur"),     // 17
		};
//		ShaderProgram geom_noise = find("geom_noise");
//		geom_noise.addUniform("xamount", new FnManagement(0.1));
//		geom_noise.addUniform("yamount", new FnManagement(0.1));
//		geom_noise.addUniform("xscale", new FnManagement(1));
//		geom_noise.addUniform("yscale", new FnManagement(1));

		copyShader = new ShaderProgram("copy", "copy");
	}

//	private ShaderProgram find(String frag_name) {
//		for(int i=0; i < blendShaders.length; ++i) {
//			if(blendShaders[i].frag_name.equals(frag_name))
//				return blendShaders[i]; 
//		}
//        throw new RuntimeException("Undefined fragment shader [" + frag_name + "]");
//	}

	public void save_framebuffer() {
        final int size = 3 * Habiat.SCREEN_WIDTH * Habiat.SCREEN_HEIGHT;
        glReadBuffer(GL_FRONT);
        ByteBuffer pixels = org.lwjgl.BufferUtils.createByteBuffer(size);
        glReadPixels(0, 0, Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT, GL_BGR, GL_UNSIGNED_BYTE, pixels);
        byte [] data_copy = new byte[size];
        pixels.get(data_copy);
        BufferedImage bi = new BufferedImage(Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT, BufferedImage.TYPE_4BYTE_ABGR);
        int j = 0;
        for(int y=0; y<Habiat.SCREEN_HEIGHT; ++y) {
            for(int x=0; x<Habiat.SCREEN_WIDTH; ++x) {
            	int abgr = 0xff000000
            			   | ((data_copy[j+2] << 16) & 0x00ff0000)
            			   | ((data_copy[j+1] << 8)  & 0x0000ff00)
            			   | (data_copy[j+0] & 0x000000ff);
                bi.setRGB(x, Habiat.SCREEN_HEIGHT-y-1, abgr);
                j += 3;
            }
        }
	    try {
			ImageIO.write(bi, "png", new File("/dev/shm/" + "jean_" + String.format("%06d", CurrentState.frame) + ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	private void save_int_framebuffer() {
//        final int size = Habiat.SCREEN_WIDTH * Habiat.SCREEN_HEIGHT;
//        glReadBuffer(GL_FRONT);
//        IntBuffer pixels = org.lwjgl.BufferUtils.createIntBuffer(size);
//        glReadPixels(0, 0, Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT, GL_RGBA, GL_UNSIGNED_INT, pixels);
//        System.out.println(pixels);
//        int [] a = new int[size];
//        pixels.get(a, 0, size);
//        BufferedImage i = new BufferedImage(Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT, BufferedImage.TYPE_INT_ARGB);
//        i.setRGB(0, 0, Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT, a, 0, Habiat.SCREEN_WIDTH);
//        System.out.println(i);
//	    try {
//			ImageIO.write(i, "png", new File("jean_" + String.format("%06d", CurrentState.frame) + ".png"));
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}

	private void create_RGBA32F_buffer(int bi) {
		rgba32f_bufferId[bi] = glGenFramebuffers();
		rgba32f_textureId[bi] = glGenTextures();

		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, rgba32f_bufferId[bi]);

		glBindTexture(GL_TEXTURE_2D, rgba32f_textureId[bi]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT, 0, GL_RGBA, GL_FLOAT, (ByteBuffer) null);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, rgba32f_textureId[bi], 0);

		try (MemoryStack stack = MemoryStack.stackPush()) {
			IntBuffer intBuff = stack.mallocInt(1);
			intBuff.put(0, GL_COLOR_ATTACHMENT0);
			glDrawBuffers(intBuff);
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
	private void create_quad() {
		final float a = 1.0f;
		positions = new float[] {
                 a,  a, 0.0f,
                -a,  a, 0.0f,
                -a, -a, 0.0f,
                
                -a, -a, 0.0f,
                 a, -a, 0.0f,
                 a,  a, 0.0f,
        };
		try (MemoryStack stack = MemoryStack.stackPush()) {
			vaoId = glGenVertexArrays();
			glBindVertexArray(vaoId);
			// Positions VBO
			vboId = glGenBuffers();
			FloatBuffer positionsBuffer = stack.callocFloat(positions.length);
			positionsBuffer.put(0, positions);
			glBindBuffer(GL_ARRAY_BUFFER, vboId);
			glBufferData(GL_ARRAY_BUFFER, positionsBuffer, GL_STATIC_DRAW);
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
		}
		
		
	}
	
	private void clear_buffer() {
		glDisable(GL_BLEND);
		glClearColor(0.0f, 0.0f, 0.0f, CLEAR_ALPHA);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	private void clear_ab_buffer_conditional(boolean force) {
		if (CurrentState.need_full_clear || force) {
			if(DEBUG_BINDING) System.out.println("clear A B");
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, rgba32f_bufferId[A_BUFFER]);
			clear_buffer();
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, rgba32f_bufferId[B_BUFFER]);
			clear_buffer();
			CurrentState.need_full_clear = false;
		}
	}

	private void loop() {
		clear_ab_buffer_conditional(true);
		glClear(GL_DEPTH_BUFFER_BIT);
		glDisable(GL_DEPTH_TEST);
		glViewport(0, 0, Habiat.SCREEN_WIDTH, Habiat.SCREEN_HEIGHT);
		
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		int buffer_index = 0;
		// Run the rendering loop until the user has attempted to close
		// the window or has pressed the ESCAPE key.
		while (!glfwWindowShouldClose(glfw_window)) {
			app.onUpdate();

			if(DEBUG_BINDING) System.out.println("===========");
			int cluster_index = 0;
			while (Jean.habiat.hasCluster(cluster_index)) {
				if(DEBUG_BINDING) System.out.println("-----------");
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, rgba32f_bufferId[L_BUFFER]);
				if(DEBUG_BINDING) System.out.println("clear L");
				clear_buffer();
				draw_layer(cluster_index);
				
				clear_ab_buffer_conditional(false);
				merge_layer_to_ab(buffer_index % 2, ((buffer_index + 1) % 2), cluster_index);
				++cluster_index;
				++buffer_index;
			}
			copy_to_screen(((buffer_index + 1) % 2));
			
			glfwSwapBuffers(glfw_window);
			if(recording)
				save_framebuffer();

			glfwPollEvents();
		}
	}

	private void draw_layer(int cluster_index) {
		if(Jean.habiat.hasCluster(cluster_index)) {
			Cluster cluster = Jean.habiat.getClusterAt(cluster_index);
            if(DEBUG_BINDING) System.out.println("cluster=" + cluster.cluster_index);
			
			cluster.prepare_layer();

			Shape shape = (Shape) cluster.stamp;
			shape.init_peer();
			Jean.habiat.rewindCellList();
	        glUseProgram(layerShader.programId);
			while (Jean.habiat.hasNextCell(cluster_index)) {
				Cell cell = Jean.habiat.nextCell(cluster_index);
				if (cell.isVisible()) {
					layerShader.setUniform1f("tx", (float)cell.pos_x);
					layerShader.setUniform1f("ty", (float)cell.pos_y);
					layerShader.setUniform1f("sx", (float)cell.dim_w);
					layerShader.setUniform1f("sy", (float)cell.dim_h);
					layerShader.setUniform1f("sin_rot", (float)cell.sin_rot);
					layerShader.setUniform1f("cos_rot", (float)cell.cos_rot);
					layerShader.setUniform1f("r", (float)cell.color.r);
					layerShader.setUniform1f("g", (float)cell.color.g);
					layerShader.setUniform1f("b", (float)cell.color.b);
					layerShader.setUniform1f("a", (float)cell.color.a);
					glDisable(GL_BLEND);
					glBindVertexArray(shape.vaoId);
					glDrawArrays(shape.triangle_fan_draw_mode ? GL_TRIANGLE_FAN : GL_TRIANGLES, 0, shape.numTriangls());
				}
			}


			glBindVertexArray(0);
			layerShader.unbind();
		}
	}

	private void merge_layer_to_ab(int dest, int previous, int cluster_index) {
		if(DEBUG_BINDING) System.out.println("merge " + toName(dest) + " <- L " + toName(previous));
		if(Jean.habiat.hasCluster(cluster_index)) {
			Cluster cluster = Jean.habiat.getClusterAt(cluster_index);
			
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, rgba32f_bufferId[dest]);
			glEnable(GL_BLEND);
			ShaderProgram blendShader = blendShaders[cluster.blend_mode % blendShaders.length];
			blendShader.bind(cluster.blend_uniforms, Cell.dummy, cluster);
			
			final int i = blendShader.programId;
			int location0 = glGetUniformLocation(i, "texture0");
			glUniform1i(location0, 0);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, rgba32f_textureId[L_BUFFER]);
			
			int location1 = glGetUniformLocation(i, "texture1");
			glUniform1i(location1, 1);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, rgba32f_textureId[previous]);
			
			int location2 = glGetUniformLocation(i, "tx");
			glUniform1f(location2, cluster.layer_tx);
			int location3 = glGetUniformLocation(i, "ty");
			glUniform1f(location3, cluster.layer_ty);
			int location4 = glGetUniformLocation(i, "sx");
			glUniform1f(location4, cluster.layer_sx);
			int location5 = glGetUniformLocation(i, "sy");
			glUniform1f(location5, cluster.layer_sy);
			int location6 = glGetUniformLocation(i, "sin_rot");
			glUniform1f(location6, (float)Math.sin(cluster.layer_rot));
			int location7 = glGetUniformLocation(i, "cos_rot");
			glUniform1f(location7, (float)Math.cos(cluster.layer_rot));
			
			glBindVertexArray(vaoId);
			glDrawArrays(GL_TRIANGLES, 0, positions.length / 3);
			glBindVertexArray(0);
			blendShader.unbind();
		}
	}

	private void copy_to_screen(int bi) {
		if(DEBUG_BINDING) System.out.println("copy  " + 'S' + " <- " + toName(bi));
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glDisable(GL_BLEND);
        glUseProgram(copyShader.programId);
		int location = glGetUniformLocation(copyShader.programId, "texture0");
		glUniform1i(location, 0);
		glBindVertexArray(vaoId);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, rgba32f_textureId[bi]);
		glDrawArrays(GL_TRIANGLES, 0, positions.length / 3);
		glBindVertexArray(0);
		copyShader.unbind();
	}

    public void run() {
        System.out.println("LWJGL " + Version.getVersion());
        
        init();
        loop();
        app.onExit();

        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(glfw_window);
        glfwDestroyWindow(glfw_window);

        // Terminate GLFW and free the error callback
        glfwTerminate();
        glfwSetErrorCallback(null).free();
    }

	public void setRecording(boolean recording) {
		this.recording = recording;
	}
}
