package jean.render.lwjgl;

import static org.lwjgl.opengl.GL11.GL_LINEAR;
import static org.lwjgl.opengl.GL11.GL_REPEAT;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.GL_UNPACK_ALIGNMENT;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glBindTexture;
import static org.lwjgl.opengl.GL11.glDrawArrays;
import static org.lwjgl.opengl.GL11.glGenTextures;
import static org.lwjgl.opengl.GL11.glDeleteTextures;
import static org.lwjgl.opengl.GL11.glPixelStorei;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL20.glUseProgram;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.stb.STBImage.stbi_failure_reason;
import static org.lwjgl.stb.STBImage.stbi_image_free;
import static org.lwjgl.stb.STBImage.stbi_load;
import static org.lwjgl.stb.STBImage.stbi_set_flip_vertically_on_load_thread;

import java.io.File;
//import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import org.lwjgl.system.MemoryStack;

import jean.model.Cell;
import jean.model.CurrentState;
import jean.render.IStamp;

public class ImageSequence extends Shape implements IStamp {
	private static final String MEDIA = "media";
	String image_path = "";
	private Vector<ByteBuffer> image_buffer = new Vector<ByteBuffer>();
	private int ilen = 0;
	private int iselected = -1;
	private int[] textureIds;
	private int[] width;
	private int[] height;
	private ShaderProgram imageShader;
	private boolean hasTexutreIds = false;

	public ImageSequence() {
		super();
		init();
	}

	public void init() {
		final float a = 1.0f;
		positions = new float[] {
                 a,  a, 0.0f,
                -a,  a, 0.0f,
                -a, -a, 0.0f,
                
                -a, -a, 0.0f,
                 a, -a, 0.0f,
                 a,  a, 0.0f,
        };
		super.is_out_of_sync = true;
	}

	public Set<Path> listFilesUsingDirectoryStream(String dir) throws IOException {
		Set<Path> fileList = new HashSet<>();
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(dir))) {
			for (Path path : stream) {
				if (Files.isRegularFile(path) && !Files.isDirectory(path) && Files.isReadable(path) && path.toString().endsWith(".png")) {
					System.out.println("add " + path.toString());
					fileList.add(path);
				}
			}
		}
		return fileList;
	}

	public void load(String path) {
		clear_img_sequ();
		image_path = path;

		Set<Path> fileList;
		try {
			fileList = listFilesUsingDirectoryStream(MEDIA + "/" + path);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}

		Vector<Integer> wv = new Vector<Integer>();
		Vector<Integer> hv = new Vector<Integer>();
		for (Path fn : fileList) {
			File f = fn.toFile();
			if (f.isFile() && f.canRead()) {
				try (MemoryStack stack = MemoryStack.stackPush()) {
					IntBuffer w = stack.mallocInt(1);
					IntBuffer h = stack.mallocInt(1);
					IntBuffer channels = stack.mallocInt(1);
					stbi_set_flip_vertically_on_load_thread(1);
					ByteBuffer b = stbi_load(f.toString(), w, h, channels, 4);
					if (b != null) {
						image_buffer.add(b);
						wv.add(w.get());
						hv.add(h.get());
						is_out_of_sync |= true;
					} else {
						System.err.println("Can't understand this file as an image  '" + f.toString() + "'");
						System.err.println(stbi_failure_reason());
					}
				}
			} else {
				System.err.println("Can't read file '" + f.toString() + "'");
			}
		}

		ilen = image_buffer.size();
		if(ilen > 0) {
			width = new int[ilen];
			int ix = 0;
			for (Iterator<Integer> iterator = wv.iterator(); iterator.hasNext();) {
				width[ix++] = iterator.next();
			}
			height = new int[ilen];
			ix = 0;
			for (Iterator<Integer> iterator = hv.iterator(); iterator.hasNext();) {
				height[ix++] = iterator.next();
			}
			iselected = ((int) (ilen * Math.random()) % ilen);
		}
	}

	private void image2texture(int id) {
		glBindTexture(GL_TEXTURE_2D, textureIds[id]);
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width[id], height[id], 0, GL_RGBA, GL_UNSIGNED_BYTE, image_buffer.get(id));

		stbi_image_free(image_buffer.get(id));
	}

	public void init_peer() {
		if (is_out_of_sync) {
			super.init_peer();
			System.out.println("init_peer " + hasTexutreIds);
			if (hasTexutreIds)
				for (int i = 0; i < ilen; ++i) {
					System.out.println("del " + Arrays.toString(textureIds));
					glDeleteTextures(textureIds);
					hasTexutreIds = false;
				}
			textureIds = new int[ilen];
			glGenTextures(textureIds);
			System.out.println("gen " + Arrays.toString(textureIds));
			hasTexutreIds = true;
			for (int i = 0; i < ilen; ++i) {
				image2texture(i);
			}
//			is_out_of_sync = false;
			if (imageShader == null)
				imageShader = new ShaderProgram("copy", "image");
		}
	}

	// TODO image transform should be independent from the layer transformation
	protected void draw_slected(float alpha, float ltx, float lty) {
		if (ilen > 0 && width[iselected] > 0 && height[iselected] > 0 && imageShader != null) {
			// NICE use texture coordinates s t
			// NICE support rotation, scaling, translation
			glUseProgram(imageShader.programId);
			imageShader.setUniform1i("texture0", 0);
			imageShader.setUniform1f("alpha", alpha);
			imageShader.setUniform1f("ltx", ltx);
			imageShader.setUniform1f("lty", lty);
			glBindVertexArray(vaoId);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureIds[iselected]);
			glDrawArrays(GL_TRIANGLES, 0, positions.length / 3);
			glBindVertexArray(0);
			imageShader.unbind();
		}
	}

	public void draw_background(float alpha, float ltx, float lty) {
		if (ilen > 0) {
			if ((CurrentState.frame % 180) == 0)
				iselected = ((int) (ilen * Math.random()) % ilen);
			if (iselected >= 0)
				draw_slected(alpha, ltx, lty);
		}
	}

	private void clear_img_sequ() {
		System.out.println("clear_img_sequ " + hasTexutreIds);
		if (hasTexutreIds)
			for (int i = 0; i < ilen; ++i) {
				glDeleteTextures(textureIds);
				hasTexutreIds = false;
			}
		width = null;
		height = null;
		image_buffer.clear();
		image_path = "";
		ilen = 0;
		iselected = -1;
		super.clear();
	}

	@Override
	public void update(Cell cell) {
	}

	@Override
	public String toString() {
		return "ImageSequence [image_path=" + image_path + ", ilen=" + ilen + ", iselected=" + iselected + "]";
	}
}
