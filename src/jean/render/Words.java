package jean.render;

import jean.model.Cell;
import jean.model.CurrentState;

public class Words implements IStamp {
	String full_text = "Modern telegraphy : some errors of dates of events and of statement in the history of telegraphy / exposed and rectified by Samuel F. B. Morse,...";
	private String[] parts;
	private int tselected = -1;

	public Words() {
		split_words();
	}

	protected void split_words() {
		tselected = -1;
		parts = new String[0];
		if (full_text.length() > 0) {
			parts = full_text.split(" ");
			if (parts.length > 0)
				tselected = ((int) (parts.length * Math.random()) % parts.length);
		}
	}

	public void setText(Object arg) {
		full_text = arg.toString();
		split_words();
	}

	@Override
	public void update(Cell cell) {
		if (cell.dim_w <= 0.0 || parts.length < 1)
			return;
		if ((CurrentState.frame % 180) == 0)
			tselected = ((int) (parts.length * Math.random()) % parts.length);
		if (tselected >= 0) {
			// TODO Words
//			gc.setTextAlign(TextAlignment.CENTER);
//			gc.setFont(font);
//			gc.setFill(cell.cfill);
//			double px = ((cell.pos_x) * 0.5 + 0.5) * Habiat.SCREEN_WIDTH;
//			double py = (((cell.pos_y * -1.0) + 0.25 * cell.dim_h) * 0.5 + 0.5) * Habiat.SCREEN_HEIGHT;
//			double pw = cell.dim_w * 0.5 *Habiat.SCREEN_WIDTH;
//			gc.fillText(parts[(tselected + cell.index) % parts.length], px, py, pw);
		}
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
	}

	@Override
	public int numTriangls() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float[] collisionShape() {
		// TODO Auto-generated method stub
		return new float[0];
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
	}

	@Override
	public String toString() {
		return "Words []";
	}

}
