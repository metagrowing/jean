package jean.render;


import jean.model.Cell;
import jean.render.lwjgl.Shape;

public class Oval extends Shape implements IStamp {
	protected float[] cpositions;

	public Oval() {
		super();
		init();
	}

	@Override
	public void init() {
        int num_segments = 60;
        positions = new float[3 * (num_segments + 2)];
        int j = 0;
        // center of triangle fan
        positions[j++] = 0.0f;
        positions[j++] = 0.0f;
        positions[j++] = 0.0f;
        // Last vertex same as first vertex
        for (int i = 0; i <= num_segments; i++) {
           double angle = i * 2.0f * Math.PI / num_segments;
           positions[j++] = (float) Math.cos(angle);
           positions[j++] = (float) Math.sin(angle);
           positions[j++] = 0.0f;
        }
        triangle_fan_draw_mode = true;
        super.is_out_of_sync = true;
	}

	@Override
	public float[] collisionShape() {
		if (cpositions == null) {
			int num_segments = 6;
			cpositions = new float[9 * num_segments];
			int j = 0;
			for (int i = 0; i < num_segments; i++) {
				cpositions[j++] = 0.0f;
				cpositions[j++] = 0.0f;
				cpositions[j++] = 0.0f;
				double angle1 = i * 2.0f * Math.PI / num_segments;
				cpositions[j++] = (float) Math.cos(angle1);
				cpositions[j++] = (float) Math.sin(angle1);
				cpositions[j++] = 0.0f;
				double angle2 = (i + 1) * 2.0f * Math.PI / num_segments;
				cpositions[j++] = (float) Math.cos(angle2);
				cpositions[j++] = (float) Math.sin(angle2);
				cpositions[j++] = 0.0f;
			}

		}
		return cpositions;
	}

	@Override
	public void update(Cell cell) {
	}

	@Override
	public String toString() {
		return "Oval []";
	}

}
