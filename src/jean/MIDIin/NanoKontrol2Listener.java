package jean.MIDIin;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiMessage;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

public class NanoKontrol2Listener implements Receiver {
	static public double ks0, ks1, ks2, ks3, ks4, ks5, ks6, ks7;
	static public double kp0, kp1, kp2, kp3, kp4, kp5, kp6, kp7;
	static public double kb0, kb1, kb2, kb3, kb4, kb5, kb6, kb7;
	private MidiDevice listen_to_device = null;

	public void setup() {
		ks0 = ks1 = ks2 = ks3 = ks4 = ks5 = ks6 = ks7 = 0.0;
	    kp0 = kp1 = kp2 = kp3 = kp4 = kp5 = kp6 = kp7 = 0.0;
		kb0 = kb1 = kb2 = kb3 = kb4 = kb5 = kb6 = kb7 = 0.0;
		listen_to_device = null;
		
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		for (int i = 0; i < infos.length; i++) {
			try {
//				System.out.println(infos[i]);
				if (infos[i].getName().indexOf("nanoKONTROL2") >= 0
						|| infos[i].getDescription().indexOf("nanoKONTROL2") >= 0) {
					listen_to_device = MidiSystem.getMidiDevice(infos[i]);
//					System.out.println(listen_to_device + " tm " + listen_to_device.getMaxTransmitters());
//					System.out.println(listen_to_device + " t " + listen_to_device.getTransmitters().size());
//					System.out.println(listen_to_device + " rm " + listen_to_device.getMaxReceivers());
//					System.out.println(listen_to_device + " r " + listen_to_device.getReceivers().size());

					if (listen_to_device.getMaxTransmitters() != 0) {
//						System.out.println(listen_to_device + " T1 " + listen_to_device.getTransmitter());
						listen_to_device.open();
						listen_to_device.getTransmitter().setReceiver(this);
//						System.out.println(listen_to_device + " T2 " + listen_to_device.getTransmitter());
						System.out.println("connected to " + infos[i]);
						break;
					}
//					System.out.println(listen_to_device + " t " + listen_to_device.getTransmitters().size());
//					System.out.println(listen_to_device + " r " + listen_to_device.getReceivers().size());
				}
			} catch (MidiUnavailableException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void close() {
	}

	@Override
	public void send(MidiMessage msg, long timeStamp) {
		if (msg instanceof ShortMessage && msg.getStatus() == ShortMessage.CONTROL_CHANGE) {
			ShortMessage message = (ShortMessage) msg;
//			System.out.println("midi received " + message.getData1() + " " + message.getData2() + " ");
			final double max_val = 128.0;
			switch (message.getData1()) {
			// slider
			case 0x0:
				ks0 = (double) message.getData2() / max_val;
				return;
			case 0x1:
				ks1 = (double) message.getData2() / max_val;
				return;
			case 0x2:
				ks2 = (double) message.getData2() / max_val;
				return;
			case 0x3:
				ks3 = (double) message.getData2() / max_val;
				return;
			case 0x4:
				ks4 = (double) message.getData2() / max_val;
				return;
			case 0x5:
				ks5 = (double) message.getData2() / max_val;
				return;
			case 0x6:
				ks6 = (double) message.getData2() / max_val;
				return;
			case 0x7:
				ks7 = (double) message.getData2() / max_val;
				return;

			// potentiometer
			case 0x10:
				kp0 = (double) message.getData2() / max_val;
				return;
			case 0x11:
				kp1 = (double) message.getData2() / max_val;
				return;
			case 0x12:
				kp2 = (double) message.getData2() / max_val;
				return;
			case 0x13:
				kp3 = (double) message.getData2() / max_val;
				return;
			case 0x14:
				kp4 = (double) message.getData2() / max_val;
				return;
			case 0x15:
				kp5 = (double) message.getData2() / max_val;
				return;
			case 0x16:
				kp6 = (double) message.getData2() / max_val;
				return;
			case 0x17:
				kp7 = (double) message.getData2() / max_val;
				return;

			// push button
			case 0x20:
				if (message.getData2() > 0)
					kb0 = 1;
				return;
			case 0x30:
				if (message.getData2() > 0)
					kb0 = 0;
				return;
			case 0x40:
				if (message.getData2() > 0)
					kb0 = -1;
				return;

			case 0x21:
				if (message.getData2() > 0)
					kb1 = 1;
				return;
			case 0x31:
				if (message.getData2() > 0)
					kb1 = 0;
				return;
			case 0x41:
				if (message.getData2() > 0)
					kb1 = -1;
				return;

			case 0x22:
				if (message.getData2() > 0)
					kb2 = 1;
				return;
			case 0x32:
				if (message.getData2() > 0)
					kb2 = 0;
				return;
			case 0x42:
				if (message.getData2() > 0)
					kb2 = -1;
				return;

			case 0x23:
				if (message.getData2() > 0)
					kb3 = 1;
				return;
			case 0x33:
				if (message.getData2() > 0)
					kb3 = 0;
				return;
			case 0x43:
				if (message.getData2() > 0)
					kb3 = -1;
				return;

			case 0x24:
				if (message.getData2() > 0)
					kb4 = 1;
				return;
			case 0x34:
				if (message.getData2() > 0)
					kb4 = 0;
				return;
			case 0x44:
				if (message.getData2() > 0)
					kb4 = -1;
				return;

			case 0x25:
				if (message.getData2() > 0)
					kb5 = 1;
				return;
			case 0x35:
				if (message.getData2() > 0)
					kb5 = 0;
				return;
			case 0x45:
				if (message.getData2() > 0)
					kb5 = -1;
				return;

			case 0x26:
				if (message.getData2() > 0)
					kb6 = 1;
				return;
			case 0x36:
				if (message.getData2() > 0)
					kb6 = 0;
				return;
			case 0x46:
				if (message.getData2() > 0)
					kb6 = -1;
				return;

			case 0x27:
				if (message.getData2() > 0)
					kb7 = 1;
				return;
			case 0x37:
				if (message.getData2() > 0)
					kb7 = 0;
				return;
			case 0x47:
				if (message.getData2() > 0)
					kb7 = -1;
				return;

			default:
				return;
			}
		}
	}

	protected void finalize() throws Throwable {
		if (listen_to_device != null && listen_to_device.isOpen()) {
			listen_to_device.close();
		}
	}

}
