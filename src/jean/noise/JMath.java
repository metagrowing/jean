package jean.noise;

public class JMath {
	
	public static float fractional_0_1(float value) {
		return value < 0.0f ? 1.0f - (-value % 1.0f): value % 1.0f;
	}

	public static float clamp_0_1(float value) {
		if (value > 1.0f)
			return 1.0f;
		if (value < 0.0f)
			return 0.0f;
		return value;
	}

	public static double mix(double x, double y, double a) {
		return x * (1.0 - a) + y * a;
	}
	
	public static double step(double p1, double p2) {
        return p2 > p1 ? 1.0 : 0.0;
	}
}
