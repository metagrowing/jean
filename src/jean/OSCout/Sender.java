package jean.OSCout;

import java.io.IOException;
import java.net.InetAddress;

import com.illposed.osc.transport.OSCPortOut;

public abstract class Sender {
	//TODO Warning if sclang is not listening at port 57120
	public final int SC_LANG_PORT = 57120;
	public final int SC_SYNTH_PORT = 57110;
	protected static OSCPortOut scLangPort = null;
	protected static OSCPortOut scSynthPort = null;

	public Sender() {
		InetAddress local_scsynth = InetAddress.getLoopbackAddress();
		if(scLangPort == null) {
			try {
				scLangPort = new OSCPortOut(local_scsynth, SC_LANG_PORT);
				System.out.println("outgoing SuperCollider lang port " + scLangPort);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(scSynthPort == null) {
			try {
				scSynthPort = new OSCPortOut(local_scsynth, SC_SYNTH_PORT);
				System.out.println("outgoing SuperCollider synth port " + scSynthPort);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
