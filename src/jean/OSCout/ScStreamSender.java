package jean.OSCout;

import java.util.ArrayList;
import java.util.List;

import com.illposed.osc.OSCMessage;

import jean.application.Jean;
import jean.model.Cell;

public class ScStreamSender extends jean.OSCout.Sender {
	private List<Object> args = new ArrayList<>(13);

	public ScStreamSender() {
		args.add((int) 0);   //  0 stream id
		args.add((int) 0);   //  1 characteristic
		args.add((int) 0);   //  2 cluster index
		args.add((int) 0);   //  3 cell index
		args.add((float) 0); //  4 cell x
		args.add((float) 0); //  5 cell y
		args.add((float) 0); //  6 cell delta x
		args.add((float) 0); //  7 cell delta y
		args.add((float) 0); //  8 cell w
		args.add((float) 0); //  9 cell h
		args.add((float) 0); // 10 cell color hue 
		args.add((float) 0); // 11 cell color saturation
		args.add((float) 0); // 12 cell color value
		args.add((float) 0); // 13 cell color alpha
	}

	public void send(Cell cell) {
		if(!Jean.send_osc)
			return;
		
//		System.out.println("S " + cell);
		try {
			args.set(0,  (int) cell.cluster.stream);
			args.set(1,  (int) cell.cluster.characteristic);
			args.set(2,  (int) cell.cluster.cluster_index);
			args.set(3,  (int) cell.index);
			args.set(4,  (float) cell.pos_x);
			args.set(5,  (float) cell.pos_y);
			args.set(6,  (float) cell.delta_x);
			args.set(7,  (float) cell.delta_y);
			args.set(8,  (float) cell.dim_w);
			args.set(9,  (float) cell.dim_h);
			args.set(10, (float) cell.color.h);
			args.set(11, (float) cell.color.s);
			args.set(12, (float) cell.color.v);
			args.set(13, (float) cell.color.a);

			OSCMessage msg = new OSCMessage("/j_stream", args);
			scLangPort.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
