package jean.OSCout;

import java.util.ArrayList;
import java.util.List;

import com.illposed.osc.OSCMessage;

import jean.application.Jean;
import jean.model.Cell;
import jean.model.Cluster;

public class ScSynthSender extends jean.OSCout.Sender {
	private final List<Object> synth_args = new ArrayList<>(24);
	private String synth_name = "";
	private boolean has_synth = false;

	public ScSynthSender(String arg) {
		setSynth(arg);
		synth_args.add("");
		synth_args.add(-1);
		synth_args.add(0);
		synth_args.add(0);
		synth_args.add("cai");
		synth_args.add((int) 0);
		synth_args.add("cax");
		synth_args.add((float) 0);
		synth_args.add("cay");
		synth_args.add((float) 0);
		synth_args.add("cadx");
		synth_args.add((float) 0);
		synth_args.add("cady");
		synth_args.add((float) 0);
		synth_args.add("caw");
		synth_args.add((float) 0);
		synth_args.add("cah");
		synth_args.add((float) 0);
		synth_args.add("cui");
		synth_args.add((int) 0);
		synth_args.add("cux");
		synth_args.add((float) 0);
		synth_args.add("cuy");
		synth_args.add((float) 0);
		synth_args.add("cudx");
		synth_args.add((float) 0);
		synth_args.add("cudy");
		synth_args.add((float) 0);
	}

	public void send_synth_new(Cluster calve_cluster, Cluster cursor_cluster, Cell calve_cell, Cell cursor_cell) {
		if (!Jean.send_osc)
			return;
		if (!has_synth)
			return;

		try {
			synth_args.set(0, synth_name);
			synth_args.set(5, (int) calve_cell.index);
			synth_args.set(7, (float) calve_cell.pos_x);
			synth_args.set(9, (float) calve_cell.pos_y);
			synth_args.set(11, (float) calve_cell.delta_x);
			synth_args.set(13, (float) calve_cell.delta_y);
			synth_args.set(15, (float) calve_cell.dim_w);
			synth_args.set(17, (float) calve_cell.dim_h);
			synth_args.set(19, (int) cursor_cell.index);
			synth_args.set(21, (float) cursor_cell.pos_x);
			synth_args.set(23, (float) cursor_cell.pos_y);
			synth_args.set(25, (float) cursor_cell.delta_x);
			synth_args.set(27, (float) cursor_cell.delta_y);

			OSCMessage msg = new OSCMessage("/s_new", synth_args);
			scSynthPort.send(msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setSynth(String synth) {
		synth_name = "";
		has_synth = false;
		if(synth != null) {
			synth_name = synth.startsWith(":") ? synth.substring(1) : synth;
			has_synth = synth_name.length() > 0;
		}
	}
}
