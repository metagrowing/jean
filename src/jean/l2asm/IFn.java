package jean.l2asm;

import jean.model.Cell;
import jean.model.Cluster;

public interface IFn {
	double expr(Cell cell, Cluster cluster);
}
