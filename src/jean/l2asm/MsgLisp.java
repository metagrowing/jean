package jean.l2asm;

public class MsgLisp {
	public MsgLisp() {
	}

	private boolean isNumberStart(char test) {
	    switch(test) {
	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
	    case '.':
	    case '+':
	    case '-':
	        return true;
	    }
	    return false;
	}
	
	private boolean isNumber(char test) {
	    switch(test) {
	    case '0':
	    case '1':
	    case '2':
	    case '3':
	    case '4':
	    case '5':
	    case '6':
	    case '7':
	    case '8':
	    case '9':
	    case '.':
	        return true;
	    }
	    return false;
	}
	
	private boolean isVarNameStart(char test) {
	    switch(test) {
	    case 'i': // index
	    case 'f': // animation frame number
	    case 'l': // cell frame number
	    case 'e': // has entered
	    case 'k': // nanoKontrol2
	    case 'x':
	    case 'y':
	    case 'w':
	    case 'h':
	    case 'b': // birth rate
	    case 'H':
	    case 'S':
	    case 'V':
	    case 'A':
	    case 'd': // decay / dx / dy
	    case 't': // t0 / t1 ...
	    case 'u': // u0 / u1 ...
	        return true;
	    }
	    return false;
	}

	private boolean isVarName(char test) {
		return Character.isLetterOrDigit(test) || test == '_';
//	    switch(test) {
//	    case '0':
//	    case '1':
//	    case '2':
//	    case '3':
//	    case '4':
//	    case '5':
//	    case '6':
//	    case '7':
//	    case '8':
//	    case '9':
//	    case 's':
//	    case 'p':
//	    case 'b':
//	    case '_':
//	        return true;
//	    }
//	    return false;
	}
	
	public ast_m compile(String src) {
	    ast_m r = null;
	    ix = 0;
	    len = src.length();
	    if(len > 0) {
	        inp = new StringBuffer(src);
	        skip();
	        if(src.charAt(ix) == '(') {
	            ++ix;
	            r = new ast_m("(");
	            collect(r);
	        }
	        else if(isNumberStart(src.charAt(ix))) {
	            tokBuffer = new StringBuffer();
                tokBuffer.append(src.charAt(ix));
                ++ix;
	            while(ix < len && isNumber(src.charAt(ix))) {
	                tokBuffer.append(src.charAt(ix));
	                ++ix;
	            }
	            r = new ast_m(tokBuffer);
	        }
	        else if(isVarNameStart(src.charAt(ix))) {
	            tokBuffer = new StringBuffer();
                tokBuffer.append(src.charAt(ix));
                ++ix;
	            while(ix < len && isVarName(src.charAt(ix))) {
	                tokBuffer.append(src.charAt(ix));
	                ++ix;
	            }
	            r = new ast_m(tokBuffer);
	        }
	    }
	    return r;
	}
	
//	public ast_m compile(String src) {
//	    ast_m r = null;
//	    ix = 0;
//	    len = src.length();
//	    if(len > 0) {
//	        inp = new StringBuffer(src);
//	        skip();
//	        r = new ast_m("(");
//	        collect(r);
//	    }
//	    return r;
//	}

	private void skip() {
	    while(ix < len && Character.isWhitespace(inp.charAt(ix))) {
	        ++ix;
	    }
	}

	private void collect(ast_m r) {
	    while(ix < len) {
	        skip();
	        char ca = inp.charAt(ix);
	        switch(ca) {
	        case '(': {
	            ++ix;
	            ast_m c = new ast_m("(");
	            collect(c);
	            r.addChild(c);
	            break;
	        }
	        case '[': {
	            ++ix;
	            ast_m c = new ast_m("[");
	            collect(c);
	            r.addChild(c);
	            break;
	        }
	        case ')':
	        case ']':
	            ++ix;
	            return;
	        default:
	            tokBuffer = new StringBuffer();
	            while(ix < len && !(Character.isWhitespace(ca)
	                    || ca == '(' || ca == ')'
	                    || ca == '[' || ca == ']')) {
	                tokBuffer.append(ca);
                	++ix;
    	        	ca = ix < len ? inp.charAt(ix) : '\0';
	            }
	            ast_m n = new ast_m(tokBuffer);
	            r.addSibling(n);
	            break;
	        }
	    }
	}

	private int len = 0;
	private StringBuffer tokBuffer = new StringBuffer();
	private int ix = 0;
	private StringBuffer inp;

	public String dump_ast(ast_m node, int depth) {
	    StringBuffer dump = new StringBuffer();
	    if(node != null) {
	        for(var i=0; i<depth; ++i) {
	            dump.append("  ");
	        }
	        dump.append("|");
	        dump.append(depth);
	        dump.append("|");
	        dump.append(node.count());
	        dump.append(":");
	        dump.append(node.tok);
	        dump.append("\n");

	        for(int i=0; i<node.count(); ++i) {
	            ast_m c = node.at(i);
	            if(node.isChild(i)) {
	                dump.append(dump_ast(c, depth+1));
	            }
	            else {
	                for(var j=0; j<=depth; ++j) {
	                    dump.append("  ");
	                }
	                dump.append("|");
	                dump.append(c.tok);
	                dump.append("\n");
	            }
	        }
	    }
	    return dump.toString();
	}
}
