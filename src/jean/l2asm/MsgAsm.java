package jean.l2asm;

import java.security.InvalidParameterException;

import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class MsgAsm implements Opcodes {

	public static byte[] generate(ast_m node, String slot) throws Exception {

		ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);
		MethodVisitor methodVisitor;

//TODO F_SAME
//		classWriter.visit(V11, ACC_PUBLIC | ACC_SUPER, "jean/l2asm/Fn"+slot, null, "java/lang/Object",
		classWriter.visit(V1_5, ACC_PUBLIC | ACC_SUPER, "jean/l2asm/Fn"+slot, null, "java/lang/Object",
				new String[] { "jean/l2asm/IFn" });

		{
			methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
			methodVisitor.visitCode();
			Label label0 = new Label();
			methodVisitor.visitLabel(label0);
			methodVisitor.visitVarInsn(ALOAD, 0);
			methodVisitor.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V", false);
			methodVisitor.visitInsn(RETURN);
			Label label1 = new Label();
			methodVisitor.visitLabel(label1);
			methodVisitor.visitLocalVariable("this", "Ljean/l2asm/Fn"+slot+";", null, label0, label1, 0);
			methodVisitor.visitMaxs(1, 1);
			methodVisitor.visitEnd();
		}
		{
			methodVisitor = classWriter.visitMethod(ACC_PUBLIC, "expr", "(Ljean/model/Cell;Ljean/model/Cluster;)D", null, null);
			methodVisitor.visitCode();
			Label label0 = new Label();
			methodVisitor.visitLabel(label0);
			
			generate_list(node, methodVisitor, slot);
			
			methodVisitor.visitInsn(DRETURN);
			Label label1 = new Label();
			methodVisitor.visitLabel(label1);
			methodVisitor.visitLocalVariable("this", "Ljean/l2asm/Fn"+slot+";", null, label0, label1, 0);
			methodVisitor.visitLocalVariable("cell", "Ljean/model/Cell;", null, label0, label1, 1);
			methodVisitor.visitLocalVariable("cluster", "Ljean/model/Cluster;", null, label0, label1, 2);
			methodVisitor.visitMaxs(0, 0);
			methodVisitor.visitEnd();
		}
		classWriter.visitEnd();

		return classWriter.toByteArray();
	}
	
	private static void generate_fetch_value(MethodVisitor methodVisitor, ast_m node, String slot) {
		String token = node.tok.toString(); 
		if(node.tok.toString().equals("f")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "frame", "I");
			methodVisitor.visitInsn(I2D);
		}
		else if(node.tok.toString().equals("f1")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "frame", "I");
			methodVisitor.visitInsn(ICONST_1);
			methodVisitor.visitInsn(IADD);
			methodVisitor.visitInsn(I2D);
		}
		else if(node.tok.toString().equals("l")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "frame", "I");
			methodVisitor.visitInsn(I2D);
		}
		else if(node.tok.toString().equals("l1")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "frame", "I");
			methodVisitor.visitInsn(ICONST_1);
			methodVisitor.visitInsn(IADD);
			methodVisitor.visitInsn(I2D);
		}
		else if(token.equals("i")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitInsn(I2D);
		}
		else if(token.equals("i1")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitInsn(ICONST_1);
			methodVisitor.visitInsn(IADD);
			methodVisitor.visitInsn(I2D);
		}
		else if(token.equals("id")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "id", "I");
			methodVisitor.visitInsn(I2D);
		}
		else if(token.equals("id1")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "id", "I");
			methodVisitor.visitInsn(ICONST_1);
			methodVisitor.visitInsn(IADD);
			methodVisitor.visitInsn(I2D);
		}
		else if(token.equals("e")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "jean/model/Cell", "has_entered", "()D", false);
		}
		else if(token.equals("ks0")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks0", "D");
		}
		else if(token.equals("ks1")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks1", "D");
		}
		else if(token.equals("ks2")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks2", "D");
		}
		else if(token.equals("ks3")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks3", "D");
		}
		else if(token.equals("ks4")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks4", "D");
		}
		else if(token.equals("ks5")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks5", "D");
		}
		else if(token.equals("ks6")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks6", "D");
		}
		else if(token.equals("ks7")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "ks7", "D");
		}
		else if(token.equals("kp0")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp0", "D");
		}
		else if(token.equals("kp1")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp1", "D");
		}
		else if(token.equals("kp2")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp2", "D");
		}
		else if(token.equals("kp3")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp3", "D");
		}
		else if(token.equals("kp4")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp4", "D");
		}
		else if(token.equals("kp5")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp5", "D");
		}
		else if(token.equals("kp6")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp6", "D");
		}
		else if(token.equals("kp7")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kp7", "D");
		}
		else if(token.equals("kb0")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb0", "D");
		}
		else if(token.equals("kb1")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb1", "D");
		}
		else if(token.equals("kb2")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb2", "D");
		}
		else if(token.equals("kb3")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb3", "D");
		}
		else if(token.equals("kb4")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb4", "D");
		}
		else if(token.equals("kb5")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb5", "D");
		}
		else if(token.equals("kb6")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb6", "D");
		}
		else if(token.equals("kb7")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/MIDIin/NanoKontrol2Listener", "kb7", "D");
		}
		else if(token.equals("u0")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u0", "D");
		}
		else if(token.equals("u1")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u1", "D");
		}
		else if(token.equals("u2")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u2", "D");
		}
		else if(token.equals("u3")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u3", "D");
		}
		else if(token.equals("u4")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u4", "D");
		}
		else if(token.equals("u5")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u5", "D");
		}
		else if(token.equals("u6")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u6", "D");
		}
		else if(token.equals("u7")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "u7", "D");
		}
		else if(token.equals("t0")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t0", "D");
		}
		else if(token.equals("t1")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t1", "D");
		}
		else if(token.equals("t2")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t2", "D");
		}
		else if(token.equals("t3")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t3", "D");
		}
		else if(token.equals("t4")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t4", "D");
		}
		else if(token.equals("t5")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t5", "D");
		}
		else if(token.equals("t6")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t6", "D");
		}
		else if(token.equals("t7")) {
			methodVisitor.visitFieldInsn(GETSTATIC, "jean/model/CurrentState", "t7", "D");
		}
		else if(token.equals("dec")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "decay", "D");
		}
		else if(token.equals("br")) {
			methodVisitor.visitVarInsn(ALOAD, 2);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cluster", "birthrate", "D");
		}
		else if(token.equals("dx")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "delta_x", "D");
		}
		else if(token.equals("dy")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "delta_y", "D");
		}
		else if(token.equals("w")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "dim_w", "D");
		}
		else if(token.equals("h")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "dim_h", "D");
		}
		else if(token.equals("H")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "color_h", "D");
		}
		else if(token.equals("S")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "color_s", "D");
		}
		else if(token.equals("V")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "color_v", "D");
		}
		else if(token.equals("A")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "color_a", "D");
		}
		else if(token.equals("x")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "pos_x", "D");
		}
		else if(token.equals("y")) {
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "pos_y", "D");
		}
		else if(token.equals("x_")) {
			methodVisitor.visitVarInsn(ALOAD, 2);
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "jean/model/Cluster", "x_up", "(I)D", false);
		}
		else if(token.equals("y_")) {
			methodVisitor.visitVarInsn(ALOAD, 2);
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "jean/model/Cluster", "y_up", "(I)D", false);
		}
		else if(token.equals("x__")) {
			methodVisitor.visitVarInsn(ALOAD, 2);
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "jean/model/Cluster", "x_upup", "(I)D", false);
		}
		else if(token.equals("y__")) {
			methodVisitor.visitVarInsn(ALOAD, 2);
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "jean/model/Cluster", "y_upup", "(I)D", false);
		}
		else if(token.equals("x___")) {
			methodVisitor.visitVarInsn(ALOAD, 2);
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "jean/model/Cluster", "x_upupup", "(I)D", false);
		}
		else if(token.equals("y___")) {
			methodVisitor.visitVarInsn(ALOAD, 2);
			methodVisitor.visitVarInsn(ALOAD, 1);
			methodVisitor.visitFieldInsn(GETFIELD, "jean/model/Cell", "index", "I");
			methodVisitor.visitMethodInsn(INVOKEVIRTUAL, "jean/model/Cluster", "y_upupup", "(I)D", false);
		}
		else {
			methodVisitor.visitLdcInsn(Double.parseDouble(token));
		}
	}
	
	private static void generate_arity_1(ast_m node, MethodVisitor methodVisitor, String owner, String name, String descriptor, String slot) throws Exception {
		if(node.count() == 2) {
		    ast_m ci = node.at(1);
			generate_list(ci, methodVisitor, slot);
			methodVisitor.visitMethodInsn(INVOKESTATIC, owner, name, descriptor, false);
		}
		else throw new InvalidParameterException("expexted 1 parameter for operator '" + name + "' in " + slot);
	}

	private static void generate_arity_2(ast_m node, MethodVisitor methodVisitor, String owner, String name, String slot) throws Exception {
		if(node.count() == 3) {
			generate_list_or_value(methodVisitor, node.at(1), slot);
			generate_list_or_value(methodVisitor, node.at(2), slot);
			methodVisitor.visitMethodInsn(INVOKESTATIC, owner, name, "(DD)D", false);
		}
		else throw new InvalidParameterException("expexted 2 parameter for operator '" + name + "' in " + slot);
	}

	private static void generate_arity_3(ast_m node, MethodVisitor methodVisitor, String owner, String name, String slot) throws Exception {
		if(node.count() == 4) {
			for(int i=1; i<4; ++i) {
				ast_m ci = node.at(i);
				generate_list_or_value(methodVisitor, ci, slot);
			}
			methodVisitor.visitMethodInsn(INVOKESTATIC, owner, name, "(DDD)D", false);
		}
		else throw new InvalidParameterException("expexted 3 parameter for operator '" + name + "' in " + slot);
	}

	private static void generate_arity_n(ast_m node, MethodVisitor methodVisitor, int opr, String slot) throws Exception {
		for(int i=1; i<node.count(); ++i) {
		    ast_m ci = node.at(i);
		    generate_list_or_value(methodVisitor, ci, slot);
		    if(i >= 2)
		    	methodVisitor.visitInsn(opr);
		}
	}

	private static void generate_branch_pos(ast_m node, MethodVisitor methodVisitor, String slot) throws Exception {
		Label label1 = new Label();
		Label label2 = new Label();
		
		generate_list_or_value(methodVisitor, node.at(1), slot);
		methodVisitor.visitInsn(DCONST_0);
		methodVisitor.visitInsn(DCMPL);
		
		methodVisitor.visitJumpInsn(IFLT, label1);
		generate_list_or_value(methodVisitor, node.at(2), slot);
		
		methodVisitor.visitJumpInsn(GOTO, label2);
		methodVisitor.visitLabel(label1);
		generate_list_or_value(methodVisitor, node.at(3), slot);
		
		methodVisitor.visitLabel(label2);
	}

	private static void generate_branch_zero(ast_m node, MethodVisitor methodVisitor, String slot) throws Exception {
		Label label1 = new Label();
		Label label2 = new Label();
		
		generate_list_or_value(methodVisitor, node.at(1), slot);
		methodVisitor.visitInsn(DCONST_0);
		methodVisitor.visitInsn(DCMPL);

		methodVisitor.visitJumpInsn(IFNE, label1);		
		generate_list_or_value(methodVisitor, node.at(2), slot);
		
		methodVisitor.visitJumpInsn(GOTO, label2);
		methodVisitor.visitLabel(label1);
		generate_list_or_value(methodVisitor, node.at(3), slot);
		
		methodVisitor.visitLabel(label2);
	}

	private static void generate_branch_even(ast_m node, MethodVisitor methodVisitor, String slot) throws Exception {
		Label label1 = new Label();
		Label label2 = new Label();
		
		generate_list_or_value(methodVisitor, node.at(1), slot);
		methodVisitor.visitInsn(D2I);
		methodVisitor.visitInsn(ICONST_2);
		methodVisitor.visitInsn(IREM);

		methodVisitor.visitJumpInsn(IFNE, label1);		
		generate_list_or_value(methodVisitor, node.at(2), slot);
		
		methodVisitor.visitJumpInsn(GOTO, label2);
		methodVisitor.visitLabel(label1);
		generate_list_or_value(methodVisitor, node.at(3), slot);
		
		methodVisitor.visitLabel(label2);
	}

	private static void generate_list_or_value(MethodVisitor methodVisitor, ast_m node, String slot) throws Exception {
		if(node.tok.toString().equals("(")) {
			generate_list(node, methodVisitor, slot);
		}
		else {
			generate_fetch_value(methodVisitor, node, slot);
		}
	}

	public static void generate_list(ast_m node, MethodVisitor methodVisitor, String slot) throws Exception {
		if(node.count() == 0) {
			// assume an 'expression' with only a single number or single variable
			// 3
			// i
            generate_fetch_value(methodVisitor, node, slot);
		}
		else if(node.count() == 1) {
			// assume operator without parameters
			// (+)
			// (*)
            ast_m c0 = node.at(0);
            String ts = c0.tok.toString();
            
            if(ts.equals("+"))
    			methodVisitor.visitLdcInsn(0.0);
            else if(ts.equals("*"))
    			methodVisitor.visitLdcInsn(1.0);
		}
		else if(node.count() > 1) {
            ast_m c0 = node.at(0);
            String ts = c0.tok.toString();
            
            if(ts.equals("+")) {
    	        generate_arity_n(node, methodVisitor, DADD, slot);
            }
            else if(ts.equals("-")) {
    	        generate_arity_n(node, methodVisitor, DSUB, slot);
            }
            else if(ts.equals("*")) {
    	        generate_arity_n(node, methodVisitor, DMUL, slot);
            }
            else if(ts.equals("/")) {
    	        generate_arity_n(node, methodVisitor, DDIV, slot);
            }
            else if(ts.equals("sin")) {
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "sin", "(D)D", slot);
            }
            else if(ts.equals("usin")) {
            	methodVisitor.visitLdcInsn(Double.valueOf(0.5));
            	methodVisitor.visitLdcInsn(Double.valueOf(0.5));
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "sin", "(D)D", slot);
            	methodVisitor.visitInsn(DMUL);
            	methodVisitor.visitInsn(DADD);            }
            else if(ts.equals("cos")) {
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "cos", "(D)D", slot);
            }
            else if(ts.equals("ucos")) {
            	methodVisitor.visitLdcInsn(Double.valueOf(0.5));
            	methodVisitor.visitLdcInsn(Double.valueOf(0.5));
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "cos", "(D)D", slot);
            	methodVisitor.visitInsn(DMUL);
            	methodVisitor.visitInsn(DADD);            }
            else if(ts.equals("sign")) {
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "signum", "(D)D", slot);
            }
            else if(ts.equals("step")) {
            	generate_arity_2(node, methodVisitor, "jean/noise/JMath", "step", slot);
            }
            else if(ts.equals("sqr")) {
        		if(node.count() == 2) {
        		    ast_m ci = node.at(1);
        			generate_list(ci, methodVisitor, slot);
                	methodVisitor.visitInsn(D2F);
                	methodVisitor.visitInsn(DUP);
                	methodVisitor.visitInsn(FMUL);
                	methodVisitor.visitInsn(F2D);
        		}
        		else throw new InvalidParameterException("expexted 1 parameter for operator sqr");
            }
            else if(ts.equals("sqrt")) {
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "abs", "(D)D", slot);
            	methodVisitor.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "sqrt", "(D)D", false);
            }
            else if(ts.equals("log")) {
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "abs", "(D)D", slot);
            	methodVisitor.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "log", "(D)D", false);
            }
            else if(ts.equals("round")) {
            	generate_arity_1(node, methodVisitor, "java/lang/Math", "round", "(D)J", slot);
            	methodVisitor.visitInsn(L2D);
            }
            else if(ts.equals("%")) {
        		if(node.count() == 3) {
    				generate_list_or_value(methodVisitor, node.at(1), slot);
    				generate_list_or_value(methodVisitor, node.at(2), slot);
    				methodVisitor.visitInsn(DREM);
        		}
            }
            else if(ts.equals("max")) {
               	generate_arity_2(node, methodVisitor, "java/lang/Math", "max", slot);
            }
            else if(ts.equals("min")) {
               	generate_arity_2(node, methodVisitor, "java/lang/Math", "min", slot);
            }
            else if(ts.equals("clamp")) {
        		if(node.count() == 4) {
    				generate_list_or_value(methodVisitor, node.at(1), slot);
    				generate_list_or_value(methodVisitor, node.at(2), slot);
    				methodVisitor.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "max", "(DD)D", false);
    				generate_list_or_value(methodVisitor, node.at(3), slot);
    				methodVisitor.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "min", "(DD)D", false);
        		}
            }
            else if(ts.equals("noise")) {
            	generate_arity_3(node, methodVisitor, "jean/noise/ImprovedNoise", "noise", slot);
            }
            else if(ts.equals("snoise")) {
            	generate_arity_3(node, methodVisitor, "jean/noise/ImprovedNoise", "noise", slot);
            }
            else if(ts.equals("unoise")) {
            	generate_arity_3(node, methodVisitor, "jean/noise/ImprovedNoise", "unoise", slot);
            }
            else if(ts.equals("mix")) {
            	generate_arity_3(node, methodVisitor, "jean/noise/JMath", "mix", slot);
            }
            else if(ts.equals("pos?")) {
                generate_branch_pos(node, methodVisitor, slot);
            }
            else if(ts.equals("zero?")) {
                generate_branch_zero(node, methodVisitor, slot);
            }
            else if(ts.equals("even?")) {
                generate_branch_even(node, methodVisitor, slot);
            }
    		else throw new InvalidParameterException("Did not understand '" + ts + "' in " + slot);
		}
	}
}
