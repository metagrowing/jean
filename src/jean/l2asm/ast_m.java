package jean.l2asm;

import java.util.ArrayList;
import java.util.List;

public class ast_m {
	public ast_m(String t) {
		tok = new StringBuffer(t);
		child_count = 0;
	}

	public ast_m(StringBuffer t) {
		tok = t;
		child_count = 0;
	}

	public void addChild(ast_m a) {
		entry.add(a);
		type_flag.add(true);
		++child_count;
	}

	public void addSibling(ast_m a) {
		entry.add(a);
		type_flag.add(false);
	}

	public int count() {
		return entry.size();
	}

	public int childCount() {
		return child_count;
	}

	public boolean isChild(int index) {
		return index >= type_flag.size() ? false : type_flag.get(index);
	}

	public ast_m at(int index) {
		return index >= entry.size() ? null : entry.get(index);
	}

	StringBuffer tok;
	int child_count = 0;
	List<ast_m> entry = new ArrayList<ast_m>();
	List<Boolean> type_flag = new ArrayList<Boolean>();

}
