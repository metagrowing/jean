package jean.l2asm;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MsgLispTest {

	@Test
	void testCompile() {
	    MsgLisp l1 = new MsgLisp();
        String input = "(+ (* f 0.01) (* 10 i))";
        System.out.println(input);
        ast_m res = l1.compile(input);
        assertTrue(res != null);
        assertEquals(3, res.count());
        int i = 0;
        assertTrue(res.at(i).tok.toString().equals("+"), res.at(i).tok.toString());
        ++i;
        assertTrue(res.at(i).tok.toString().equals("("), res.at(i).tok.toString());
        ++i;
        assertTrue(res.at(i).tok.toString().equals("("), res.at(i).tok.toString());
	}

	@Test
	void testDumpAst1() {
		MsgLisp l1 = new MsgLisp();
		String input = "(+ (* f 0.01) (* 10 i))";
		System.out.println(input);
		ast_m res = l1.compile(input);
		String expected =
				"|0|3:(\n"
						+ "  |+\n"
						+ "  |1|3:(\n"
						+ "    |*\n"
						+ "    |f\n"
						+ "    |0.01\n"
						+ "  |1|3:(\n"
						+ "    |*\n"
						+ "    |10\n"
						+ "    |i\n";
		String dump = l1.dump_ast(res, 0);
		System.out.println(dump);
		assertEquals(expected, dump);
	}
	
	@Test
	void testDumpAst2() {
	    MsgLisp l1 = new MsgLisp();
        String input = "(noise 0.1 0.2 0.3)";
        System.out.println(input);
        ast_m res = l1.compile(input);
        String expected = "|0|4:(\n"
        		+ "  |noise\n"
        		+ "  |0.1\n"
        		+ "  |0.2\n"
        		+ "  |0.3\n";
        String dump = l1.dump_ast(res, 0);
        System.out.println(dump);
        assertEquals(expected, dump);
	}

	@Test
	void testDumpAst3() {
	    MsgLisp l1 = new MsgLisp();
        String input = "(pos? 0.1 0.2 0.3)";
        System.out.println(input);
        ast_m res = l1.compile(input);
        String expected = "|0|4:(\n"
        		+ "  |pos?\n"
        		+ "  |0.1\n"
        		+ "  |0.2\n"
        		+ "  |0.3\n";
        String dump = l1.dump_ast(res, 0);
        System.out.println(dump);
        assertEquals(expected, dump);
	}

}
