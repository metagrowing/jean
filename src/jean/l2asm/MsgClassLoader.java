package jean.l2asm;

import java.util.Arrays;

public class MsgClassLoader extends ClassLoader  {
	@SuppressWarnings("unchecked")
	public Class<IFn> defineClass(String name, byte[] b) throws Exception {
		Class<?> c = defineClass(name, b, 0, b.length);
		return (Class<IFn>) c;
	}

	@SuppressWarnings("unchecked")
	public Class<IFn> defineClass_debug(String name, byte[] b) throws Exception {
		Class<?> c = defineClass(name, b, 0, b.length);
		Class<?>[] interfaces = c.getInterfaces();
		System.out.println(name + " " + Arrays.asList(interfaces) + " " + Arrays.asList(interfaces).contains(IFn.class));
		Module m = this.getClass().getModule();
		Module mf = c.getModule();
		System.out.println(m + " " + mf);
//		if(interfaces.length == 1 && Arrays.asList(interfaces).contains(jean.l2asm.IFn.class))
			return (Class<IFn>) c;

//		throw new Exception(name + " " + b.length + " " + interfaces.length + " " + Arrays.asList(interfaces) + " " + Arrays.asList(interfaces).contains(IFn.class));
	}
}
