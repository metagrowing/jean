package jean.l2asm;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Test;

import jean.model.Cell;
import jean.model.Cluster;
import jean.model.CurrentState;
import jean.noise.ImprovedNoise;

class MsgAsmTest {

	@Test
	void testExp() {
		ImprovedNoise.seed(1512);
        doExpression(0, "ks0");
        doExpression(0, "(+ ks0 ks1 ks2 ks3 ks4 ks5 ks6 ks7)");
        doExpression(0, "(+ kp0 kp1 kp2 kp3 kp4 kp5 kp6 kp7)");
        doExpression(0, "(+ kb0 kb1 kb2 kb3 kb4 kb5 kb6 kb7)");
        doExpression(0, "(* 0 (+ u0 u1 u2 u3 u4 u5 u6 u7))");
        doExpression(0, "(+ t0 t1 t2 t3 t4 t5 t6 t7)");

        doExpression(3.0, "3");
        doExpression(-3.1, "-3.1");
        doExpression(5.0, "f");
        doExpression(6.0, "f1");
        doExpression(5.0, "(- f1 1)");
        doExpression(11.0, "l");
        doExpression(12.0, "l1");
        doExpression(7.0, "i");
        doExpression(0.11, "x");
        doExpression(0.23, "y");
        doExpression(0.0, "dx");
        doExpression(0.0, "dy");
        doExpression(1.0, "dec");
        doExpression(1.0, "br");
        doExpression(1.0, "(+ dx dy br)");
        doExpression(0.0, "e");
        doExpression(0, "(+)");
//FIXME        doExpression(-1, "(- 1)");
        doExpression(1, "(*)");
//FIXME        doExpression(0.5, "(/ 2)");
        doExpression(3, "(+ 3)");
        doExpression(-4, "(* -4)");
        doExpression(7+5, "(+ i f)");
        doExpression(7*5, "(* i f)");
        doExpression(7-5, "(- i f)");
        doExpression(7.0/5.0, "(/ i f)");
        doExpression(93.05, "(+ (* f 0.01) (* 10 i) 23)");
        
        doExpression(2, "(% 8 3)");
        doExpression(2, "(% 8 -3)");
        doExpression(2.1, "(% 8.1 3)", true, 0.00000000001d);
        
        doExpression(210,  "(+ 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)");
        doExpression(-208, "(- 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)");
        doExpression(2432902008176640000.0, "(* 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20)");
        doExpression(5.0/24.0, "(/ 5 4 3 2)");
        doExpression(5, "(* 1 f)");
        
        doExpression(3.0, "(* 3)");
        doExpression(-12, "(* 3 -4)");
        doExpression(60, "(* 3 -4 -5)");
        doExpression(60, "(* 3 (* -4 (* -5)))");
        doExpression(12, "(* 3 (* -4 (* -5)) 0.2)");
        doExpression(93.05, "(+ (* f 0.01) (* 10 i) 23)");

//FIXME        doExpression(-3.0, "(- 3)");
        doExpression(-0.4, "(- 0.6 1)");
        doExpression(-2.4, "(- 0.6 1 2)");
//FIXME        doExpression(-0.3, "(- 0.7 x)", 1, 0, 0, 0);
//FIXME        doExpression(-0.3, "(- y x)", 1, 0.7, 0, 0);
        doExpression(-3.5, "(- 0.5 (/ 12 3))");
        doExpression(-8.5, "(- 0.5 (/ 12 3) 5)");
        doExpression(-7.5, "(- 0.5 (/ 12 3) (- 5 1))");

//FIXME        doExpression(1.0/3.0, "(/ 3)");
        doExpression(0.6, "(/ 0.6 1)");
        doExpression(-0.1, "(/ 0.6 -3 2)", true, 0.00000000001d);
//FIXME        doExpression(0.7, "(/ 0.7 x)", 1, 0, 0, 0);
//FIXME        doExpression(0.7/1.1, "(/ y x)", 1.1, 0.7, 0, 0);
        doExpression(0.125, "(/ 0.5 (/ 12 3))");
        doExpression(0.025, "(/ 0.5 (/ 12 3) 5)");
        doExpression(0.03125, "(/ 0.5 (/ 12 3) (- 5 1))");
        
        doExpression(4.0, "(sqr 2))", true, 0.00000000001d);
        doExpression(4.0, "(sqr -2))", true, 0.00000000001d);
        doExpression(3.0, "(sqrt 9))", true, 0.00000000001d);
        doExpression(3.0, "(sqrt -9))", true, 0.00000000001d);
        doExpression(1.23, "(sqrt (sqr 1.23))", true, 0.00000001d);
        
        doExpression(4.0, "(max 2 4))");
        doExpression(2.0, "(min 2 4))");
//FIXME        doExpression(-2.0, "(* 2 -1)");
//FIXME        doExpression(-2.0, "(min 4 (max -1 (* 2 -1)))");
        doExpression(2.0, "(clamp 1 2 4))");
        doExpression(4.0, "(clamp (sqr 3) 2 4))");
        doExpression(3.0, "(clamp 3 2 4))");
        
        doExpression(0.0, "(step 3 2))");
        doExpression(1.0, "(step -3 -2))");

        doExpression(1.0, "(sin (+ 1.5707963267948966))");
        doExpression(Math.sin(0.5 * CurrentState.frame) * 100 + 400, "(+ 400 (* 100 (sin (* 0.5 f))))");
        doExpression(0.5, "(usin 0)");
        doExpression(0.0, "(usin -1.5707963267948966))");
        doExpression(0.5 + 0.5 * Math.sin(9), "(usin (+ 4 f))");

        doExpression(-1.0, "(cos (* 2 1.5707963267948966))");
        doExpression(1.0, "(ucos 0)");
        doExpression(0.5, "(ucos -1.5707963267948966))");
        doExpression(0.5 + 0.5 * Math.cos(9), "(ucos (+ 4 f))");
        
        doExpression(Math.log(1.0), "(log 1)");
        doExpression(Math.log(Math.abs(-1.0)), "(log -1)");
        doExpression(Math.log(0.0), "(log 0)");
        doExpression(Math.log(1.234), "(log 1.234)");
        doExpression(Math.log(1.234), "(log (* -1 1.234))");
        
        doExpression(1.0, "(round 1)");
        doExpression(1.0, "(round 0.5)");
        doExpression(-1.0, "(round -1.1)");
        doExpression(0.0, "(round -0.5)");
        doExpression(-1.0, "(round -0.500001)");

        doExpression(0.2, "(pos? 0.1 0.2 0.3)");
        doExpression(5.0, "(pos? -0.1 i f)");
        doExpression(5.0, "(pos? (cos (* 2 1.5707963267948966)) 0.4 f)");
        doExpression(1, "(pos? 0 1 2)");
        doExpression(2, "(pos? -0.00000001 1 2)");
        doExpression(-4, "(pos? (pos? -1 2 -3) 1 -4)");
//        doExpression(5, "(pos? (pos? -1 2 -3) 1 (pos? 1 5 -3))");
        doExpression(-0.5, "(- 0 0.5)");
        doExpression(-1, "(pos? (- 0 0.5) 1 -1)");
        doExpression(0.5, "(- 1 0.5)");
        doExpression(1, "(pos? (- 1 0.5) 1 -1)");
        doExpression(5, "(pos? (pos? -1 2 -3) 1 (pos? 1 5 -3))");
        
        doExpression(0.2, "(even? 0.0 0.2 0.3)");
        doExpression(0.3, "(even? 1.0 0.2 0.3)");
        doExpression(0.3, "(even? -1.0 0.2 0.3)");
        doExpression(0.2, "(even? -0.9 0.2 0.3)");

        doExpression(0.2, "(zero? 0 0.2 0.3)");
        doExpression(0.3, "(zero? 0.1 0.2 0.3)");
	}
	
	private void doExpression(double expected, String input) {
		doExpression(expected, input, false, 0.00000000001d);
	}
	
	private static int id_counter = 1;

	private void doExpression(double expected, String input, boolean imprecise, double epsilon) {
	    MsgLisp l1 = new MsgLisp();
		System.out.println(input);
        ast_m code = l1.compile(input);
        assertNotNull(code);
        
        byte[] asm = null;
        try {
        	asm = MsgAsm.generate(code, "0");
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
        assertNotNull(asm);

		MsgClassLoader msgClassLoader = new MsgClassLoader();
		try {
			Class<IFn> c = msgClassLoader.defineClass("jean.l2asm.Fn0", asm);
			IFn fn = c.getDeclaredConstructor().newInstance();
			Cluster dummy_cluster = new Cluster(0, null, Cluster.C_SILENT, "", null);
			Cell dummy_cell = new Cell(0, 0, Cluster.dummy);
			dummy_cell.id = id_counter++;
			dummy_cell.index = 7;
			dummy_cell.frame = 11;
			dummy_cell.pos_x = 0.11;
			dummy_cell.pos_y = 0.23;
			dummy_cluster.cell_list.add(dummy_cell);
			dummy_cell.cluster_index = id_counter++; // patch
			CurrentState.frame = 5;
			var value = fn.expr(dummy_cell, dummy_cluster);
			System.out.println("expr() = " + value);
			if(imprecise)
				assertEquals(expected, value, epsilon);
			else
				assertEquals(expected, value);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	void testRange() {
		doRange(0.0, "(noise x y 0.1)", true, true);
		doRange(0.0, "(noise x y 0)", true, true);
		doRange(0.0, "(snoise y x 0.1)", true, true);
		doRange(0.0, "(unoise y x 0.1)", false, true);
	}

	private void doRange(double expected, String input, boolean signed, boolean imprecise) {
		MsgLisp l1 = new MsgLisp();
		System.out.println(input);
		ast_m code = l1.compile(input);
		assertNotNull(code);

		byte[] asm = null;
		try {
			asm = MsgAsm.generate(code, "0");
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
		assertNotNull(asm);

		MsgClassLoader msgClassLoader = new MsgClassLoader();
		try {
			Class<IFn> c = msgClassLoader.defineClass("jean.l2asm.Fn0", asm);
			IFn fn = c.getDeclaredConstructor().newInstance();
			Cell dummy_cell = new Cell(0, 0, Cluster.dummy);
			dummy_cell.id = 7;
			CurrentState.frame = 1;

			double min_value = Double.MAX_VALUE;
			double max_value = -1.0 * Double.MAX_VALUE;
			for (int ix = -111; ix <= 111; ++ix) {
				for (int iy = -111; iy <= 111; ++iy) {
					dummy_cell.pos_x = 0.51 * ix;
					dummy_cell.pos_y = 0.73 * iy;
					var value = fn.expr(dummy_cell, null);
					if(value < min_value)
						min_value = value;
					if(value > max_value)
						max_value = value;
//					System.out.println(min_value + " " + max_value + " " + value);
					++CurrentState.frame;
				}
			}
			System.out.println(min_value + " " + max_value);
			if(signed) {
				assertTrue(max_value > min_value);
				assertTrue(max_value > 0.0);
				assertTrue(max_value <= 1.0);
				assertTrue(min_value < 0.0);
				assertTrue(min_value >= -1.0);
			}
			else {
				assertTrue(max_value > min_value);
				assertTrue(max_value <= 1.0);
				assertTrue(min_value >= 0.0);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}
