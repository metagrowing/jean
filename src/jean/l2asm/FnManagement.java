package jean.l2asm;

import java.security.InvalidParameterException;

import jean.model.Cell;
import jean.model.Cluster;

public class FnManagement {
	private MsgClassLoader fnClassLoader;
	private Class<IFn> fnClass;
	private IFn fnInstance;
	private boolean undefined = true;
	private double value;
	private final double default_value;
	
	public FnManagement(double default_value) {
		super();
		this.default_value = default_value;
	}

	public void remove_code() {
		fnInstance = null;
		fnClass = null;
		fnClassLoader = null;
		undefined = true;
	}
	
	public void reload_code(Object arg, String identifier) {
		remove_code();
		MsgLisp l1 = new MsgLisp();
		ast_m ast = null;
		if (arg instanceof Float) {
			ast = l1.compile(((Float) arg).toString());
		}
		else if (arg instanceof String) {
			ast = l1.compile((String) arg);
		}
		if(ast != null) {
			try {
				byte[] asm = MsgAsm.generate(ast, identifier);
				insert_code(identifier, asm);
			} catch (InvalidParameterException e) {
				remove_code();
				System.err.println(e.getMessage());
			} catch (Exception e) {
				remove_code();
				e.printStackTrace();
			}
		}
		else {
			System.err.println("Unknown data type '" + arg + "'");
		}
	}

	public void insert_code(String identifier, byte[] asm) throws Exception {
		fnClassLoader = new MsgClassLoader();
		fnClass = fnClassLoader.defineClass("jean.l2asm.Fn" + identifier, asm);
		fnInstance = fnClass.getDeclaredConstructor().newInstance();
		undefined = false;
	}

	public double eval(Cell cell, Cluster cluster) {
		if (undefined)
			return default_value;
		if (fnInstance != null)
			return fnInstance.expr(cell, cluster);
		return value;
	}

	public void setValue(double value) {
		remove_code();
		undefined = false;
		this.value = value;
	}

	public static void parsePair(Object arg, Object name, String key, FnManagement m) {
		if (name.equals(key)) {
			if (arg instanceof Float) {
				System.out.println("f " + name + " " + arg);
				m.setValue((Float)arg);
			} else if (arg instanceof String) {
				System.out.println("s " + name + " " + arg);
				m.reload_code(arg, key);
			}
		}
	}
}
