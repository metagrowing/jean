package jean.OSCin;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import jean.model.CurrentState;

class OscListenerTest {

	private static final double EPSILON = 0.0000001d;
	
	static void zero() {
		CurrentState.u0 = 0;
		CurrentState.u1 = 0;
		CurrentState.u2 = 0;
		CurrentState.u3 = 0;
		CurrentState.u4 = 0;
		CurrentState.u5 = 0;
		CurrentState.u6 = 0;
		CurrentState.u7 = 0;
	}

	@Test
	void testCopy2stateA() {
		zero();
		List<Object> args = new ArrayList<>();
		args.add(0.1f);
		OscListener.copy2state(args, 0);
		assertEquals(0.1, CurrentState.u0, EPSILON);
		assertEquals(0.0, CurrentState.u1, EPSILON);
		assertEquals(0.0, CurrentState.u2, EPSILON);
		assertEquals(0.0, CurrentState.u3, EPSILON);
	}

	@Test
	void testCopy2stateB() {
		zero();
		List<Object> args = new ArrayList<>();
		args.add(0.1f);
		args.add(0.2f);
		OscListener.copy2state(args, 0);
		assertEquals(0.1, CurrentState.u0, EPSILON);
		assertEquals(0.2, CurrentState.u1, EPSILON);
		assertEquals(0.0, CurrentState.u2, EPSILON);
		assertEquals(0.0, CurrentState.u3, EPSILON);
	}

	@Test
	void testCopy2stateC() {
		zero();
		List<Object> args = new ArrayList<>();
		args.add(0.1f);
		args.add(0.2f);
		args.add(0.3f);
		OscListener.copy2state(args, 5);
		assertEquals(0.0, CurrentState.u4, EPSILON);
		assertEquals(0.1, CurrentState.u5, EPSILON);
		assertEquals(0.2, CurrentState.u6, EPSILON);
		assertEquals(0.3, CurrentState.u7, EPSILON);
	}

	@Test
	void testCopy2stateD() {
		zero();
		List<Object> args = new ArrayList<>();
		args.add(0.1f);
		OscListener.copy2state(args, 7);
		assertEquals(0.0, CurrentState.u4, EPSILON);
		assertEquals(0.0, CurrentState.u5, EPSILON);
		assertEquals(0.0, CurrentState.u6, EPSILON);
		assertEquals(0.1, CurrentState.u7, EPSILON);
	}

	@Test
	void testCopy2stateE() {
		zero();
		List<Object> args = new ArrayList<>();
		args.add(0.1f);
		args.add(0.2f);
		OscListener.copy2state(args, 7);
		assertEquals(0.0, CurrentState.u4, EPSILON);
		assertEquals(0.0, CurrentState.u5, EPSILON);
		assertEquals(0.0, CurrentState.u6, EPSILON);
		assertEquals(0.1, CurrentState.u7, EPSILON);
	}
	
	@Test
	void testCopy2stateF() {
		zero();
		List<Object> args = new ArrayList<>();
		args.add(0.1f);
		args.add(0.2f);
		args.add(0.3f);
		args.add(0.4f);
		args.add(0.5f);
		args.add(0.6f);
		args.add(0.7f);
		args.add(0.8f);
		OscListener.copy2state(args, 0);
		assertEquals(0.1, CurrentState.u0, EPSILON);
		assertEquals(0.2, CurrentState.u1, EPSILON);
		assertEquals(0.3, CurrentState.u2, EPSILON);
		assertEquals(0.4, CurrentState.u3, EPSILON);
		assertEquals(0.5, CurrentState.u4, EPSILON);
		assertEquals(0.6, CurrentState.u5, EPSILON);
		assertEquals(0.7, CurrentState.u6, EPSILON);
		assertEquals(0.8, CurrentState.u7, EPSILON);
	}

}
