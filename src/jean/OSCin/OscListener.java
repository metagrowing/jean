package jean.OSCin;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import com.illposed.osc.MessageSelector;
import com.illposed.osc.OSCMessage;
import com.illposed.osc.OSCMessageEvent;
import com.illposed.osc.OSCMessageListener;
import com.illposed.osc.messageselector.OSCPatternAddressMessageSelector;
import com.illposed.osc.transport.OSCPortIn;

import jean.application.Jean;
import jean.model.Cluster;
import jean.model.CurrentState;

public class OscListener {
	public final static boolean DEBUG_OSC_CMD = true;
	private OSCPortIn receiver;
	private final int JEAN_PORT = 57320;
	private static long msg_count = 0;

	public void setup() {
		try {
			receiver = new OSCPortIn(JEAN_PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}

		OSCMessageListener jean_pipeline_listener = new OSCMessageListener() {
			@Override
			public void acceptMessage(OSCMessageEvent event) {
				try {
					Jean.habiat.culster_list_lock.lock();
					try {
						OSCMessage msg = getMsg(event);
						var args = msg.getArguments();
						if(DEBUG_OSC_CMD) System.out.println(args);
						Jean.habiat.build_pipeline(args);
					} finally {
						Jean.habiat.culster_list_lock.unlock();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		OSCMessageListener jean_command_listener = new OSCMessageListener() {
			@Override
			public void acceptMessage(OSCMessageEvent event) {
				try {
					Jean.habiat.culster_list_lock.lock();
					try {
						OSCMessage msg = getMsg(event);
						var args = msg.getArguments();
						if(DEBUG_OSC_CMD) System.out.println(args);
						int alen = args.size();
						for (int ax = 2; ax < alen; ++ax) {
							Object arg = args.get(ax);
							int cmd = 0;
							if (arg instanceof Float) {
								cmd = (int) (float) arg;
							} else if (arg instanceof String) {
								try {
									cmd = Integer.parseInt((String) arg);
								} catch (NumberFormatException e) {
									System.err.println(arg + "\n" + e.getMessage());
								}
							}
							switch (cmd) {
							case -1:
								CurrentState.need_full_clear = true;
								break;
							case -2:
								CurrentState.frame = 0;
								Jean.habiat.reset();
								break;
							case -3:
								Jean.binding.setRecording(true);
								break;
							case -4:
								Jean.binding.setRecording(false);
								break;
							}
						}
					} finally {
						Jean.habiat.culster_list_lock.unlock();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		};

		OSCMessageListener jean_layer_listener = new OSCMessageListener() {
			@Override
			public void acceptMessage(OSCMessageEvent event) {
				OSCMessage msg = getMsg(event);
				List<Object> args = msg.getArguments();
				if (args.size() < 2 || !(args.get(0) instanceof String))
					return;

				String name = (String) args.get(0);
				if(DEBUG_OSC_CMD) System.out.println(args);
				Jean.habiat.culster_list_lock.lock();
				try {
					for (Iterator<Cluster> cluster_iter = Jean.habiat.culster_pipeline.iterator(); cluster_iter
							.hasNext();) {
						Cluster cluster = cluster_iter.next();
						if (cluster.match(name)) {
							cluster.reload(args);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					Jean.habiat.culster_list_lock.unlock();
				}
			}
		};

		OSCMessageListener jean_texpr_listener = new OSCMessageListener() {
			@Override
			public void acceptMessage(OSCMessageEvent event) {
				OSCMessage msg = event.getMessage();
				++msg_count;
//				System.out.println("Message " + msg_count + " received: " + msg.getAddress());
				List<Object> args = msg.getArguments();
				if(DEBUG_OSC_CMD) System.out.println(args);
				Jean.habiat.culster_list_lock.lock();
				try {
					final int stop = args.size() - 1;
					// TODO too many numArgs
					for (int ax = 2; ax < stop; ax += 2) {
						Object name = args.get(ax);
						Object arg = args.get(ax + 1);
						if (name.equals("t0")) {
							CurrentState.mng_t0.reload_code(arg, "t0");
						} else if (name.equals("t1")) {
							CurrentState.mng_t1.reload_code(arg, "t1");
						} else if (name.equals("t2")) {
							CurrentState.mng_t2.reload_code(arg, "t2");
						} else if (name.equals("t3")) {
							CurrentState.mng_t3.reload_code(arg, "t3");
						} else if (name.equals("t4")) {
							CurrentState.mng_t4.reload_code(arg, "t4");
						} else if (name.equals("t5")) {
							CurrentState.mng_t5.reload_code(arg, "t5");
						} else if (name.equals("t6")) {
							CurrentState.mng_t6.reload_code(arg, "t6");
						} else if (name.equals("t7")) {
							CurrentState.mng_t7.reload_code(arg, "t7");
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					Jean.habiat.culster_list_lock.unlock();
				}
			}
		};

		OSCMessageListener ana_u_listener = new OSCMessageListener() {
			@Override
			public void acceptMessage(OSCMessageEvent event) {
				OSCMessage msg = event.getMessage();
				++msg_count;
//				System.out.println("Message " + msg_count + " received: " + msg.getAddress());
				String path = msg.getAddress();
				List<Object> args = msg.getArguments();
				if (path.equals("/ana_u0")) {
					copy2state(args, 0);
				} else if (path.equals("/ana_u1")) {
					copy2state(args, 1);
				} else if (path.equals("/ana_u2")) {
					copy2state(args, 2);
				} else if (path.equals("/ana_u3")) {
					copy2state(args, 3);
				} else if (path.equals("/ana_u4")) {
					copy2state(args, 4);
				} else if (path.equals("/ana_u5")) {
					copy2state(args, 5);
				} else if (path.equals("/ana_u6")) {
					copy2state(args, 6);
				} else if (path.equals("/ana_u7")) {
					copy2state(args, 7);
				} else if (path.equals("/ana_uniform")) {
					copy2state(args, 0);
				}
			}
		};

		final var dispatcher = receiver.getDispatcher();
		dispatcher.addListener(new OSCPatternAddressMessageSelector("/jean/pipe"),  jean_pipeline_listener);
		dispatcher.addListener(new OSCPatternAddressMessageSelector("/jean/layer"), jean_layer_listener);
		dispatcher.addListener(new OSCPatternAddressMessageSelector("/jean/texpr"), jean_texpr_listener);
		dispatcher.addListener(new OSCPatternAddressMessageSelector("/jean/cmd*"),  jean_command_listener);

		MessageSelector ana_u_selector = new OSCPatternAddressMessageSelector("/ana_u*");
		dispatcher.addListener(ana_u_selector, ana_u_listener);
		// NOTE You might want to use this code, in case you have bundles
		// with time-stamps in the future, which you still want
		// to process immediately.
		// dispatcher.setAlwaysDispatchingImmediately(true);
		receiver.startListening();
	}

	private OSCMessage getMsg(OSCMessageEvent event) {
		OSCMessage msg = event.getMessage();
		++msg_count;
		System.out.println("Message " + msg_count + " received: " + msg.getAddress());
		return msg;
	}

	public static void copy2state(List<Object> args, int start) {
//		int numArgs = Math.min(args.size(), 8-start);
		int numArgs = args.size();
//		int toomany = numArgs+start-8;
//		if(toomany > 0) {
//			numArgs -= toomany;
//		}
		// TODO too many numArgs
		while (numArgs > 0 && numArgs + start > 8) {
			--numArgs;
		}
		if (numArgs > 0 && numArgs + start <= 8) {
			int ax = 0;
			for (int sx = start; sx < numArgs + start; ++sx) {
				switch (sx) {
				case 0:
					CurrentState.u0 = (float) args.get(ax);
					break;
				case 1:
					CurrentState.u1 = (float) args.get(ax);
					break;
				case 2:
					CurrentState.u2 = (float) args.get(ax);
					break;
				case 3:
					CurrentState.u3 = (float) args.get(ax);
					break;
				case 4:
					CurrentState.u4 = (float) args.get(ax);
					break;
				case 5:
					CurrentState.u5 = (float) args.get(ax);
					break;
				case 6:
					CurrentState.u6 = (float) args.get(ax);
					break;
				case 7:
					CurrentState.u7 = (float) args.get(ax);
					break;
				}
				++ax;
			}
		}
	}
}
