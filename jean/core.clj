(ns jean.core)
(require '[clojure.walk :as walk])
(use 'overtone.osc)

(def OSC-WAIT 1)
(def JEAN_PORT 57320)
; (def to-analog (osc-client "192.168.2.2" JEAN_PORT))
(def to-analog (osc-client "localhost" JEAN_PORT))

(def cmd_keywords {
  :clear    -1
  :reset    -2
  :record   -3
  :stop     -4
  })

(def layer_keywords {
  :src       0
  :mix       1
  :max       2
  :min       3
  :mul       4
  :add       5
  :diff      6
  :screen    7
  :overlay   8
  :inv       9
  :red      10
  :gray     11
  :inv_dest 12

  :rotate   13
  :pixelate 14
  :repeat   15
  :noise    16
  :blur     17

  :oval      0
  :soval     1
  :rect      2
  :srect     3
  :tri       4
  :stri      5
  :word      6
  })

(defn layer_key_replace [token]
  (if (and (keyword? token) (not (contains? layer_keywords token)))
    (println "warning: did not understand" token "in this context."))
  (if (and (keyword? token) (contains? layer_keywords token))
    (layer_keywords token)
    token)
)

(defn cmd_key_replace [token]
  (if (and (keyword? token) (not (contains? cmd_keywords token)))
    (println "warning: did not understand" token "in this context."))
  (if (and (keyword? token) (contains? cmd_keywords token))
    (cmd_keywords token)
    token)
)

(defn repalce_layer_keywords [line]
  ; (println "orig " line)
  ; (println "repl " (walk/prewalk kreplace line))
  (str (walk/prewalk layer_key_replace line))
)

(defn repalce_cmd_keywords [line]
  ; (println "orig " line)
  ; (println "repl " (walk/prewalk kreplace line))
  (str (walk/prewalk cmd_key_replace line))
)

(defn jean_type [token]
  (if (number? token) "f" "s")
)

(defn jean_flat_unaltered [message embedded_count token]
  [(conj message
    (str token))
    (jean_type token)]
)

(defn jean_flat [message embedded_count token]
  [(conj message
              (if (number? token)
                  token
                  (repalce_cmd_keywords token)))
    (jean_type token)]
)

(defn jean_split [message embedded_count nodes]
  (if (and (> embedded_count 0) (not (= (count nodes) 2)))
    (println "warning: expexted 2 elements but got " (count nodes) "in " nodes))
  ; (println "(first nodes)" (first nodes))
  (if (> embedded_count 0)
    [(conj message
                (str (first nodes))
                (if (number? (second nodes))
                    (second nodes)
                    (repalce_layer_keywords (second nodes))))
     (str "s" (jean_type (second nodes)))]
    [(conj message (str nodes))
     "s"]
  )
)

(defn unroll [id tnode spliter]
  (loop [lines [(str id)]
         types "s"
         nodes tnode
         count 0]
    (if (seq nodes)
      (let [s (spliter lines count (first nodes))]
        ; (println (first s))
        (recur (first s)
               (str types (second s))
               (rest nodes)
               (inc count)))
      [types lines]
    )
  )
)

(defn jean_send [id tnode path_ends spliter]
  (let [fnode (if (vector? (first tnode))
                   tnode
                   (conj tnode "[]"))]
     (let [msg (unroll id fnode spliter)]
       ; (println "osc " (first msg) (second msg))
       (osc-send-msg to-analog {:path (str "/jean/" path_ends)
                                :type-tag (first msg)
                                :args (second msg)})
                                ))
  (Thread/sleep OSC-WAIT)
)

(defmacro layer [id & tnode] (jean_send id tnode "layer" jean_split))
(defmacro pipe [& tnode]     (jean_send "" tnode "pipe"  jean_flat_unaltered))
(defmacro ! [& tnode]        (jean_send "" tnode "cmd"   jean_flat))
(defmacro texpr [& tnode]    (jean_send "" tnode "texpr" jean_split))
(defmacro fndef [id & tnode] (jean_send id tnode "fndef" jean_flat))
