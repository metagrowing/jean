(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :cursor0 :cursor1)

(texpr
  (t0 (+ 1.57 (* i1 3.14)))
  (t1 (sin 1.57))
)
(layer :silent0 []
  (img "frozen/frozen0.png")
  (lb :mul)
  (la 0.05)
)
(layer :cursor0 []
  (n 3)
  (lb :screen)

  (x (* 0.213 i1 (cos (* 0.00113 (* 3 i1) f))))
  (y (* 0.171 i1 (sin (* 0.00073 (* 3 i1) f))))
  (w 0.10)
  (h 0.10)

  (H 0)
  (S 0.5)
  (V 1)
  (A 0.1)
)
(layer :cursor1 []
  (n 6)
  (lb :screen)

  (x (+ x_ (* 0.113 i1 (cos (* 0.00117 (* 3 i1) f)))))
  (y (+ y_ (* 0.071 i1 (sin (* 0.00091 (* 3 i1) f)))))
  (w 0.050)
  (h 0.050)

  (H 0.1)
  (S 0.5)
  (V 1)
  (A 0.5)
)
