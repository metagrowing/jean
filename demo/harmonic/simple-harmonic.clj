(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :silent1 :cursor0 :calve0)

(layer :silent0
  (img "morse/modern_telegraphy_inv.png")
  (la 0.1)
  (lb :mix)
  ; (lb :rotate)
  ; (lrot 0.01)
)

(layer :silent1
  (img "flower-pages/page_301-center.png")
  (la 0.1)
  (lb :mix)
)

(layer :cursor0 []
  (n (+ 1 (* 5 (usin (* 0.005 f)))))
  ; (n 1)
  (la 0.5)
  (lb :diff)

  (x (* 0.800 (cos (* 0.0111 (+ f (* 30 i))))))
  (y (* 0.400  (sin (* 0.0613 f))))
  (w 0.015)
  (h 0.015)

  (H 0.1)
  (S 0.8)
  (V 1)
  (A 0.8)
)

(layer :calve0 []
  (hit :wxh)
  (shape :rect)

  (n 17)
  (la 1)
  (lb :diff)

  (x (- (* 0.09 i) 0.8))
  (y 0)
  (h (* 0.200 (+ 1 (noise x (* 0.01 f) 0.2))))
  (w 0.03)

  (H (+ 0.55 (* 0.01 i)))
  (S 0.5)
  (V 1)
  (A 0.2)
)
