(
s.waitForBoot({
	SynthDef(\wxh,
		{ |cax=0, cay=0, caw=0, cah=0, cux=0, cuy=0, gate = 1, rq = 0.3, out = 0, amp = 0.05|
			// see: mrufrufin / dxkSynthDefs / GNU GENERAL PUBLIC LICENSE Version 3
			// abstracted out from 08091500Acid309 by_otophilia
			// changed env2 to multiplicative instead of additive
			var env1, env2, output;
			var area = caw * cah;
			var max_area = 0.03 * 0.4;
			var area_diff = max(max_area - area, 0);
			var freq = area.linexp(0, area_diff, 400, 90);
			var pan = LinLin.ar(cax, -1, 1, -1, 1);
			freq = Lag.kr(freq, 0.12 * (1-Trig.kr(gate, 0.001)) * gate);
			env1 = EnvGen.ar(Env.new([0, 1.0, 0, 0], [0.001, 2.0, 0.04], [0, -4, -4], 2), gate, amp);
			env2 = EnvGen.ar(Env.adsr(0.001, 0.8, 1, 0.8, 1.4, -4), gate);
			output = LFPulse.ar(freq, 0.0, 0.51, 2, -1);

			output = RLPF.ar(output, freq * env2, rq);
			output = output * env1;
			output = Pan2.ar(output, pan);

			DetectSilence.ar(output, doneAction: Done.freeSelf);
			Out.ar(out, output);
	}).add;


	s.meter(0, 2);
	s.freqscope();
});
)
