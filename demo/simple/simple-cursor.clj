(use 'jean.core)

(pipe :silent0 :cursor1)

(layer :silent0 []
  (img "flower-pages/page_301-center.jpg")
  (lb :diff)
)

(layer :cursor1 []
  (n 5)
  (lb :add)

  (x (* 0.5 (cos (* 0.0113 (+ i 1) f))))
  (y (sin (* 0.0121 (+ i 1) f)))
  (w 0.10)
  (h 0.10)

  (H (+ 0.05 (* i 0.030)))
  (S 0.5)
  (V 0.5)
  (A 0.4)
)
