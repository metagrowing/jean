
recorded by kwahmah_02
November 12th, 2014

Ham radio morse code broadcasts on shortwave radio. The file name tells you the band I recorded it in. Picked up and recorded with Twente university's online radio receiver.

https://freesound.org/people/kwahmah_02/sounds/254521/

dkozinn:
Just to let you know, this isn't morse code. It's RTTY (Radio Teletype), which is a digital form of communications that dates back to the use of mechanical teletypes but today is almost exclusively done completely digitially.
