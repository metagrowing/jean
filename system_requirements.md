# System requirements

## List of hardware and software used during development of Jean.

### Software
#### Operating System
- Operating System: 64-Bit Debian GNU/Linux 12

#### Visualization
- java.version 17.0.11
- java byte code generator asm-9.2
- LWJGL 3.3.3+5
- OpenGL 4.6
- running on the Desktop PC
-- NVIDIA Driver Version: 525.147.05
-- X11
- running on the Laptop
-- Wayland

#### Real time audio synthesis
- SuperCollider 3.13.0

#### Live coding
- Clojure 1.11.1
- Atom 1.58.0
- required Atom packages: proto-repl 1.4.24 (proto-repl can use ink 0.12.2. Do not update ink.)
- optional Atom packages: ink 0.12.2

### Hardware
#### Hardware Desktop PC
- Processors: 16 × Intel® Core™ i9-9900K CPU @ 3.60GHz
- Memory: 32 GiB of RAM
- 3D Accelerator: GeForce RTX 2080 Ti

#### Hardware Laptop
- Processors: 8 × Intel® Core™ i7-8650U CPU @ 1.90GHz
- Memory: 7.2 GiB of RAM
- Graphics Processor: Mesa Intel® UHD Graphics 620
