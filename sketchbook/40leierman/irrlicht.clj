(ns jean.demo)
(use 'jean.core)

; (pipe :silent0 :silent3 :silent4 :silent1 :cursor0 :silent2)
(pipe :silent0 :silent3 :silent1 :cursor0 :cursor1 :silent2)

(texpr
  (t0 (* 1.1 (usin (* 0.01 f))))
  (t1 0.2) ; 0.05 0.3
)

(layer :silent3
  (lb :noise)
  (xscale 0.1)
  (yscale 0.1)
  (xamount 5)
  (yamount 5)
)

; (layer :silent4
;   (lb :rotate)
;   ; (lsy (/ (sin (* 0.0123 f)) 0.50))
;   (lrot 0.0)
; )

(layer :cursor0 []
  (shape :tri)
  (rot (+ 1.57 (* i1 3.14)))
  (stream 40)

  (n 2)
  ; (lb :max)
  ; (lb :inv)
  (lb :overlay)

  (x (+ -0.7 i))
  (y (- (round (unoise 0.1 0.1 (+ i (* i1 0.00313 f))) ) 0.5))
  (w 0.050)
  (h 0.100)

  (H 0)
  (S 0.5)
  (V 1)
  (A 0.3)
)

(layer :silent0 []
  (img "leiermann/P1090582-1024x1024.png")
  ; (lb :add)
  (lb :overlay)
  (la 0.07)
)

(layer :silent1 []
  (img "leiermann/wilhelm-mueller_1024x1024.png")
  (lb (pos? (sin (* 0.0013 f)) :mix :overlay))
  (la 0.05)
)

(layer :silent2 []
  (img "leiermann/franz-schubert_1024x1024.png")
  (lb (pos? (sin (* 0.0011 f)) :mix :inv))
  (la 0.05)
)

(layer :cursor1 []
  (n 13)
  (la 0.7)
  (lb :overlay)

  (x (* t0 (cos (* 0.03 i1 i1 f))))
  (y (* t0 (sin (* 0.03 i1 i1 f))))
  (w t1)
  (h t1)

  (H (+ 0.45 (* i 0.030)))
  (S 0.2)
  (V (+ 0.7 (* 0.2 (sin (* 0.1 i1 f)))))
  (A 0.5)
)
