(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :silent1 :lorenz0 :calve0)

(layer :silent0 []
  (img "flower-pages/page_301-center.png")
  (lb :mix)
  (la 0.01)
)

(layer :silent1 []
  (img "lorenz/LGP-30-1280.png")
  (lb :mix)
  (la 0.02)
)

(layer :lorenz0 []
  (lb :mul)

  (n 51)
  (pa 10)
  (pb 28)
  (pc (/ 8 3))
  (sx 0.08)
  (sy 0.03)

  (w 0.04)
  (h 0.04)

  ; (H (/ 19 128))
  (H (usin (* 0.01 f)))
  ; (H ks0)
  (S 0.5)
  ; (S ks1)
  (V 0.7)
  ; (V ks2)
  (A 0.2)
)

(layer :calve0 []
  (hit :dxkAcid)
  (shape :rect)

  (n 2)
  (lb :mix)

  (x (+ (sin (* 0.01 f)) -0.280 (* 0.560 i)))
  (y (* 0.50 (+ 1 i) (sin (* 0.0113 (* (+ 1 i) f)))))
  (w 0.050)
  (h 0.200)

  ; (H (/ 53 128))
  (H (usin (* 0.01 f)))
  (S 0.2)
  (V 1)
  (A 0.5)
)
