(ns jean.demo)
(use 'jean.core)

(pipe :cursor0 :silent1)
; (! :reset)
; (! :reset :clear)
(texpr
  (t0 (* 0.8 (usin (* 0.01 f))))
  (t1 0.03) ; 0.03 0.1
)

(layer :cursor0 []
  (stream 30)
  (n 13)
  (la 0.7)
  (lb :max)

  (x (* t0 (cos (* 0.03 i1 i1 f))))
  (y (* t0 (sin (* 0.03 i1 i1 f))))
  (w t1)
  (h t1)

  (H (+ 0.45 (* i 0.030)))
  (S 0.8)
  (V (+ 0.7 (* 0.2 (sin (* 0.1 i1 f)))))
  (A 0.5)
)

(layer :silent1
  (img "frozen/frozen4.png") ; 4 1 2
  (la 0.1)
  (lb :mul)
  ; (lb :overlay)
)
