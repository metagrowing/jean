(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :cursor0 :silent1 :calve1)

(layer :silent0
  (img "morse/diagram_11_12_inv.png")
  (la 0.015)
  (lb :mul)
)

(layer :cursor0 []
  (stream 1)
  (n 2)
  (la 0.3)
  (lb :screen)

  (x (* 0.9 (cos (* 0.0031 (+ 1 i) f))))
  (y (* (even? i 0.3 0.7) (/ (sin (* 0.037 (+ 1 i) f)))))
  (w 0.03)
  (h 0.20)

  (H (+ -0.01 (* 0.03 (+ 1 i))))
  (S 0.8)
  (V (+ 0.5 (* 0.4 (sin (* 0.1 (+ 1 i) f)))))
  (A 0.3)
)

(layer :silent1
  (lb :rotate)
  (lsy (/ (sin (* 0.0123 f)) 0.50))
  (lrot 0.05)
)

(layer :calve1 []
  (hit :dxkAcid)
  (n 3)
  (shape 1)
  (la 0.5)
  (lb :add)

  (x 0)
  (y (* 0.08 (round (* 10 (cos (* 0.011 (+ 1 i) f))))))
  (w (* 2 (noise x (* f 0.001 y) 0.1)))
  (h 0.015)

  (H 0.5)
  (S 0.8)
  (V (+ 0.5 (* 0.4 (sin (* 0.1 (+ 1 i) f)))))
  (A 0.3)
)
