s.waitForBoot({
    ~fadeout = 0;

	SynthDef(\dxkAcid,
		{ |cax=0, cay=0, caw=0.01, cah=0.01, cux=0, cuy=0, gate = 1, rq = 0.3, amp = 0.05|
			// see: mrufrufin / dxkSynthDefs / GNU GENERAL PUBLIC LICENSE Version 3
			// abstracted out from 08091500Acid309 by_otophilia
			// changed env2 to multiplicative instead of additive
			var env1, env2, output;
			var freq = 100 + (500 * caw * cah);
			var pan = cax.linlin(-1, 1, -1, 1);
			freq = Lag.kr(freq, 0.12 * (1-Trig.kr(gate, 0.001)) * gate);
			env1 = EnvGen.ar(Env.new([0, 1.0, 0, 0], [0.001, 2.0, 0.04], [0, -4, -4], 2), gate, amp);
			env2 = EnvGen.ar(Env.adsr(0.001, 0.8, 1, 0.8, 1.4, -4), gate);
			output = LFPulse.ar(freq, 0.0, 0.51, 2, -1);

			output = RLPF.ar(output, freq * env2, rq);
			output = output * env1;
			output = Pan2.ar(output, pan);

			DetectSilence.ar(output, doneAction: Done.freeSelf);
			Out.ar(0, output);
	}).add;

	SynthDef(\rsnz, { |out = 0, freq = 90, amp = 0.3, xpos = 0, fadeout = 0.0|
		var sig = Resonz.ar(BrownNoise.ar(0.5), freq, 0.1);
		Out.ar(out, 0.5 * fadeout * amp * Pan2.ar(sig, xpos));
	}).add;
	1.wait;

	~a0 = Synth(\rsnz);
	~a1 = Synth(\rsnz);
	["lang port", NetAddr.langPort].postln;
	h.free;
	h = OSCFunc({ |msg, time, addr, recvPort|
		//  1 stream id
		//  2 characteristic
		//  3 cluster or layer index
		//  4 cell id
		//  5 cell x
		//  6 cell y
		//  7 cell delta x
		//  8 cell delta y
		//  9 cell w
		// 10 cell h
		// 11 cell color hue
		// 12 cell color saturation
		// 13 cell color value
		// 14 cell color alpha
		// msg.postln;
		if(msg[2] == 2,  // cursor cluster
			{
				if(msg[4] == 0, // cell id
					{
                        ~fadeout = 1.0;
						// ["msg[4]==0", msg[5], msg[13], msg[6]].postln;
						~a0.set(\xpos, msg[5]);
						~a0.set(\amp, msg[13].linlin(0, 1, 0, 1));
						~a0.set(\freq, msg[6].linexp(-1, 1, 10, 10*10));
                        ~a0.set(\fadeout, ~fadeout);
					},
					{});
				if(msg[4] == 1, // cell id
					{
						// ["msg[4]==1", msg[5], msg[13], msg[6]].postln;
                        ~fadeout = 1.0;
						~a1.set(\xpos, msg[5]);
						// msg[5].linexp(-1, 1, 120, 20*120).postln;
						~a1.set(\amp, msg[13].linlin(0, 1, 0, 1));
						~a1.set(\freq, msg[6].linexp(-1, 1, 30, 10*30));
                        ~a1.set(\fadeout, ~fadeout);
					},
					{});
			},
			{});
	}, '/j_stream');

	// fade out if there are no more osc messages
	t.free;
	t = Task({
		loop {
			~fadeout = ~fadeout * 0.9;
            ~a0.set(\fadeout, ~fadeout);
            ~a1.set(\fadeout, ~fadeout);
			(1.0/25.0).wait;
		}
	}).play;

	s.meter(0, 2);
});
