(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :cursor0)

(layer :silent0 []
  ; (img "morse/diagram_1_inv.png")
  (img "bend/bend-20240430-210218-1024x1024.png")
  (lb :mult)
  (la 0.01)
)

(layer :cursor0 []
  (n 1)
  (shape :tri)
  (lb :add)
  ; (la 0.01)

  (x (+ 1 (* 2 (- u0 4))))
  (y (* 2 (- u1 0.5)))
  ; (x 0)
  ; (y 0)
  (w 0.02)
  (h 0.02)

  (H 0.5)
  (S 1)
  (V 1)
)
