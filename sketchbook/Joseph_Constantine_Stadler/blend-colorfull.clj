(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :silent1 :cursor0 :calve1)
; (! :reset :clear :record)
; (! :stop)
(layer :silent0
  (img "flower-pages/page_301-center.png")
  (lb :min)
)

(layer :silent1
  (img "flower-pages/page_241-center.png")
  (lb :min)
  (la 0.3)
)

(layer :cursor0 []
  (lb :src)

  (n 7)
  (br 0.2)
  (dec 0.0001)

  (x (* 0.100 i1 (cos (* 0.00111 (* (+ 3 i) f)))))
  (y (* 0.060 i1 (sin (* 0.00113 (* (+ 3 i) f)))))
  (w 0.05)
  (h 0.05)

  (H (+ (/ 13 360) (* 0.005 i)))
  (S 1)
  (V 0.7)
  (A 0.3)
)

(layer :calve1 []
  (lb :mix)
  (shape :rect)
  (n 5)

  (x (* 0.2 i1 (cos (* -0.00117 (* i1 f)))))
  (y (* 0.1 i1 (sin (* -0.00119 (* i1 f)))))
  (h (* 0.01 i1))
  (w 0.350)
  (rot (* -0.01 f))

  (H (+ (/ 173 360) (* 0.02 i)))
  (S 0.9)
  (V (mix 0.2 1 e))
  (A 0.3)
)
