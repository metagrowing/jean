(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :cursor0 :cursor1)

(layer :silent0 []
  (img "iss/scale_1024x8373.png")
  (lb :min)
  ; (la 0.05)
  (la 1)
  (lty (* 0.9 (usin (* 0.001 f))))
)

(layer :cursor0 []
  (stream 20)
  (shape :rect)
  (lb :max)
  (n 2)

  (x (* 1.4 (noise 0.1 0.2 (* i1 0.0113 f))))
  (y 0.3)
  (w 0.01)
  (h 0.2)

  (H 0.5)  (S 1)  (V 1)  (A 0.5)
)

(layer :cursor1 []
  (stream 20)
  (shape :rect)
  (lb :max)
  (n 2)

  (x (* 1.4 (noise 0.5 0.6 (* i1 0.0091 f))))
  (y -0.3)
  (w 0.01)
  (h 0.2)

  (H 0.1)  (S 1)  (V 1)  (A 0.5)
)
