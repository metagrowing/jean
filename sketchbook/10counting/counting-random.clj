(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :calve0 :silent1 :cursor0)

(layer :silent0 []
  (img "zuse/3.jpg")
  (lb :src)
)

(layer :calve0 []
  (hit  :playfwd)
  (exit :playrev)

  (lb :max)
  (n 1)
  (shape :rect)

  (x 0)
  (y 0)
  (w 0.2)
  (h 0.6)

  (H 0)
  (S 1)
  (V 1)
  (A (+ 0.2 (* e 0.6)))
)

(layer :cursor0 []
  (lb :screen)
  (n 25)
  (dec 0.01)
  (br 0.05)

  (x (* 0.8 (cos (+ i (* 0.01 f)))))
  (y (- (* i 0.05) 0.5))
  (w 0.05)
  (h 0.05)

  (H 0.1)
  (S 1)
  (V 1)
  (A 0.8)
)

(layer :silent1 []
  (lb :noise)
  (xscale 5)
  (yscale 5)
  (xamount 0)
  (yamount 2)
)
