(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :cursor0 :silent1)
(! :reset :clear)

(layer :silent0 []
  (img "morse/diagram_1_inv.png")
  (lb :red)
  (la 0.01)
)

(layer :silent1 []
  (img "frozen/frozen4.png")
  (lb :gray)
  (la 0.01)
)

(layer :cursor0 []
  (fresh :fresh)
  (lb :inv_dest)
  (n 10)
  (dec 0.01)
  (br 0.05)

  (x (- (* 0.18 i) 0.8))
  (y 0.25)
  (w 0.05)
  (h 0.05)

  (H 0)
  (S 1)
  (V 1)
)
