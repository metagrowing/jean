(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :cursor0)

(layer :silent0 []
  ; (img "morse/diagram_1_inv.png")
  ; (img "lorenz/LGP-30-1280.png")
  ; (img "light/P1090560.png")
  ; (img "bend/bend-20240430-210218-1024x1024.png")
  (img "bend/bend-20240430-210218.png")
  (lb :mult)
  (la 0.02)
)

(layer :cursor0 []
  (n 1)
  (shape :tri)
  (lb :diff)
  ; (la 0.01)

  (x u1)
  (y u2)
  (w u0)
  (h u0)

  (H 0.5)
  (S 1)
  (V 1)
)
