# Jean a live coding environment
Simple geometric shapes can be animated on a layer. Individual layers can also contain just a single image. The shapes and images are stacked in layers similar to an image editing program. Complex color patterns are created by blending the layers.

The geometric shapes are animated by mathematical formulas. Their colors are also defined algorithmically in the HSV color space. All these expressions can be changed at any time while the program is running. This is the live coding aspect of Jean.

Jean can initiate sounds synchronously to the movement. To do this, Jean continuously sends OSC packets containing the current position and velocity of the shapes. There is also a simple distance check between the shapes belonging to an animation. If *cursor* shapes overlap with *calve* shapes, an additional OSC packet is sent.

SuperCollider is the receiver of this OSC messages. This synthesizer can be used to generate sound in real-time synchronously to the animation.

[[_TOC_]]


## Motivation
It where Jean Tinguely's machines that inspired me. Their parts move on simple curves, sometimes uniformly, sometimes more complex and sometimes with heavy jitter. A reminiscent of construction machinery. And some of them had lots of flashing light bulbs. But not to be ignored: They had to make loud noise.

### Technical Background
1. For live coding, I wanted to use a programming environment that allows that the source code can be changed several times on the fly. Without loosing graphical data or internal states of the current run. And without the need to restart the animation when modifying the live coding part.

2. I wanted to use a domain specific language (DSL) for this type of animation. In other words, a programming language that covers exactly the scope of moving simple 2D shapes around the screen.

3. I need a high-end algorithmic synthesizer.

Points 1 and 2 can be achieved by using Lisp. More precisely, with a combination of Clojure and a Java byte code generator. This is covering the visual part of the audio-visual animation.

For sound generation I choose SuperCollider. SuperCollider also supports live coding. So I can switch back and forth between graphic animation and sound generation during a performance.

## Limitations
* 2D only
* The dimension of the canvas is always 1024x1024 pixels.
* A great deal of installation work is required to get the system up and running.
* Actually my Jean system is unstable.
* Some of the components used are no longer being supported by the original authors. In fact, I would have to reduce the number of components to get a maintainable and stable system.
* Most topics you need to install and use Jean are undocumented yet.

## Structure of a live coding program
A program consists of several top level expressions. Almost all of these top level expressions can be edited several times at runtime and sent individually or as a whole to the visualization. The top level expressions are ordered sequentially in the editor, but they can be sent in any order. Only the `(use 'jean.core)` and the `(pipe ...)` expression must be sent at first.

### Graphics pipeline
The `(pipe ...)` top level expression describes the data flow through the layers. Data flow is more like a feedback loop, not a simple stack as used in image manipulation programs. The graphic data of the last layer in the pipe expression is send to the first layer in the next round.

The upstream layer is combined with current layer. There is a blend mode and an alpha parameter available.
```clojure
(layer :silent1
  ;; display a background image
  (img "flower-pages/page_301-center.png")
  ;; layer alpha value
  (la 0.1)
  ;; mode how to blend this layer with the upstream layer
  (lb :mix)
)
```

| layer blend mode | description |
| --- | --- |
| :src  | source over<br>ignores the upstream |
| :mix | blend RGB values |
| :max  | from each R, G or B channel select the maximum |
| :min  | from each R, G or B channel select the minimum |
| :mul | multiply the channels |
| :add | add the channels |
| :diff | difference between the channels |
| :screen | Screen blend mode. see:<br>https://en.wikipedia.org/wiki/Blend_modes |
| :overlay | Overlay blend mode. see:<br>https://en.wikipedia.org/wiki/Blend_modes |
| :inv | invert source |
| :red | set the G and B channels to zero,<br>but preserve the  R channel|
| :gray | convert to gray scale |
| :inv_dest | invert the upstream image |

### Layers
Layers have unconventional names. As keywords in Clojure layer names are starting with an `:`. The first part (`:silent`, `:cursor`, `:calve`) describes the characteristic of the layer. This is followed by a numeric value to distinguish between different layers of the same characteristic.

`:silent...` describes a layer that is not to be used for sound generation. It is used, for example, to load a background image.

`:cursor...` Here, the movement and the colors are defined for a group of cells.

`:calve...` A layer with calve cells.


#### Setter Expressions inside a layer
| setter expression | description | range |
| --- | --- | --- |
| (n ...) | number of cells | 0 .. n |
| (x ...) | set x position | -1.0 .. 1.0 |
| (y ...) | set y position | -1.0 .. 1.0 |
| (w ...) | set the width of the shape | 0.0 .. 1.0 |
| (h ...) | set the width of the shape | 0.0 .. 1.0 |
| (H ...) | set the hue of the shape | will be reduced to the range <br> 0.0 .. 1.0 |
| (S ...) | set the color saturation of the shape | 0.0 .. 1.0 |
| (V ...) | set the brightness of the shape | 0.0 .. 1.0 |
| (A ...) | set the opacity of the shape | 0.0 .. 1.0 |

You can also specify for each layer how it is linked to its predecessor.

| setter expression | description | range |
| --- | --- | --- |
| (la ...) | layer opacity | 0.0 .. 1.0 |
| (lb ...) | layer blend mode | :src :mix :max :min :mul :add :diff :screen :overlay :inv :red :gray :inv_dest |

### Simple Example
![](./demo/simple/simple-cursor.jpg)

```clojure
(use 'jean.core)

(pipe :silent0 :cursor1)

(layer :silent0 []
  (img "flower-pages/page_301-center.jpg")
  (lb :diff)
)

(layer :cursor1 []
  (n 5)
  (lb :add)

  (x (* 0.5 (cos (* 0.0113 (+ i 1) f))))
  (y (sin (* 0.0121 (+ i 1) f)))
  (w 0.10)
  (h 0.10)

  (H (+ 0.05 (* i 0.030)))
  (S 0.5)
  (V 0.5)
  (A 0.4)
)
```

At the beginning, the expression `(use 'jean.core)` must be executed once. It loads the required Clojure namespace.
```clojure
(use 'jean.core)
```

The top level expression `(pipe ...)` should be executed next. It specifies the processing sequence in the graphics pipeline. The order in which the graphics data flows through the individual layers. This expression can be changed during a live coding session and sent again.

```clojure
(pipe :silent0 :cursor1)
```

All subsequent top level expressions can be changed and sent to the visualization as often as required. Top level expressions can also use the same name. If they have different contents, you can quickly switch back and forth between individual configurations.
```clojure
(layer :silent0 []
  (img "flower-pages/page_301-center.jpg")
  (lb :diff)
)

(layer :cursor1 []
  (n 5)
  (lb :add)

  (x (* 0.5 (cos (* 0.0113 (+ i 1) f))))
  (y (sin (* 0.0121 (+ i 1) f)))
  (w 0.10)
  (h 0.10)

  (H (+ 0.05 (* i 0.030)))
  (S 0.5)
  (V 0.5)
  (A 0.4)
)
```

### A Visual and Sound Example
![](./sketchbook/updown/up_down.jpg)

#### Cursor versus Calve
From the perspective of sound generation, there are two types of cells. Sound bars and cursors.

`:calve...` are sound bars. They can be hit by a `:cursor...` cell. At the beginning and end of the overlap, an OSC packet is sent from the visualization.

`:cursor...`cells continuously send their position as OSC packets.

#### Continuous detuning
The `\rsnz` Synth definition generates the background noise. This Synth definition is loaded into the SuperCollider sound server by the SuperCollider interpreter. Shortly afterwards, exactly 1 instance is started in the server to run for a longer period of time.
```java
SynthDef(\rsnz, { |out = 0, freq = 90, amp = 0.3, xpos = 0, fadeout = 0.0|
  var sig = Resonz.ar(BrownNoise.ar(0.5), freq, 0.1);
  Out.ar(out, 0.5 * fadeout * amp * Pan2.ar(sig, xpos));
}).add;
1.wait;

~a0 = Synth(\rsnz);
~a1 = Synth(\rsnz);

// receive messages send by the :cursor0 layer
h = OSCFunc({ |msg, time, addr, recvPort|
  if(msg[2] == 2,  // cursor cluster
    {
      if(msg[4] == 0, // cell id 0
        {
          ~fadeout = 1.0;
          ~a0.set(\xpos, msg[5]);
          ~a0.set(\amp, msg[13].linlin(0, 1, 0, 1));
          ~a0.set(\freq, msg[6].linexp(-1, 1, 10, 10*10));
          ~a0.set(\fadeout, ~fadeout);
        },
        {});
      if(msg[4] == 1, // cell id 1
        {
          ~fadeout = 1.0;
          ~a1.set(\xpos, msg[5]);
          ~a1.set(\amp, msg[13].linlin(0, 1, 0, 1));
          ~a1.set(\freq, msg[6].linexp(-1, 1, 30, 10*30));
          ~a1.set(\fadeout, ~fadeout);
        },
        {});
    },
    {});
}, '/j_stream');

```

The `:cursor0` layer in this example is responsible for animation two red ovals. These two cells / shapes are sending OSC packages at every frame. Each message contains the position, size and color of a shape. In this case the SuperCollider interpreter is the receiver. The interpreter will extract the relevant information and adjusts the tuning of the sound. This is an indirect way: The visualization speaks with the interpreter and then the interpreter informs the server that some parameter must be changed.
```clojure
(layer :cursor0 []
  (stream 1)
  (n 2)
  (la 0.3)
  (lb :screen)

  (x (* 0.9 (cos (* 0.0031 (+ 1 i) f))))
  (y (* (even? i 0.3 0.7) (/ (sin (* 0.037 (+ 1 i) f)))))
  (w 0.03)
  (h 0.20)

  (H (+ -0.01 (* 0.03 (+ 1 i))))
  (S 0.8)
  (V (+ 0.5 (* 0.4 (sin (* 0.1 (+ 1 i) f)))))
  (A 0.3)
)
```

#### Event based sounds
The `:calve1` layer is for animating two sound bars. These cells are the bluish rectangles. If one oft the cursor cells overlaps a calve cell a single OSC package is send to start a new Synth. This *start new Synth command* is send directly to the SuperCollider sound server. In this case the Synth is designed to terminate soon. You find a `doneAction: Done.freeSelf` in its definition. This allows the visualization to start many instances of this Synth one after the other without overloading the server.
```java
SynthDef(\dxkAcid,
  { |cax=0, cay=0, caw=0.01, cah=0.01, cux=0, cuy=0, gate = 1, rq = 0.3, amp = 0.05|
    // see: mrufrufin / dxkSynthDefs / GNU GENERAL PUBLIC LICENSE Version 3
    // abstracted out from 08091500Acid309 by_otophilia
    // changed env2 to multiplicative instead of additive
    var env1, env2, output;
    var freq = 100 + (500 * caw * cah);
    var pan = cax.linlin(-1, 1, -1, 1);
    freq = Lag.kr(freq, 0.12 * (1-Trig.kr(gate, 0.001)) * gate);
    env1 = EnvGen.ar(Env.new([0, 1.0, 0, 0], [0.001, 2.0, 0.04], [0, -4, -4], 2), gate, amp);
    env2 = EnvGen.ar(Env.adsr(0.001, 0.8, 1, 0.8, 1.4, -4), gate);
    output = LFPulse.ar(freq, 0.0, 0.51, 2, -1);

    output = RLPF.ar(output, freq * env2, rq);
    output = output * env1;
    output = Pan2.ar(output, pan);

    DetectSilence.ar(output, doneAction: Done.freeSelf);
    Out.ar(0, output);
}).add;
```

```clojure
(layer :calve1 []
  (hit :dxkAcid)
  (n 3)
  (shape 1)
  (la 0.5)
  (lb :add)

  (x 0)
  (y (* 0.08 (round (* 10 (cos (* 0.011 (+ 1 i) f))))))
  (w (* 2 (noise x (* f 0.001 y) 0.1)))
  (h 0.015)

  (H 0.5)
  (S 0.8)
  (V (+ 0.5 (* 0.4 (sin (* 0.1 (+ 1 i) f)))))
  (A 0.3)
)
```

You can find the complete source code in this folder: `sketchbook/updown/`


## Using Super Collider to generate Sounds.

### Position Messages
These data packets are sent from the visualization at 60 frames per second to the SuperCollider language interpreter. Every "cursor cell" (a visible shape) will send this message. This results in a high volume of data that has to be transmitted in real time. So you have to keep the number of cells low. For example, one dozen.

The SuperCollider Interpreter can do whatever it wants with this message. As a standard, it will extract parts of the information and pass it on to the SuperCollider Sound Server. We assume that the addressed Synth is already running and receives values to modified its soundscape.

| argument<br>index | argument<br>type | description |
| --- | --- | --- |
| 0 | int | stream id |
| 1 | int | characteristic of this cell|
| 2 | int | cluster / layer index |
| 3 | int | cell id |
| 4 | float | cell x position|
| 5 | float | cell y position|
| 6 | float | cell delta x since last frame (velocity) |
| 7 | float | cell delta y since last frame (velocity) |
| 8 | float | cell width of the shape |
| 9 | float | cell height of the shape |
| 10 | float | color hue |
| 11 | float | color saturation |
| 12 | float | color value|
| 13 | float | opacity / color alpha value |

```java
h = OSCFunc({ |msg, time, addr, recvPort|
      ~fadeout = 1.0;
  //  1 stream id
  //  2 characteristic
  //  3 cluster or layer index
  //  4 cell id
  //  5 cell x
  //  6 cell y
  //  7 cell delta x
  //  8 cell delta y
  //  9 cell w
  // 10 cell h
  // 11 cell color hue
  // 12 cell color saturation
  // 13 cell color value
  // 14 cell color alpha
  // [msg[4], msg[6]].postln;
  if (msg[4] == 0, {
    ~left.set(\speed, msg[6].linlin(-0.5, 0.5, 0.2, -1));
    ~left.set(\fadeout, ~fadeout);
  });
  if (msg[4] == 1, {
    ~right.set(\speed, msg[6].linlin(-0.5, 0.5, 0.2, -1));
    ~right.set(\fadeout, ~fadeout);
  });
}, '/j_stream');
```

### New Synth Messages
These messages are send directly to the SuperCollider Sound Server. Without taking the detour via the SuperCollider Interpreter.

 Each of these messages will create a new instance of the predefined Synth. To avoid overloading the system, these synths contain a DoneAction statment to remove them from the sound server. For example `doneAction: 2` or `PlayBuf.ar(..., doneAction: Done.freeSelf)` or `DetectSilence.ar(output, doneAction: 2);`.

These messages take advantage of the fact that a Synth has named parameters. The message consists mainly of name-value pairs.

| argument<br>index | argument<br>type | description | |
| --- | --- | --- | --- |
| 0 | string | name of the Synth | |
| 1 | int | always -1 | |
| 2 | int | always 0 | |
| 3 | int | always 0 | |
| 4 | string | 'cai' name of the parameter to be set | first name-value pair |
| 5 | int | calve cell id | "" |
| 4 | string | 'cax' name of the parameter to be set | second name-value pair |
| 5 | float | calve cell x position | "" |
| 6 | string | 'cay' name of the parameter to be set | next name-value pair |
| 7 | float | calve cell y position | "" |
| 8 | string | 'cady' name of the parameter to be set | next name-value pair |
| 9 | float | calve cell delta y since last frame (velocity) | "" |
| 10 | string | 'caw' name of the parameter to be set | next name-value pair |
| 11 | float | width of the shape | "" |
| 10 | string | 'cah' name of the parameter to be set | next name-value pair |
| 11 | float | height of the calve shape | "" |
| 12 | string | 'cui' name of the parameter to be set | first name-value pair |
| 13 | int | cursor cell id | "" |
| 14 | string | 'cux' name of the parameter to be set | second name-value pair |
| 15 | float | cell x position | "" |
| 16 | string | 'cuy' name of the parameter to be set | next name-value pair |
| 17 | float | cell y position | "" |
| 18 | string | 'cudy' name of the parameter to be set | next name-value pair |
| 19 | float | cell delta y since last frame (velocity) | "" |

```java
// a panning buffer player
SynthDef(\fresh, { | cai=0, cax=0, cay=0, caw=0, cah=0, cui=0, cux=0, cuy=0 |
	var pan = cax.linlin(-1, 1, -1, 1);
	Out.ar(0, 0.25 * Pan2.ar(PlayBuf.ar(1, cai, BufRateScale.kr(cai), doneAction: 2), pan))
	}).add;
```


## Top Level expressions
### A stack of layers
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| pipe | 1 .. n | enumerate used layers | (pipe :silent0 :cursor0 :calve1) |

### Defining a single layer
| function | number of<br>expressions | description | example |
| --- | --- | --- |  --- |
| layer :silent | 0 .. n | a layer without any sound generation | (layer :silent0 [] ...) |
| layer :cursor | 0 .. n | a layer with sound cursors | (layer :cursor1 [] ...) |
| layer :calve | 0 .. n | a layer with sound calves | (layer :calve2 [] ...) |

#### Assignments that can be used inside a layer
| function | number of<br>expressions | description | example |
| --- | --- | --- |  --- |
| lb | 1 |  | () |
| la | 1 |  | () |
| ltx | 1 |  | () |
| lty | 1 |  | () |
| lrot | 1 |  | () |
| img | 1 |  | () |
| shape | 1 |  | () |
| n | 1 |  | () |
| br | 1 |  | () |
| dec | 1 |  | () |
| x | 1 |  | () |
| y | 1 |  | () |
| w | 1 |  | () |
| h | 1 |  | () |
| H | 1 |  | () |
| S | 1 |  | () |
| V | 1 |  | () |
| A | 1 |  | () |
| stream | 1 |  | () |
| hit | 1 |  | () |
| exit | 1 |  | () |
| fresh | 1 |  | () |
|  | 1 |  | () |
|  | 1 |  | () |


#### Assignments for special effects inside a layer
| function | number of<br>expressions | description | example |
| --- | --- | --- |  --- |
| xscale | 1 |  | () |
| yscale | 1 |  | () |
| xamount | 1 |  | () |
| yamount | 1 |  | () |

### Defining a synchro layer
| function | number of<br>expressions | description | example |
| --- | --- | --- |  --- |
| influx | 1 |  | () |
| drain | 1 |  | () |
| pulse | 1 |  | () |

### Set predefined vars
| function | number of<br>expressions | description | example |
| --- | --- | --- |  --- |
| t0<br>t1<br>t2<br>t3<br>t4<br>t5<br>t6<br>t7 | 1 | assign per frame calculated values | (t0 (sin (* 0.01 f))) |

### Reading current values
| var | description | example |
| --- | --- |  --- |
| i | cell indice | i |
| i1 | cell indice + 1 | i1 |
| id | cell id | id |
| id1 | cell id + 1 | id1 |
| f | frame number | (sin (* 0.01 f)) |
| f1 | frame number + 1| (sin (* 0.01 f1)) |
| l | how many frames this cell exist | l |
| l1 | ?? how many frames + 1 this cell exist | l1 |
| e | cursor cell has entered a calve cell |  |
| ks0<br>ks1<br>ks2<br>ks3<br>ks4<br>ks5<br>ks6<br>ks7 | nanoKONTROL2 sliders | (* 3 ks0) |
| kp0<br>kp1<br>kp2<br>kp3<br>kp4<br>kp5<br>kp6<br>kp7 | nanoKONTROL2 sliders | (sqr kp0) |


### Top level Special commands
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| ! :clear | 0 .. n | clear canvas to black | (! :clear) |
| ! :reset | 0 .. n | special commands | (! :reset) |
|  | 0 .. n | | () |
|  | 0 .. n | | () |

## Expressions
### Simple arithmetic
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| + | 2 .. n | sum of all parameters | (+ 0.1 x)<br><br>(+ 1 2 3) |
| - | 2 .. n | subtract from first parameter all following parameters | (- 0.1 x)<br><br>(- 1 2 3) |
| * | 2 .. n | multiply all parameters | (* 0.1 x)<br><br>(* 0.05 f x) |
| / | 2 .. n | dived the first parameter by all following parameters | (/ x 2)<br><br>(/ 16 4 2) |
| % | 2 | modulo | (% i 4) |
| sqr | 1 | square of x<br>(* x x) | (sqr x) |
| sqrt | 1 | square root | (sqrt x) |

### Conditionals
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| zero? | 3 | If the first term evaluates to 0 the second term is evaluated. If not the third term is evaluated. | (zero?<br>(/ x 1000)<br>1<br>(+ 1 x)) |
| pos? | 3 | If the first term evaluates to an positive number the second term is evaluated. If not the third term is evaluated. | (pos?<br>x<br>x<br>(* -1.0 x)) |
| even? | 3 | If the first term evaluates to an even number the second term is evaluated. If not the third term is evaluated. | (even?<br>x<br>x<br>(* -1.0 x)) |

### Common functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| abs | 1 | absolute value | (abs x) |
| floor | 1 | nearest integer less than x | (floor x) |
| ceil | 1 | nearest integer greater than x | (ceil x) |
| clamp | 3 | returns (min (max x minVal) maxVal) | (clamp x 0.4 0.6) |
| sign | 1 | signum function | (sign y) |
| trunc | 1 | truncate | (trunc 1.3) |
| round | 1 | round to nearest | (round x) |

### Exponential functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| pow | 2 | returns x raised to the y power | (pow x y) |
| log | 1 | returns the natural logarithm of x | (log (abs x)) |
| log2 | 1 | returns the binary logarithm of x | (log2 (abs x)) |

### Trigonometry functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| cos | 1 | cosine | (cos 0) |
| ucos | 1 | like cosine but returns values in the range 0.0 to 1.0 | (cos (* 0.1 f)) |
| acos | 1 | inverse cosine | (acos 0) |
| sin | 1 | sine | (sin 1.57) |
| usin | 1 | like sine but returns values in the range 0.0 to 1.0 | (usin -0.5) |
| asin | 1 | inverse sine | (asin 0) |
| tan | 1 | tangent | (tan 0.5) |
| atan | 2 | arc tanget for y over x | (atan y x) |

### Merge values
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| max | 2 | Maximum value | (max 0 x) |
| min | 2 | Minimum value | (min f 100) |
| mix | 3 | linear blend of x and y | (mix x y 0.75) |

### Noise functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| rand | 0 | random numbers range 0.0 to 1.0 | (rand) |
| snoise | 3 | simplex noise function | (snoise x y z) |
| unoise | 3 | like noise but returns values approximately in the range 0.0 to 1.0 | ((unoise x y z)) |
| turb | 4 | turbulence noise function<br>last parameter stets the octaves | (turb 0.1 0.2 0.3 3) |

see: http://mrl.nyu.edu/~perlin/noise/

### Procedural textures functions
seen: Darwyn Peachey: Building Procedural Textures, in Texturing and Modeling, 1994

| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| step | 2 |  | (step y -0.99) |
| smooth | 3 | smooth step | (smooth -1 1 x) |
| pulse | 3 |  | (puls -1 1 x) |
| clamp | 3 |  | (clamp -1 1 x) |
| box | 3 |  | (box -1 1 x) |
| gamma | 2 |  | (gamma 0.7 x) |
| gain | 2 |  | (gain 0.8 0.5) |
| bias | 2 |  | (bias 0.7 x) |


### Geometric functions
| function | number of<br>parameters | description | example |
| --- | --- | --- |  --- |
| distance | 4 | euclidean distance between two points  | (TODO) |
| mdist | 4 | Manhattan distance between two points  | (mdist 11 12 x y) |

## Links

https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html#jvms-6.5

https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html

https://asm.ow2.io/

https://github.com/hoijui/JavaOSC

https://www.slf4j.org/

https://github.com/danbernier/korgnano

https://github.com/kingpulse/xdotool-Java

https://linuxhint.com/xdotool_stimulate_mouse_clicks_and_keystrokes/

### Blend modes
https://en.wikipedia.org/wiki/Blend_modes

https://web.archive.org/web/20180807162746/http://www.svgopen.org/2005/papers/abstractsvgopen/index.html#S5.

https://ssp.impulsetrain.com/porterduff.html

### LWJGL 3
https://ahbejarano.gitbook.io/lwjglgamedev/

### OpenGL
#### Text rendering in OpenGL
https://github.com/SilverTiger/lwjgl3-tutorial/wiki/Fonts

https://www.glprogramming.com/red/chapter06.html

### SuperCollider
https://doc.sccode.org/Help.html

https://depts.washington.edu/dxscdoc/Help/Help.html

### nanoKontrol2
https://github.com/danbernier/korgnano/tree/master
