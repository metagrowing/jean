#version 330

out vec4 fragColor;

uniform sampler2D texture0;
uniform float alpha = 1.0;
uniform float ltx = 0.0;
uniform float lty = 0.0;

void main()
{
    // TODO use texture coordinates s t
    fragColor = vec4(texture(texture0, vec2(ltx, lty) + (gl_FragCoord.xy / textureSize(texture0, 0))).rgb, alpha);
}
