#version 330

out vec4 fragColor;

uniform sampler2D texture0; // new layer
uniform sampler2D texture1; // layer accumulator
uniform float xscale = 1.0;
uniform float yscale = 1.0;

void main()
{
    vec2 s = textureSize(texture1, 0);
    vec2 p = gl_FragCoord.xy / s;
    p = fract(p * vec2(xscale, yscale));
    fragColor = texture(texture1, p);
}
