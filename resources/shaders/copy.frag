#version 330

out vec4 fragColor;

uniform sampler2D texture0;

void main()
{
    fragColor = vec4(texture(texture0, gl_FragCoord.xy / textureSize(texture0, 0)).rgb, 1.0);
}
