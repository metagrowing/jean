#version 330

out vec4 fragColor;

uniform sampler2D texture0; // new layer
uniform sampler2D texture1; // layer accumulator

void main()
{
    vec4 src = texture(texture0, gl_FragCoord.xy / textureSize(texture0, 0));
    if(src.a > 0.0)
        fragColor = src;
    else
        fragColor = texture(texture1, gl_FragCoord.xy / textureSize(texture1, 0));
}
