#version 330

out vec4 fragColor;

uniform sampler2D texture0; // new layer
uniform sampler2D texture1; // layer accumulator

void main()
{
    vec4 src  = texture(texture0, gl_FragCoord.xy / textureSize(texture0, 0));
    vec4 dest = texture(texture1, gl_FragCoord.xy / textureSize(texture1, 0));
    if(src.a > 0.0) {
        if(length(src.rgb) < 0.5)
             fragColor = vec4(vec3(2.0) * dest.rgb * src.rgb, src.a);
        else
             fragColor = vec4(vec3(1.0) - (vec3(1.0) - src.rgb) * (vec3(1.0) - dest.rgb), src.a);
    }
    else
        fragColor = dest;
}
