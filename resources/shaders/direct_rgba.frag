#version 330

out vec4 fragColor;
uniform float r = 1.0;
uniform float g = 1.0;
uniform float b = 1.0;
uniform float a = 1.0;

void main()
{
    fragColor = vec4(r, g, b, a);
}