#version 330

out vec4 fragColor;

uniform sampler2D texture0; // new layer
uniform sampler2D texture1; // layer accumulator

void main()
{
    vec4 src  = texture(texture0, gl_FragCoord.xy / textureSize(texture0, 0));
    vec4 dest = texture(texture1, gl_FragCoord.xy / textureSize(texture1, 0));
    vec3 one_minus_src  = vec3(1.0) - src.rgb;
    vec3 one_minus_dest = vec3(1.0) - dest.rgb;
    if(src.a > 0.0)
        fragColor = vec4(vec3(1.0) - one_minus_src * one_minus_dest, src.a);
    else
        fragColor = dest;
}
