#version 330

out vec4 fragColor;

uniform sampler2D texture0; // new layer
uniform sampler2D texture1; // layer accumulator
uniform float tx = 0.0;
uniform float ty = 0.0;
uniform float sx = 1.0;
uniform float sy = 1.0;
uniform float sin_rot = 0.0;
uniform float cos_rot = 1.0;

void main()
{
    vec2 p = gl_FragCoord.xy / textureSize(texture1, 0) - vec2(0.5) + vec2(tx, ty);
    p = p * vec2(sx, sy);
    vec2 r = vec2(p.x * cos_rot - p.y * sin_rot,
                  p.x * sin_rot + p.y * cos_rot);
    fragColor = texture(texture1, r + vec2(0.5));
}
