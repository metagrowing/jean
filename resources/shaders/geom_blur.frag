#version 330

out vec4 fragColor;

uniform sampler2D texture0; // new layer
uniform sampler2D texture1; // layer accumulator

void main()
{
    vec2 s = textureSize(texture1, 0);
	fragColor  = 0.077847 * texture(texture1, clamp(gl_FragCoord.xy + vec2(-1,  1), vec2(0.0), s) / s);
	fragColor += 0.123317 * texture(texture1, clamp(gl_FragCoord.xy + vec2( 0,  1), vec2(0.0), s) / s);
	fragColor += 0.077847 * texture(texture1, clamp(gl_FragCoord.xy + vec2( 1,  1), vec2(0.0), s) / s);

	fragColor += 0.123317 * texture(texture1, clamp(gl_FragCoord.xy + vec2(-1,  0), vec2(0.0), s) / s);
	fragColor += 0.195346 * texture(texture1, gl_FragCoord.xy / s);
	fragColor += 0.123317 * texture(texture1, clamp(gl_FragCoord.xy + vec2( 1,  0), vec2(0.0), s) / s);

	fragColor += 0.077847 * texture(texture1, clamp(gl_FragCoord.xy + vec2(-1, -1), vec2(0.0), s) / s);
	fragColor += 0.123317 * texture(texture1, clamp(gl_FragCoord.xy + vec2( 0, -1), vec2(0.0), s) / s);
	fragColor += 0.077847 * texture(texture1, clamp(gl_FragCoord.xy + vec2( 1, -1), vec2(0.0), s) / s);
}
