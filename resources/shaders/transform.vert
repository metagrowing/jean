#version 330

layout (location=0) in vec3 inPosition;
uniform float tx = 0.0;
uniform float ty = 0.0;
uniform float sx = 1.0;
uniform float sy = 1.0;
uniform float sin_rot = 0.0;
uniform float cos_rot = 1.0;

void main()
{
    vec2  s = inPosition.xy * vec2(sx, sy);
    vec2  r = vec2(s.x * cos_rot - s.y * sin_rot,
                   s.x * sin_rot + s.y * cos_rot);
    gl_Position = vec4(r + vec2(tx, ty), 0.0, 1.0);
}