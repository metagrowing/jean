(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :cursor0)

(layer :silent0
  (img "morse/diagram_1_inv.png")
)


(layer :cursor0 []
  ; (hit :glisson)

  (shape :oval)
  (n 10)
  (br  0.009)
  (dec 0.001)

  (y (- (* 0.1 (% id 10)) 0.7))
  (x (* 0.9 (sin (* 0.01 f))))
  (w 0.030)
  (h 0.030)

  (H (* id 0.1))
  (S 0.5)
  (V 1)
  (A 0.2)
)
