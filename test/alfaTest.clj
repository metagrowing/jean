(ns jean.demo)
(use 'jean.core)

(pipe :silent :calve0 :cursor0 :calve1)

(layer :silent
  ; (img "flower-pages/page_301-center.png")
  (img "flower-pages/page_241-center.png")
  (la 0.001)
  (lb 0)
)

(layer :calve0 []
  (shape :tri)
  (n 1)
  (lb 2)
  (la 0.1)
  (rot (* 0.01 f))
  ; (x 0)
  ; (y 0)
  (w 0.2)
  ; (h 2)
  (H 0)
  (S 0)
  (V 0.05)
  (A 0.05)
)

(layer :cursor0 []
  (n 7)
  (br 0.2)
  (dec 0.001)
  (la 0.25)
  (lb 9)

  (x (* 0.113 i1 (cos (* 0.00113 (* 3 i) f))))
  (y (* 0.071 i1 (sin (* 0.00073 (* 3 i) f))))
  (w 0.05)
  (h 0.05)

  (H 0.95)
  (S 0.5)
  (V 0.5)
  (A 0.8)
)

(layer :calve1 []
  (hit :dxkAcid)
  ; (hit :glisson)
  ; (exit :glisson)

  (shape :rect)
  (n 5)
  (la 0.05)
  (lb 9)

  (x (* 0.1 i1 (cos (* -0.00117 (* i1 f)))))
  (y (* 0.1 i1 (sin (* -0.00119 (* i1 f)))))
  (h (* 0.02 i1))
  (w 0.250)
  (rot (* 0.01 f))

  ; (H (+ 0 (* 0.01 i)))
  (H (mix 0.4 0.9 e))
  (S 0.5)
  (V (mix 0.3 0.6 e))
  (A 0.3)
)
