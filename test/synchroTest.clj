(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :synchro0)

(layer :silent0 []
  ; (img "morse/diagram_1_inv.png")
  ; (img "morse/Modern_telegraphy_some_errors_[...]Morse_Samuel_bpt6k82605g.pdf")
  (img "lorenz/LGP-30-1280.png")
  (la 0.1)
)

(layer :synchro0 []
  (hit :dxkAcid)

  (shape :oval)
  (n 27)
  (lb :diff)

  (influx  0.0064)
  (drain   0.0033)
  (pulse   (* 6 0.0025))

  ; (influx  0.0044)
  ; (drain   0.0013)
  ; (pulse   (* 1 0.0025))

  (x (- (* 0.060 i) 0.7))
  (y 0.25)
  (w 0.030)
  (h 0.250)

  (H 170)
  (S 0.5)
  (V (* 30 w))
  (A 0.2)
)
