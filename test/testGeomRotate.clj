(ns jean.demo)
(use 'jean.core)

; (pipe :silent0 :silent1 :cursor0 :cursor1)
; (pipe :silent0 :silent1 :silent2 :cursor0 :cursor1)
; (pipe :silent0 :silent1)
(pipe :silent0 :silent1 :cursor0 :silent1 :cursor1 :silent1)

(t0 (+ 1.57 (* i1 3.14)))
(t1 (sin 1.57))

(layer :silent0 []
  ; (img "frozen/frozen0.png")
  (img "irrlicht/P1090582-1024x1024.png")
  (lb 8)
  (la 0.1)
  (lsx 0.05)
  (lrot (* 0.01 f))
)

(layer :silent1 []
  (lb 16)
  ; (lsx 0.05)
  ; (lrot (* 0.01 f))
  (xscale 25)
  (yscale 5)
  (xamount 0.2)
  (yamount 0.2)
)

(layer :cursor0 []
  (n 3)
  (lb 7)

  (x (* 0.213 i1 (cos (* 0.00113 (* 3 i1) f))))
  (y (* 0.171 i1 (sin (* 0.00073 (* 3 i1) f))))
  (w 0.10)
  (h 0.10)

  (H 0)
  (S 0.5)
  (V 1)
  (A 0.1)
)

(layer :cursor1 []
  (n 6)
  (lb 7)

  (x (+ x_ (* 0.113 i1 (cos (* 0.00117 (* 3 i1) f)))))
  (y (+ y_ (* 0.071 i1 (sin (* 0.00091 (* 3 i1) f)))))
  (w 0.050)
  (h 0.050)

  (H 0.1)
  (S 0.5)
  (V 1)
  (A 0.5)
)
