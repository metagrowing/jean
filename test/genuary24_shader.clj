(ns jean.demo)
(use 'jean.core)

(pipe :silent0 :silent1 :cursor0 :cursor1)

(layer :silent0 []
  (img "flower-pages/page_301-center.png")
  (lb 10)
  (la 0.01)
)

(layer :silent1
  (img "flower-pages/page_241-center.png")
  (la 0.01)
  (lb 9)
)

(layer :cursor0 []
  (n 6)
  (lb 8)

  (x (* 0.113 i1 (cos (* 0.00113 (* 3 i1) f))))
  (y (* 0.071 i1 (sin (* 0.00073 (* 3 i1) f))))
  (w 0.05)
  (h 0.05)

  (H 0.95)
  (S 0.8)
  (V 0.5)
  (A 0.2)
)

(layer :cursor1 []
  (n 2)
  (lb 12)
  (shape :rect)

  (x (* 0.113 i1 (cos (* 0.00073 (* 3 i1) f))))
  (y (* 0.43 i1 (sin (* 0.00113 (* 3 i1) f))))
  (w 0.45)
  (h 0.05)

  (H 0.2)
  (S 0.5)
  (V 0.9)
  (A 0.1)
)
