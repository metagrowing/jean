(ns jean.demo)
(use 'jean.core)

(pipe :cursor0)
(! -2)
(layer :cursor0 []
  (n 11)
  (lb :add)
  (la 0.1)

  (x (* 0.9 (cos (* 0.0031 (+ 1 i) f))))
  (y (* 4 (/ (sin (* 0.037 (+ 1 i) f)) (log (* 0.1 f)))))
  (w 0.05)
  (h 0.20)

  (H 0.5)
  (S 0.8)
  (V (+ 0.5 (* 0.4 (sin (* 0.1 i1 f)))))
  (A 0.1)
)
